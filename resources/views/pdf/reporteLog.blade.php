<!-- PDF: LOG del sistema -->
<style>
html {
    margin: 15px 10px;
}
.row {
    margin-right: 15px;
    margin-left: 15px;
}

.col-lg-12 {
    width: 100%;
}

.text-center {
  text-align: center;
}

.brd {
    border:1px solid black; 
    border-collapse:collapse
}
thead{
  font-weight: bold;
}
</style>

<div class="row">
    <div class="col-lg-12">
        <table id="titulo" style="width:90%" >
            <tbody>
                <tr>
                    <td><h4>LOG del sistema</h4></td>
                    <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                </tr>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <table id="LOG" style="width:90%"  class="brd">
                    <thead>
                        <tr>
                            <th style="text-align:center" class="brd"><strong>#</strong></th>
                            <th style="text-align:center" class="brd"><strong>Usuario</strong></th>
                            <th style="text-align:center" class="brd"><strong>Evento</strong></th>
                            <th style="text-align:center" class="brd"><strong>Detalle</strong></th>
                            <th style="text-align:center" class="brd"><strong>Fecha/hora evento</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($logs as $logsItem)
                        <tr >
                            <td style="text-align:center" class="brd">{{$logsItem->id}}</td>
                            <td style="text-align:center" class="brd">{{$logsItem->evento_usr}}</td>
                            <td style="text-align:center" class="brd">{{$logsItem->evento}}</td>
                            <td style="text-align:center" class="brd">{{$logsItem->evento_detalle}}</td>
                            <td style="text-align:center" class="brd">{{$logsItem->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.ol-sm-12 -->
        </div><!-- /.row -->
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

    



