<!-- PDF: tabla de TODAS reservas recibidas -->
<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, 
    .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3,
    .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4,
    .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, 
    .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, 
    .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, 
    .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, 
    .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, 
    .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, 
    .col-sm-12, .col-md-12, .col-lg-12 {
        position: relative;
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
    }
    
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
    
</style>

    <div class="row">
        <div class="col-lg-12">
            <table id="titulo" style="width:90%">
                <tbody>
                    <tr>
                        <td><h4>Reservas</h4></td>
                        <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                    </tr>
                </tbody>
            </table>
            <hr />
            <div class="row">
                <div class="col-sm-12">
                    <table id="ReporteReservas" style="width:90%" class="brd">
                        <thead>
                            <tr>
                                <th style="text-align:center" class="brd"><strong>Código Reserva</strong></th>
                                <th style="text-align:center" class="brd"><strong>Solicitante</strong></th>
                                <th style="text-align:center" class="brd"><strong>Fecha Creada</strong></th>
                                <th style="text-align:center" class="brd"><strong>Fecha desde</strong></th>
                                <th style="text-align:center" class="brd"><strong>Fecha hasta</strong></th>
                                <th style="text-align:center" class="brd"><strong>Estado</strong></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($reservas as $reservasItem)
                                <tr>
                                    <td class="brd">{{$reservasItem->id}}</td>
                                    <td class="brd">{{$reservasItem->user_id}}</td>
                                    <td class="brd">{{$reservasItem->created_at}}</td>
                                    <td class="brd">{{$reservasItem->fecha_desde}} - {{$reservasItem->hora_desde}}</td>
                                    <td class="brd">{{$reservasItem->fecha_hasta}} - {{$reservasItem->hora_hasta}}</td>
                                    <td class="brd">{{$reservasItem->est_reserv_id}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div><!-- /.ol-sm-12 -->
            </div><!-- /.row -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->

    



