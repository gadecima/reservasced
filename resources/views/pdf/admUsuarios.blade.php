<!-- PDF: tabla de TODAS los prestamos realizados -->
<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
    <div class="row">
        <div class="col-lg-12">
            <table id="titulo" style="width:90%">
                <tbody>
                    <tr>
                        <td><h4>Usuarios del Sistema</h4></td>
                        <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                    </tr>
                </tbody>
            </table>
            <hr />
            <div class="row">
                <div class="col-sm-12">
                    <table id="Prestamos" style="width:90%" class="brd">
                        <thead>
                            <tr>
                                <th style="text-align:center" class="brd"><strong>#ID</strong></th>
                                <th style="text-align:center" class="brd"><strong>Rol</strong></th>
                                <th style="text-align:center" class="brd"><strong>eMail</strong></th>
                                <th style="text-align:center" class="brd"><strong>Apellido y Nombre</strong></th>
                                <th style="text-align:center" class="brd"><strong>DNI</strong></th>
                                <th style="text-align:center" class="brd"><strong>Teléfono</strong></th>
                                <th style="text-align:center" class="brd"><strong>Cargo/Área/Dependencia</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuariosItem)
                            <tr>
                                <td class="brd">{{$usuariosItem->id}}</td>
                                <td class="brd">{{$usuariosItem->rol_id}}</td>
                                <td class="brd">{{$usuariosItem->email}}</td>
                                <td class="brd">{{$usuariosItem->apellido.', '.$usuariosItem->nombre}}</td>
                                <td class="brd">{{$usuariosItem->dni}}</td>
                                <td class="brd">{{$usuariosItem->telefono}}</td>
                                <td class="brd">{{$usuariosItem->cargo}}<br>{{$usuariosItem->ofi_area}} <br> {{$usuariosItem->dependencia}} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.ol-sm-12 -->
            </div><!-- /.row -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->

    



