<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <table id="titulo" style="width:90%">
            <tbody>
                <tr>
                    <td><h4>Reporte de Reservas</h4></td>
                    <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                </tr>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <table id="datos" style="width:90%" class="brd">
                    <thead>
                        <tr>
                            <th  class="brd"style="text-align:center"><strong>Código Reserva</strong></th>
                            <th  class="brd"style="text-align:center"><strong>Solicitante</strong></th>
                            <th  class="brd"style="text-align:center"><strong>Fecha Creada</strong></th>
                            <th  class="brd"style="text-align:center"><strong>Fecha desde</strong></th>
                            <th  class="brd"style="text-align:center"><strong>Fecha hasta</strong></th>
                            <th  class="brd"style="text-align:center"><strong>Estado</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservas as $reservasItem)
                        <tr>
                            <td class="brd">{{$reservasItem->id}}</td>
                            <td class="brd">{{$reservasItem->user_id}}</td>
                            <td class="brd">{{$reservasItem->created_at}}</td>
                            <td class="brd">{{$reservasItem->fecha_desde}} - {{$reservasItem->hora_desde}}</td>
                            <td class="brd">{{$reservasItem->fecha_hasta}} - {{$reservasItem->hora_hasta}}</td>
                            <td class="brd">{{$reservasItem->est_reserv_id}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.ol-sm-12 -->
        </div><!-- /.row -->
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

    



