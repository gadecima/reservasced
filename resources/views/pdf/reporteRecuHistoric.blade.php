<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table id="titulo" style="width:90%">
                <tbody>
                    <tr>
                        <td><h4>Histórico de Recurso {{ $codigo }}</h4></td>
                        <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                    </tr>
                </tbody>
            </table>
            <hr />
            <div class="row">
                <div class="col-sm-12">
                    <table id="datos" style="width:90%" class="brd">
                        <thead>
                            <tr>
                                <th class="brd"style="text-align:center"><strong>Evento</strong></th>
                                <th class="brd"style="text-align:center"><strong>#id evento</strong></th>
                                <th class="brd"style="text-align:center"><strong>Desde</strong></th>
                                <th class="brd"style="text-align:center"><strong>Solicitante</strong></th>
                                <th class="brd"style="text-align:center"><strong>Estado</strong></th>
                                <th class="brd"style="text-align:center"><strong>Hasta</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($historic as $historicItem) 
                                <tr>
                                    <td class="brd"style="text-align:center">{{ $historicItem->Evento }}</td> 
                                    <td class="brd"style="text-align:center">{{ $historicItem->evento_id }}</td>
                                    <td class="brd"style="text-align:center">{{ $historicItem->f_desde }}</td>
                                    <td class="brd"style="text-align:center">{{ $historicItem->solicitante }}</td>
                                    <td class="brd"style="text-align:center">{{ $historicItem->estado }}</td>
                                    @if ($historicItem->estado == 'Finalizada' || $historicItem->estado == 'Devuelto')
                                        <td class="brd"style="text-align:center">{{ $historicItem->f_desde }}</td>
                                    @else
                                        <td class="brd"style="text-align:center"> -- </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.ol-sm-12 -->
            </div><!-- /.row -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container -->
    



