<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table id="titulo" style="width:90%">
                <tbody>
                    <tr>
                        <td><h4>Reporte de Recursos</h4></td>
                        <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                    </tr>
                </tbody>
            </table>
            <hr />
            <div class="row">
                <div class="col-sm-12">
                    <table id="datos" style="width:90%" class="brd">
                        <thead>
                            <tr>
                                <th  class="brd"style="text-align:center"><strong>Código</strong></th>
                                <th  class="brd"style="text-align:center"><strong>Serie</strong></th>
                                <th  class="brd"style="text-align:center"><strong>Marca</strong></th>
                                <th  class="brd"style="text-align:center"><strong>Modelo</strong></th>
                                <th  class="brd"style="text-align:center"><strong>Tipo</strong></th>
                                <th  class="brd"style="text-align:center"><strong>estado</strong></th>
                                <th  class="brd"style="text-align:center"><strong>Cantidad</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($recursos as $recursosItem)
                            <tr>
                                <td class="brd">{{$recursosItem->codigo}}</td>
                                <td class="brd">{{$recursosItem->serie}}</td>
                                <td class="brd">{{$recursosItem->marca}}</td>
                                <td class="brd">{{$recursosItem->modelo}}</td>
                                <td class="brd">{{$recursosItem->tipo}}</td>
                                <td class="brd">{{$recursosItem->estado}}</td>
                                <td class="brd">{{$recursosItem->cant}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="brd" colspan="7" style="text-align:center">*Para más detalles, puede imprimir los detalles de cada recurso del listado</td>
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.ol-sm-12 -->
            </div><!-- /.row -->
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
</div><!-- /.container -->
    



