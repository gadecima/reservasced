<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
    <div class="row">
        <div class="col-lg-12">
            <table id="titulo" style="width:90%">
                <tbody>
                    <tr>
                        <td><h4>Detalle Recurso</h4></td>
                        <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                    </tr>
                </tbody>
            </table>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <hr />
    <div class="row">
        <div class="col-lg-12">
            <table style="width:100%" class="brd">
                <thead>
                    <tr>
                        <th class="brd" scope="col" colspan="2">Detalles del recurso ID #{{$recurso->id}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td  class="brd" scope="row"><strong>Código:</strong></td>
                        <td class="brd">{{$recurso->codigo}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Serie:</strong></td>
                        <td class="brd">{{$recurso->serie}}</td>
                    </tr>                
                    <tr>
                        <td class="brd" scope="row"><strong>Marca:</strong></td>
                        <td class="brd">{{$recurso->marca}}</td>
                    </tr>
                    <tr>
                       <td class="brd"scope="row"><strong>Modelo:</strong></td>
                        <td class="brd">{{$recurso->modelo}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Descripción:</strong></td>
                        <td class="brd">{{$recurso->descripcion}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Tipo:</strong></td>
                        <td>{{$recurso->tipo}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Estado:</strong></td>
                        <td class="brd">{{$recurso->estado}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Disponibilidad:</strong></td>
                        @if ($recurso->disponible)
                            <td class="brd">Disponible</td>
                        @else
                            <td class="brd">NO disponible</td>
                        @endif
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Cantidad:</strong></td>
                        <td class="brd">{{$recurso->cant}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Observaciones:</strong></td>
                        <td class="brd">{{$recurso->observacion}}</td>
                    </tr>
                    <tr>
                        <td class="brd" scope="row"><strong>Creado fecha:</strong></td>
                        <td class="brd">{{$recurso->created_at}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    



