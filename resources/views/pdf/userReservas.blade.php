<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
    
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <table id="titulo" style="width:90%" >
            <tbody>
                <tr>
                    <td><h4>Reservas realizadas</h4></td>
                    <td style='text-align:right'><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                </tr>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-sm-12">
                <table id="MisReservas" style="width:90%"  class="brd">
                    <thead>
                        <tr>
                            <th  style="text-align:center" class="brd">Código Reserva</th>
                            <th  style="text-align:center" class="brd">Fecha Creada</th>
                            <th  style="text-align:center" class="brd">Fecha desde</th>
                            <th  style="text-align:center" class="brd">Fecha hasta</th>
                            <th  style="text-align:center" class="brd">Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservUser as $reservUserItem)
                        <tr>
                            <th class="brd">{{$reservUserItem->id}} </th>
                            <td class="brd">{{$reservUserItem->created_at}}</td>
                            <td class="brd">{{$reservUserItem->fecha_desde}} - {{$reservUserItem->hora_desde}}</td>
                            <td class="brd">{{$reservUserItem->fecha_hasta}} - {{$reservUserItem->hora_hasta}}</td>
                            <td class="brd">{{$reservUserItem->est_reserv_id}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.ol-sm-12 -->
        </div><!-- /.row -->
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

    



