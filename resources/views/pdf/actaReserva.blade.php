<!-- PDF: Acta de reserva -->
<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
    .align-top {
        vertical-align: top !important;
    }

    .align-middle {
        vertical-align: middle !important;
    }

    .align-bottom {
        vertical-align: bottom !important;
    }
    
</style>

<div class="row">
    <div class="col-lg-12">
        <table style="width:100%">
            <tbody>
                <tr>
                    <td class="align-bottom"><b>Reserva #{{$codigo}}</b></td>
                    <td style="text-align:right"><img src="images/logoCEDMED.jpg" alt="logo_CED" width="238" height="51" /></td>
                </tr>
            </tbody>
        </table>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->
<hr />
<div class="row">
    <div class="col-lg-12">
        <table style="width:100%">
            <tbody>
                <tr>
                    <td>
                        <p style="text-align: right;"><span>San Miguel de Tucumán, {{ $hoy }} </span></p>
                        <p><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; En sede de la 
                            <strong>Coordinación de Educación Digital</strong>, se labra la presente acta
                             para dejar constancia de la entrega en <strong>calidad de préstamo</strong> 
                             por los recursos listados a continuación, a <strong>{{ $creador->apellido }},
                             {{ $creador->nombre }}</strong>, en su caracter de: {{ $creador->cargo }} del 
                             Área/Dependencia: {{ $creador->ofi_area }} - {{ $creador->dependencia }}, 
                             cuyos datos de contacto son: <strong>{{ $creador->telefono }}</strong> <br/></span>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <table style="width:100%" class="brd">
            <thead>
                <tr >
                    <th style="text-align:center" class="brd"><strong>id</strong></th>
                    <th style="text-align:center" class="brd"><strong>tipo</strong></th>
                    <th style="text-align:center" class="brd"><strong>Marca/Modelo</strong></th>
                    <th style="text-align:center" class="brd"><strong>Descripción</strong></th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($reservados)) 
                    @foreach($reservados as $reservadosItem)
                    <tr>
                        <th scope="row" class="brd">{{$reservadosItem->codigo }} </th>
                        <td class="brd">{{$reservadosItem->tipo}}</td>
                        <td class="brd">{{$reservadosItem->marca.'-'.$reservadosItem->modelo}}</td>
                        <td class="brd">{{$reservadosItem->descripcion}}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div><!-- /.ol-sm-12 -->
</div><!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table style="width:100%">
            <tbody>
                <tr>
                    <td>
                        <p><strong><span>El solicitante se hace responsable de regresar los recursos prestados 
                            en las mismas condiciones que se le entregaron.</span></strong>
                        </p>
                        <p><span>Se establece como fecha de devolución el día <strong>{{ $fecha_hasta }}</strong>
                             a las {{ $hora_hasta }}</span>
                        </p>
                        <p>Para constancia firman:</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table style="width:100%">
            <tbody>
                <tr>
                    <td style="text-align:center; height:100px;vertical-align:bottom">
                        Soporte Técnico</td>
                    <td style="text-align:center; height:100px;vertical-align:bottom">
                        Solicitante</td>
                </tr>
            </tbody>
        </table>
    </div><!-- /col-lg-12 -->
</div><!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table style="width:100%" class="brd">
            <tbody>
                <tr>
                    <td style="text-align:center" class="brd" colspan="2">
                        <strong>Devolución de recursos</strong>
                    </td>
                </tr>
                <tr>
                    <td class="brd">Fecha devolución: ____ /_____/ 2022</td>
                    <td class="brd" style="text-align:center; height:80px;vertical-align:bottom" rowspan="2">firma y sello recibe</td>
                </tr>
                <tr>
                    <td class="brd" style="text-align:center; height:80px;vertical-align:bottom">Observaciones:</td>
                </tr>
            </tbody>
        </table>
    </div><!-- /col-lg-12 -->
</div><!-- /.row -->
    



