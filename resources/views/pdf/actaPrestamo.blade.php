<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
    <div class="row">
        <div class="col-lg-12">
            <table style="width:100%">
                <tbody>
                    <tr>
                        <td style="text-align:right"><img src="images/logoCEDMED.jpg" alt="logoCED" width="200" height="50"/></td>
                    </tr>
                </tbody>
            </table>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->
    <hr />
    <div class="row">
        <div class="col-lg-12">
            <table style="width:100%">
                <tbody>
                    <tr>
                        <td>
                            <p style="text-align: right;"><span>San Miguel de Tucumán, {{ $hoy }} </span></p>
                            <p><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; En sede de la <strong>Coordinación de Educación Digital</strong>, 
                                se entrega en <strong>calidad de préstamo</strong> los recursos listados a continuación, al Sr/a <strong>
                                {{ $prestamo->solicitante }}</strong>, con DNI N° {{ $prestamo->dni }}, cuyos datos de contacto son:
                                <strong>{{ $prestamo->contacto }}.</strong> <br/></span>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <table style="width:100%" class="brd">
                <thead>
                    <tr >
                        <th class="brd" style="text-align:center"><strong>id</strong></th>
                        <th class="brd" style="text-align:center"><strong>tipo</strong></th>
                        <th class="brd" style="text-align:center"><strong>Marca/Modelo</strong></th>
                        <th class="brd" style="text-align:center"><strong>Descripción</strong></th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($prestados)) 
                        @foreach($prestados as $prestadosItem)
                        <tr>
                            <th class="brd" scope="row">{{$prestadosItem->codigo }} </th>
                            <td class="brd">{{$prestadosItem->tipo}}</td>
                            <td class="brd">{{$prestadosItem->marca.'-'.$prestadosItem->modelo}}</td>
                            <td class="brd">{{$prestadosItem->descripcion}}</td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <p><strong><span>El solicitante se hace responsable de regresar los recursos prestados en las mismas condiciones que se le 
                entregaron.</span></strong></p>
            <p><span>Los recursos deben ser retornados en el transcurso de la <strong>jornada laboral de hoy</strong></span></p>
        </div><!-- /.ol-sm-12 -->
    </div><!-- /.row -->
    
    <div class="row">
        <div class="col-lg-12">
            <table style="width:100%" class="brd">
                <tbody>
                    <tr>
                        <td class="brd" style="text-align:center; height:80px;vertical-align:bottom" rowspan="1">firma del solicitante (al retirar)</td>
                        <td class="brd" style="text-align:center; height:80px;vertical-align:bottom" rowspan="1">firma y sello AST (al recibir devolución)</td>
                    </tr>
                    <tr>
                        <td class="brd" style="text-align:left; height:auto;vertical-align:bottom" colspan="2">
                            <font size="1" FACE="verdana">
                            *Recurso/s prestado por: {{ $responsable }}.<br>
                            *Al devolver los recursos entregar este comprobante al solicitante como constancia de la devolución.</font>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div><!-- /col-lg-12 -->
    </div><!-- /.row -->

    



