<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <h2 class="centeredText" style="text-align: left;">
            <span style="color: #339966;">
                <strong>Su reserva fue recibida</strong>
            </span><span style="text-decoration: underline; color: #e34f26;"><br /></span>
        </h2>
        <hr />
        <p>Hemos recibido una reserva de recursos por su parte, a la brevedad un integrante de nuestro equipo chequeará, 
            preparará los recursos, luego será notificado por este medio cuando estén listos para ser retirados o por cualquier 
            observación al respecto.
        </p>
        
        <table style="width:50%" class="brd">
            <tbody>
                <tr>
                    <td class="brd" colspan="2" style="text-align: center;">
                        <strong>Datos sobre la reserva:<br /></strong>
                    </td>
                </tr>
                <tr>
                    <td class="brd">Código de reserva:</td>
                    <td class="brd" style="text-align: center; width: 266px;">
                        <em>{{ $mensaje->id }}</em>
                    </td>
                </tr>
                <tr>
                    <td class="brd">Desde:</td>
                    <td class="brd" style="text-align: center;">{{$mensaje->fecha_desde.' - '.$mensaje->hora_desde}}</td>
                </tr>
                <tr>
                    <td class="brd">Hasta:</td>
                    <td class="brd" style="text-align: center;">{{$mensaje->fecha_hasta.' - '.$mensaje->hora_hasta}}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="brd" colspan="2" style="text-align: center;">
                        *Encontrará más información de la reserva ingresando al sistema.
                    </td>
                </tr>
            </tfoot>
        </table>
        <br>
        <table style="width:50%" class="brd">
            <thead>
                <tr>
                    <th style="text-align:center" class="brd"><strong>cant.</strong></th>
                    <th style="text-align:center" class="brd"><strong>tipo</strong></th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($reservados)) 
                    @foreach($reservados as $reservadosItem)
                    <tr>
                        <td scope="row" class="brd">{{$reservadosItem->cant }} </td>
                        <td class="brd">{{$reservadosItem->tipo}}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <h4>!- Tenga en cuenta:</h4>
        <ul>
            <li>Por favor, si no necesitará los recursos reservados cancele la reserva para que puedan ser utilizados por otra persona.</li>
            <li>El horario de atención del área de Soporte Técnico es:<strong> 09:00hs a 13:00hs</strong>, en caso de necesitar
                    retirar los recursos en otro horario, por favor contactarnos a la brevedad.</li>
            <li>Su reserva puede ser <span style="text-decoration: underline;">suspendida</span> por causas de fuerza mayor,
                    en ese caso será notificado con anticipación.</li>
        </ul>
        <p>Quedamos a su disposición por cualquier consulta.</p>
        <ul>
            <li>Nuestros medios de contacto son:
                <ul>
                    <li><span style="color: #339966;"><strong>whatsapp:</strong> </span>3813666350 </li>
                    <li><strong>Tel. fijo:</strong> 4206792 </li>
                    <li><strong>eMail:</strong> soportetecnico@educaciondigitaltuc.gob.ar</li>
                </ul>
            </li>
        </ul>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->

