<style>
    html {
        margin: 15px 10px;
    }
    .row {
        margin-right: 15px;
        margin-left: 15px;
    }
        
    .col-lg-12 {
        width: 100%;
    }
    
    .text-center {
      text-align: center;
    }
    
    .brd {
        border:1px solid black; 
        border-collapse:collapse
    }
    thead{
      font-weight: bold;
    }
</style>
    <div class="row">
        <div class="col-lg-12">
            <h2 class="centeredText" style="text-align: left;"><span style="color: #ff8800;"><strong>Reserva #{{ $mensaje->id }} SUSPENDIDA por el Administrador</strong></span><span style="text-decoration: underline; color: #e34f26;"><br /></span>
            </h2>
            <hr />
            <p>Lamentamos informarle que por causas de fuerza mayor, tuvimos que suspender su reserva #{{ $mensaje->id }}.</p>
            
            <table style="height: 164px;" width="378" class="brd">
                <tbody>
                    <tr>
                        <td class="brd" colspan="2" style="text-align: center;"><strong>Datos sobre la reserva:<br /></strong></td>
                    </tr>
                    <tr>
                        <td class="brd">Código de reserva:</td>
                        <td class="brd" style="text-align: center; width: 266px;"><em>{{ $mensaje->id }}</em></td>
                    </tr>
                    <tr>
                        <td class="brd">Desde:</td>
                        <td class="brd" style="text-align: center;">{{$mensaje->fecha_desde.' - '.$mensaje->hora_desde}}</td>
                    </tr>
                    <tr>
                        <td class="brd">Hasta:</td>
                        <td class="brd" style="text-align: center;">{{$mensaje->fecha_hasta.' - '.$mensaje->hora_hasta}}</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td class="brd" colspan="2" style="text-align: center;">
                            *Encontrará más información de la reserva ingresando al sistema.
                        </td>
                    </tr>
                </tfoot>
            </table>
            <h4>!- Tenga en cuenta:</h4>
            <ul>
                <li>Puede volver a crear la reserva con otra fecha.</li>
            </ul>
            <p>Quedamos a su disposici&oacute;n por cualquier consulta.</p>
            <ul>
                <li>Nuestros medios de contacto son:
                    <ul>
                        <li><span style="color: #339966;"><strong>whatsapp:</strong> </span>3813666350 </li>
                        <li><strong>Tel. fijo:</strong> 4206792 </li>
                        <li><strong>eMail:</strong> soportetecnico@educaciondigitaltuc.gob.ar</li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.col-lg-12 -->
    </div><!-- /.row -->

    



