@extends('layouts.super')

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">SuperAdmin</a></li>
                        <li class="breadcrumb-item active">Configuración del Sistema</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <div class="float-left">
                                <blockquote class="quote-info mt-0">
                                <h5 id="tip">Parametros del Sistema<br></p></h5>
                                *<br>
                                </blockquote>
                            </div>
                        </div><!--fin card-header-->
                        <div class="card-body">
                        <form name="configVars"  action="{{ route('config.actualizar') }}" method="POST">
                            @csrf
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td scope="row"  colspan="2"><strong>Reservas permitidas:</strong></td>
                                        <td>
                                            <div class="custom-control custom-switch custom-switch-on-success custom-switch-off-danger">
                                                <input type="checkbox" class="custom-control-input" name="swtResOn" id="swtResOn" 
                                                @if(  $config->reservas_on ) checked @endif>
                                                <label class="custom-control-label" for="swtResOn">Habilita nuevas reservas.</label>
                                                
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row" colspan="2"><strong>Anticipación minímo para reservas:</strong></td>
                                        <td>
                                            <input type="number" class="col-md-3" name="diasAntes" value="{{ $config->dias_antes }}"> 
                                            <label class="col-md-3 @error('diasAntes') is-invalid @enderror">(Días)</label>
                                            @error('diasAntes')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row" colspan="2"><strong>Uso de recursos:</strong></td>
                                        <td>
                                            <input type="number" class="col-md-3" name="diasUso" value="{{$config->dias_uso}}">
                                            <label class="col-md-3 @error('diasUso') is-invalid @enderror">(Días)</label>
                                            @error('diasUso')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Horario de atención:</strong></td>
                                        <td>
                                            <select class="form-control" name="desde_hora" >
                                                <option value="08:00:00" @if($config->desde_hora === "08:00:00") selected @endif>08:00</option>
                                                <option value="09:00:00" @if($config->desde_hora === '09:00:00') selected @endif>09:00</option>
                                                <option value="10:00:00" @if($config->desde_hora === '10:00:00') selected @endif>10:00</option>
                                                <option value="11:00:00" @if($config->desde_hora === '11:00:00') selected @endif>11:00</option>
                                                <option value="12:00:00" @if($config->desde_hora === '12:00:00') selected @endif>12:00</option>
                                                <option value="13:00:00" @if($config->desde_hora === '13:00:00') selected @endif>13:00</option>
                                                <option value="14:00:00" @if($config->desde_hora === '14:00:00') selected @endif>14:00</option>
                                                <option value="15:00:00" @if($config->desde_hora === '15:00:00') selected @endif>15:00</option>
                                                <option value="16:00:00" @if($config->desde_hora === '16:00:00') selected @endif>16:00</option>
                                                <option value="17:00:00" @if($config->desde_hora === '17:00:00') selected @endif>17:00</option>
                                                <option value="18:00:00" @if($config->desde_hora === '18:00:00') selected @endif>18:00</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control col-md-4 @error('hasta_hora') is-invalid @enderror" name="hasta_hora">
                                                <option value="10:00:00" @if($config->hasta_hora === '10:00:00') selected @endif>10:00</option>
                                                <option value="11:00:00" @if($config->hasta_hora === '11:00:00') selected @endif>11:00</option>
                                                <option value="12:00:00" @if($config->hasta_hora === '12:00:00') selected @endif>12:00</option>
                                                <option value="13:00:00" @if($config->hasta_hora === '13:00:00') selected @endif>13:00</option>
                                                <option value="14:00:00" @if($config->hasta_hora === '14:00:00') selected @endif>14:00</option>
                                                <option value="15:00:00" @if($config->hasta_hora === '15:00:00') selected @endif>15:00</option>
                                                <option value="16:00:00" @if($config->hasta_hora === '16:00:00') selected @endif>16:00</option>
                                                <option value="17:00:00" @if($config->hasta_hora === '17:00:00') selected @endif>17:00</option>
                                                <option value="18:00:00" @if($config->hasta_hora === '18:00:00') selected @endif>18:00</option>
                                            </select>
                                            @error('hasta_hora')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-right">Guardar cambios</button>
                        </div><!--fin card-footer-->
                        </form>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@if( NULL !== (session('result')) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! intente nuevamente.');
        break;
    }
</script>
@endif    


@endsection   
