@extends('layouts.super')
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">SuperAdmin</a></li>
                        <li class="breadcrumb-item active">Reservas</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-2">
                    <a id='btnExporta' class="btn btn-block btn-sm btn-primary float-right">
                        <i class="fas fa-print"></i>  Exportar listado
                    </a>    
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card"><!-- card -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="Reservas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="reservas">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1" cellpadding="1">
                                                    Código
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha Creada
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Solicitante
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha desde
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha hasta
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Estado
                                                </th>
                                                <th tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1" data-orderable="false" >
                                                    Opciones
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($reservas as $reservasItem)
                                            <tr>
                                                <th scope="row">{{$reservasItem->id}}</th>
                                                <td>{{$reservasItem->created_at}}</td>
                                                <td>{{$reservasItem->user_id}}</td>
                                                <td>{{$reservasItem->fecha_desde}} - {{$reservasItem->hora_desde}}
                                                </td>
                                                <td>{{$reservasItem->fecha_hasta}} - {{$reservasItem->hora_hasta}}
                                                </td>
                                                <td>
                                                    <a id="estado" name="{{$reservasItem->id}}" class="btn btn-block btn-sm estado
                                                    @switch($reservasItem->est_reserv_id)
                                                        @case('Confirmada')
                                                            btn-outline-primary
                                                            @break
                                                        @case('En Curso') 
                                                            btn-outline-info
                                                            @break
                                                        @case('Preparada') 
                                                            btn-outline-success
                                                            @break
                                                        @case('Vencida') 
                                                            btn-outline-danger
                                                            @break
                                                        @case('Suspendida') @case('Cancelada')
                                                            btn-outline-warning
                                                            @break
                                                        @default
                                                            btn-outline-secondary
                                                    @endswitch
                                                    ">{{$reservasItem->est_reserv_id}}
                                                    </a>
                                                </td>
                                                <td>
                                                    <form method="POST" action="{{ route('reserva.eliminar', $reservasItem->id) }}">
                                                    <a class="btn btn-default" href="{{ route('reserva.verAdm',$reservasItem->id) }}" title="Ver">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    <a class="btn btn-info" href="{{ route('pdf.actaReserva',$reservasItem->id) }}" title="Imprimir">
                                                      <i class="fas fa-print">  </i>
                                                    </a>
                                                        @csrf @method('DELETE')
                                                        <a id="eliminar" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

@if( NULL !== (session('result')) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! intente nuevamente.');
        break;
    }
</script>
@endif

<script type="text/javascript">
$("#btnExporta").click(function(e){
    url = '{{ route('pdf.reservasAdm') }}';
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrrado');
                    Swal.close();
                }
            })
        }
    })
});

$('#estado*').click(function(e){
    e.preventDefault();
    Swal.fire({
        title: '¿Cambiar estado de la reserva?',
        input: 'select',
        inputOptions: {
            '1': 'Confirmada',
            '2': 'En Curso',
            '3': 'Preparada',
            '4': 'Vencida',
            '5': 'Suspendida',
            '6': 'Finalizada'
        },
        inputPlaceholder: 'Seleccione',
        showCancelButton: true,
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value !== '') {
                    resolve();
                } else {
                    resolve('Antes, debe elegir un estado para cambiar');
                }
            });
        }
    }).then((result) => {
        if (result.isConfirmed) {
            window.location = 'cambiaEstado'+this.name+'/'+result.value;
        }
    });
} );

$('#eliminar*').click(function(e){
    e.preventDefault();
    var form =  $(this).closest("form");
    Swal.fire({
        title: '¿Está seguro de elimnar la reserva?',
        text: "No podrá recuperarlo",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.isConfirmed) {
            form.submit();
        }
    });
});

$('#Reservas').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 5,
    "order": [[ 1, "desc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );

</script>

@endsection
