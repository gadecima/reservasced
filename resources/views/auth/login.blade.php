<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema de Reservas | Login </title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="adminlte/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a class="h1"><b>Reserva de Recursos</b></a>
                <h3>Coordinación de Educación Digital</h3>
            </div>
            <div class="card-body">
            <p class="login-box-msg">Ingresar al sistema</p>
            <form action="{{route('login')}}" method="post">
                @csrf
                <div class="input-group mb-3">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="correo electrónico">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                <div class="input-group-append">
                    <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                    </div>
                </div>
                </div>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">{{ __('Recuerdame') }}</label>
                        </div>
                    </div>
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col">
                        @if (Route::has('password.request'))
                        <a type="button" href="{{ route('password.request') }}" class="btn btn-block btn-outline-info btn-xs">
                            ¿Olvidaste tu contraseña?
                        </a>
                        @endif
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-block btn-outline-info btn-xs" data-toggle="modal" href="" data-target="#solicitudNuevoUsr">
                            Solicitud de nuevo usuario
                        </button>
                    </div>
                </div>
            </form>
            </div><!-- /.card-body -->
        </div><!-- /.card -->
    </div><!-- /.login-box -->

    
    <!-- Modal -->
    <div class="modal fade" id="solicitudNuevoUsr" tabindex="-1" role="dialog" aria-labelledby="solicitudNuevoUsrlLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Solicitud de nuevo usuario</h4>
                </div>
                <div class="modal-body">
                    <p>Para solicitad el alta en el sistema de <strong> Reserva de Recursos</strong> de la <strong> Coordinación de Educación Digital</strong>,
                        Contáctanos: <br> <br>
                        Via whatsap al <strong> 03813666350 </strong> o a nuestra casilla de correo <strong> soportetecnico@educaciondigitaltuc.gob.ar </strong>
                        
                    </p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Entendido!</button>
                
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="adminlte/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="adminlte/dist/js/adminlte.min.js"></script>

</body>
</html>