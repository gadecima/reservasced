@extends('layouts.usuario')
@csrf
@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item ">Reservas</li>
                        <li class="breadcrumb-item active">Nueva Reserva</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                            <h5 id="tip">A tener en cuenta:</h5>
                            <p>* Los horarios de atención del Área de Soporte Técnico son de Lunes a Viernes de 09:30 a 17 hs.<br>
                            * Las reservas se deben realizar con un mínimo de 48 hs antes del evento.<br>
                            * En el caso de que se requiera la instalación de software especifico debe realizar la reserva con 72 hs de anticipación.
                            * Realizar las reservas con criterio, recuerde que los recursos son de todos y debemos compartirlos, no reserve recursos que no utilizará.<br>
                            * En caso de ser necesaria alguna observación sobre su reserva, le contactaremos a la brevedad.<br>
                            * En el caso de reservas fuera del horario del Área, deberá pasar a retirar los recursos durante la jornada disponible anterior al evento.<br></p>
                            </blockquote>

                            <h4>Seleccione el rango de fecha y horario el que necesita reservar los recursos.</h4>
                            <form name="selecRangoFecha"  action="{{ route('reserva.selecRecursos') }}" method="POST">
                            @csrf
                            <!-- Input: selectores de fecha -->
                            <div class="row"> 
                                <div class="col-5 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{{'Desde: '}}</span>
                                    </div>
                                    <input type="text" name="dt_desde_fecha" class="form-control float-right" id="dt_desde_fecha" required="" onkeydown="return false">
                                </div>
                                <div class="col"></div>
                                <div class="col-5 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{{'Hasta: '}}</span>
                                    </div>
                                    <input type="text" name="dt_hasta_fecha" class="form-control float-right" id="dt_hasta_fecha" required="" onkeydown="return false">
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#dt_desde_fecha').datetimepicker({
                                                daysOfWeekDisabled: [0, 6],
                                                //minDate: moment().add(2,'days'),
                                                format: 'DD-MM-YYYY',
                                                locale: 'es-MX',
                                                //format: 'L'
                                            }).on("dp.show", function (){
                                                $('#dt_desde_fecha').data("DateTimePicker")
                                                    .minDate(moment().add({{ $config->dias_antes }},'days'))
                                            })
                                            .on("dp.change", function (e){
                                                $('#dt_hasta_fecha').val('');
                                                $('#dt_hasta_fecha').data("DateTimePicker").maxDate(moment(e.date).add({{ $config->dias_uso }},'days')),
                                                $('#dt_hasta_fecha').data("DateTimePicker").minDate(moment(e.date))
                                            });
                                        });
                                        $(function () {
                                            $('#dt_hasta_fecha').datetimepicker({
                                                daysOfWeekDisabled: [0, 6],
                                                format: 'DD-MM-YYYY',
                                                locale: 'es-MX',
                                                //format: 'L'
                                            });
                                        });
                                    </script>
                                </div>
                            <!--FIN: Input: selectores de fecha -->
                            </div>
                            <!-- Input: selectores de HORA -->
                            <div class="row"> 
                                <div class="col-5 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{{'Desde: '}}</span>
                                    </div>
                                    <input type="text" name="dt_desde_hora" class="form-control float-right" id="dt_desde_hora" required="" onkeydown="return false">
                                </div>
                                <div class="col"></div>
                                <div class="col-5 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">{{'Hasta: '}}</span>
                                    </div>
                                    <input type="text" name="dt_hasta_hora" class="form-control float-right" id="dt_hasta_hora" required="" onkeydown="return false">
                                </div>
                            <script type="text/javascript">
                                $(function () {
                                    var array = {{ $horasAten }};
                                    $('#dt_desde_hora').datetimepicker({                          
                                        format: 'HH:MM',
                                        enabledHours: array,
                                        locale: 'es-MX',
                                        format: 'LT',
                                        stepping: 30
                                        //disabledTimeIntervals: [[moment({ h: 0 }), moment({ h: 8 })], [moment({ h: 18 }), moment({ h: 24 })]],
                                        //enabledHours: [9, 10, 11, 12, 13, 14, 15],
                                        //disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 16, 17, 18, 19, 20, 21, 22, 23, 24],
                                    });
                                });
                          
                                $(function () {
                                    var array = {{ $horasAten }};
                                    $('#dt_hasta_hora').datetimepicker({ 
                                        format: 'HH:MM',
                                        enabledHours: array,
                                        locale: 'es-MX',
                                        format: 'LT',
                                        stepping: 30
                                    });
                                });
                            </script>
                            </div>
                            <!--FIN: Input: selectores de HORA -->
                        </div> <!-- fin card-body-->
                        <div class="card-footer">
                            <div class="col-md-6">
                                <a href="javascript:history.back()" class="btn bg-gradient-primary float-left">Volver</a>
                            </div>
                            @if( $config['reservas_on'])
                                <button type="submit" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-right">Seleccionar Recursos</button>
                            @else
                                <div class="alert alert-warning alert-dismissible">
                                    <h5><i class="icon fas fa-exclamation-triangle"></i> Reservas suspendidas temporalmente!</h5>
                                </div>
                            @endif
                        </div>
                        </form>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@if(session('error') == 1)
    <script type="text/javascript">
        Swal.fire({
            title: 'info!',
            text: 'Debe seleccionar por lo menos un recurso a reservar',
            icon: 'info',
            confirmButtonText: 'Volver'
        });
    </script>
@endif


<script type="text/javascript">
    $(document).ready(function(){
        $('#dt_desde_fecha').val('');
        $('#dt_hasta_fecha').val('');
        $('#dt_desde_hora').val('');
        $('#dt_hasta_hora').val('');
    });
</script>
@endsection

