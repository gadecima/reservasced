@extends('layouts.usuario')


@section('contenido')

<div class="content-wrapper">
    <div class="content-header"> <!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item active">Reservas</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col"><!-- /.col -->
                    <div class="float-right">
                        @if( $config['reservas_on'])
                        <a href="{{ route('reserva.nueva') }}" class="btn btn-success">
                            <i class="fas fa-plus"></i> Nueva Reserva
                        </a>
                        @else
                        <a class="btn btn-warning" style="cursor: default;">
                            <i class="fas fa-exclamation-triangle"></i> Reservas suspendidas temporalmente!
                        </a>
                        @endif
                        <a id='btnExporta' class="btn btn-info">
                            <i class="fas fa-print"></i> Exportar Listado
                        </a>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div> <!-- /.content-header -->

    <div class="content"><!-- Main content -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-success"><i class="fas fa-dolly"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Reservas listas para retirar</span>
                            <span class="info-box-number">{{ $resListas }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-info"><i class="fas fa-calendar-check"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Reservas concluidas.</span>
                            <span class="info-box-number">{{ $resConcluidas }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger"><i class="fas fa-calendar-times"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Reservas a devolver</span>
                            <span class="info-box-number">{{ $resAdeuda }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- tabla de Usuario - Mis Reservas -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="MisReservas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Código Reserva
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha Creada
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha desde
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha hasta
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Estado
                                                </th>
                                                <th tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1" data-orderable="false" >
                                                    Opciones
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($reservUser as $reservUserItem)
                                              <tr>
                                                <th scope="row">
                                                    {{$reservUserItem->id}} 
                                                </th>
                                                <td>
                                                    {{$reservUserItem->created_at}}
                                                </td>
                                                <td>
                                                    {{$reservUserItem->fecha_desde}} - {{$reservUserItem->hora_desde}}
                                                </td>
                                                <td>
                                                    {{$reservUserItem->fecha_hasta}} - {{$reservUserItem->hora_hasta}}
                                                </td>
                                                <td>
                                                    <a  class="btn btn-block btn-sm 
                                                    @switch($reservUserItem->est_reserv_id)
                                                        @case('Confirmada') @case('En Curso') 
                                                            btn-outline-primary
                                                            @break
                                                        @case('Preparada') 
                                                            btn-outline-success
                                                            @break
                                                        @case('Suspendida') @case('Adeuda')
                                                            btn-outline-danger
                                                            @break
                                                        @case('Cancelada') 
                                                            btn-outline-warning
                                                            @break
                                                        @default
                                                            btn-outline-secondary
                                                    @endswitch
                                                    "style="cursor: default;">{{$reservUserItem->est_reserv_id}}
                                                    </a>
                                                </td>
                                                <td>
                                                    <!-- Boton VER detalles -->
                                                    <a class="btn btn-default" href="{{ route('reserva.ver',$reservUserItem->id)}}" title="Ver">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    <!--FIN: Boton VER detalles -->
                                                    <!-- Boton EXPORTAR ACTA -->
                                                    @switch($reservUserItem->est_reserv_id)
                                                        @case('Preparada')@case('En Curso')
                                                            <a class="btn btn-info" href="{{route('pdf.actaReserva',$reservUserItem->id)}}" title="Exportar Acta">
                                                                <i class="fas fa-print"></i>
                                                            </a>
                                                        @break
                                                    @endswitch
                                                    <!--FIN: Boton EXPORTAR ACTA -->
                                                    <!-- Boton CANCELAR reserva -->
                                                    @switch($reservUserItem->est_reserv_id)
                                                        @case('Confirmada')@case('Preparada')
                                                            <a class="btn btn-danger cancelar" href="{{route('reserva.cancelar',$reservUserItem->id)}}" title="Cancelar">
                                                                <i class="fas fa-ban"></i>
                                                            </a>
                                                            @break
                                                    @endswitch
                                                    <!--FIN: Boton CANCELAR reserva -->
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                        <div class="card-footer clearfix"> <!-- card-footer -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="width:100%">
                                        <tbody>
                                            <tr>
                                                <td><span  class="badge badge-primary">Confirmada:</span> Su reserva está en preparación, le notificaremos cuando estén listos para retirar.</td>
                                                <td><span  class="badge badge-success">Preparada:</span> Sus recursos se encuentran listos para ser retirados.</td>
                                            </tr>
                                            <tr>
                                                <td><span  class="badge badge-danger">Adueda:</span> Debe devolver los recursos de ésta reserva lo antes posible.</td>
                                                <td><span  class="badge badge-warning">Cancelada:</span> Haz cancelado ésta reserva.</td>
                                            </tr>
                                            <tr>
                                                <td><span  class="badge badge-danger">Suspendida:</span>Un Administrador ha suspendido tu reserva. </td>
                                                <td><span  class="badge badge-secondary">Vencida:</span>No has retirado los recursos de esta reserva.</td>
                                            </tr>
                                            <tr>
                                                <td><span  class="badge badge-secondary">Finalizada:</span> Reserva terminada y cerrada. </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>  <!-- /.card-footer -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Captura de mensajes de session para trabajar los Alerts -->
@if( NULL !== (session('result')) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! intente nuevamente.');
        break;
    }
</script>
@endif    

@if( 1 == ( session('cancelada')  ?? 0 ))
<script type="text/javascript">
    Swal.fire(
        'Reserva Cancelada!',
        'Haz cancelado la reserva',
        'success')
</script>
@endif

<script type="text/javascript">
//script-alert-cancelar
$('.cancelar').click(function(e){
    e.preventDefault();
    Swal.fire({
        title: '¿Está seguro de cancelar la reserva?',
        text: "No podrá recuperarla",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, cancelar!',
        cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location = this.href;
            }
        })
}) //FIN: script-alert-cancelar

$("#btnExporta").click(function(e){
    url = '{{ route('pdf.reservasUsr') }}';
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrado');
                    Swal.close();
                }
            })
        }
    })
});

//script-DataTable
$('#MisReservas').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 5,
    "order": [[ 0, "desc" ]],
    language: {
        "search": "Buscar:",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Última",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );//FIN: script-DataTable


</script>

@endsection

