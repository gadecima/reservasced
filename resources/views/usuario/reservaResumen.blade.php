@extends('layouts.usuario')
@csrf
@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item ">Reservas</li>
                        <li class="breadcrumb-item active">Nueva Reserva</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <form name="recReservados" action="{{ route('reserva.confirmada') }}" method="POST">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">A continuación se listan los recursos seleccionados:</h5>
                                <p>{{ 'Desde: '.$dt_desde_fecha.' - '.$dt_desde_hora.', hasta: '.$dt_hasta_fecha.' - '.$dt_hasta_hora }}</p>
                                <p>Verifique y confirme la reserva. <br></p>
                            </blockquote>
                            @csrf
                            <table class="table table-bordered">
                                <tbody>
                                    @if(!empty($selNbk)) 
                                    <tr>
                                        <th style="width:50%">Netbooks</th>
                                        <td>{{$selNbk}}</td>
                                    </tr>
                                    @endif
                                    @if(!empty($selTbl)) 
                                    <tr>
                                        <th style="width:50%">Tablets</th>
                                        <td>{{$selTbl}}</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">tipo</th>
                                        <th scope="col">Marca/Modelo</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($reservados)) 
                                        @foreach($reservados as $reservadosItem)
                                            @if ($reservadosItem->tipo <> 'Tablet' && $reservadosItem->tipo <> 'netbook')
                                                <tr>
                                                    <td>{{$reservadosItem->tipo}}</td>
                                                    <td>{{$reservadosItem->marca.'-'.$reservadosItem->modelo}}</td>
                                                    <td>{{$reservadosItem->cant}}</td>
                                                    <td>{{$reservadosItem->descripcion}}</td>
                                                </tr>
                                            @endif
                                        <input type="hidden"  name="reservados[]" value="{{$reservadosItem->id }}">
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>

                            <dl class="row">
                                <dt class="col-sm-3">Observaciones:</dt>
                                <dd class="col-sm-9">{{$observ}}</dd>
                            </dl>
                            <input type="hidden"  name="dt_desde_fecha" value="{{$dt_desde_fecha}}">
                            <input type="hidden"  name="dt_hasta_fecha" value="{{$dt_hasta_fecha}}">
                            <input type="hidden"  name="dt_desde_hora" value="{{$dt_desde_hora}}">
                            <input type="hidden"  name="dt_hasta_hora" value="{{$dt_hasta_hora}}">
                            <input type="hidden"  name="observ" value="{{$observ }}">
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver para modificar</a>
                            <button type='submit' id="btnConfirma" class="btn btn-block bg-gradient-success btn-sm col-5 float-sm-right">Confirmar</button>
                        </div><!--fin cardfooter-->
                    </div><!-- FIN CARD -->
                </div><!-- FIN col-9 -->
                </form>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$("#btnConfirma").click(function(e){
    //e.preventDefault();
    $("#btnConfirma").addClass('disabled');

    Swal.fire({
        title: 'Reservando recursos...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
        }
    });
})
</script>

@endsection


