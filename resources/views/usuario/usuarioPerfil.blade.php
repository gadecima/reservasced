@extends(Auth::user()->rol_id == 1 ? 'layouts.admin' : ((Auth::user()->rol_id == 2) ? 'layouts.usuario' : 'layouts.usuario'))
@csrf

@section('contenido')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Principal</a></li>
                        <li class="breadcrumb-item ">Usuario</li>
                        <li class="breadcrumb-item active">Editar perfil</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <form method="POST" action="{{ route('usuario.perfil.actualizar') }}">
                    @csrf @method('PATCH')
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <div class="float-left">
                                <blockquote class="quote-info mt-0">
                                <h5 id="tip">Editar Perfil de Usuario <br></p></h5>
                                -Al modificar sus datos, considere:<br>
                                *Utilizar un eMail conocido, este será su usuario del sistema y donde recibirá todas las notificaciones.<br>
                            </blockquote>
                            </div>
                        </div><!--fin card-header-->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td scope="row"><strong>eMail </strong>(nombre de usuario):</td>
                                        <td>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$usuario->email}}" required autocomplete="email"  autofocus>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Apellido/s:</strong></td>
                                        <td>
                                            <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ $usuario->apellido }}" required autocomplete="apellido">
                                            @error('apellido')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Nombre/s:</strong></td>
                                        <td><input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ $usuario->nombre }}" required autocomplete="nombre">
                                        @error('nombre')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>DNI:</strong> (solo números)</td>
                                        <td>
                                            <input id="dni" type="text" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{$usuario->dni}}" required autocomplete="dni">
                                            @error('dni')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Teléfono: </strong> (solo números)</td>
                                        <td>
                                            <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{$usuario->telefono}}" required autocomplete="telefono">
                                            @error('telefono')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Cargo:</strong></td>
                                        <td>
                                            <input id="cargo" type="text" class="form-control @error('cargo') is-invalid @enderror" name="cargo" value="{{ $usuario->cargo}}" autocomplete="cargo">
                                            @error('cargo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td scope="row"><strong>Área:</strong></td>
                                        <td>
                                            <input id="ofi_area" type="text" class="form-control @error('ofi_area') is-invalid @enderror" name="ofi_area" value="{{ $usuario->ofi_area}}" autocomplete="ofi_area">
                                            @error('ofi_area')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                        <td scope="row"><strong>Dependencia:</strong></td>
                                        <td>
                                            <input id="dependencia" type="text" class="form-control @error('dependencia') is-area @enderror" name="dependencia" value="{{ $usuario->dependencia}}" autocomplete="dependencia">
                                            @error('dependencia')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            <button type="submit" class="btn btn-block bg-gradient-success btn-sm col-5 float-sm-right"> Guardar cambios </button>
                        </div><!--fin card-footer-->
                    </div><!-- FIN CARD -->
                    </form>
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->
@endsection