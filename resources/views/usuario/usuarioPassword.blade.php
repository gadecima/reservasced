@extends(Auth::user()->rol_id == 1 ? 'layouts.admin' : ((Auth::user()->rol_id == 2) ? 'layouts.usuario' : 'layouts.super'))
@csrf

@section('contenido')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item">Cambiar Contraseña</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <form method="POST" action="{{ route('usuario.pass.actualizar') }}">
                    @csrf @method('PATCH')
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">Cambiar contraseña<br></p></h5>
                                <ul> 
                                    <li>Utilizar una contraseña que le sea facíl de recordar.</li>
                                    <li>Con un mínimo de 8 caracteres</li>
                                </ul>
                            </blockquote>
                        </div><!--fin card-header-->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td><strong>Contraseña actual:</strong></td>
                                        <td>
                                            <input id="passVieja" type="password" class="form-control @error('passVieja') is-invalid @enderror" name="passVieja">
                                            @error('passVieja')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Contraseña nueva:</strong></td>
                                        <td>
                                            <input id="passNueva" type="password" class="form-control @error('passNueva') is-invalid @enderror" name="passNueva">
                                            @error('passNueva')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Repita contraseña nueva:</strong></td>
                                        <td>
                                            <input id="passNueva2" type="password" class="form-control @error('passNueva2') is-invalid @enderror" name="passNueva2">
                                            @error('passNueva2')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            <button type="submit" class="btn btn-block bg-gradient-success btn-sm col-5 float-sm-right"> Guardar cambios </button>
                        </div><!--fin card-footer-->
                    </div><!-- FIN CARD -->
                    </form>
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->
@endsection