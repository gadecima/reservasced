@extends('layouts.usuario')
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item ">Reservas</li>
                        <li class="breadcrumb-item active">Nueva Reserva</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                            <h5 id="tip">Reserva Confirmada! <br></p>
                            <h4>Su código de reserva es: #{{ $reservaID }}</h4>
                            <b>Se ha enviado un correo a su casilla con los detalles de la reserva.</b>
                            </blockquote>
                        </div> <!-- fin card-body-->
                        <div class="card-footer">
                            <a href="{{ route('reservas.usuario') }}" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">
                                <i class="fas fa-home">  </i>  Volver al principio
                            </a>
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@endsection
