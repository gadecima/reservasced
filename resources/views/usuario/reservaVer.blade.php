@extends('layouts.usuario')
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item ">Reservas</li>
                        <li class="breadcrumb-item active">Ver Reserva</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">Reserva #{{$reserva->id}}<br></p></h5>
                                <ul> 
                                    <li>Del {{$reserva->fecha_desde}} - {{$reserva->hora_desde}} al {{$reserva->fecha_hasta}} - {{$reserva->hora_hasta}} </li>
                                    <li>Creada el: {{$reserva->created_at}} </li>
                                    <li>Creado por: {{$creador->apellido}}, {{$creador->nombre}}</li>
                                    <li>Estado: {{ $estado_reserv }} </li>
                                </ul>
                            </blockquote>
                        @switch($estado_reserv)
                            @case('Confirmada')@case('Suspendida')@case('Cancelada')@case('Vencida')
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align:center"><strong>cantidad.</strong></th>
                                            <th style="text-align:center"><strong>Tipo</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($resumen)) 
                                            @foreach($resumen as $resumenItem)
                                            <tr>
                                                <td scope="row" >{{$resumenItem->cant }} </td>
                                                <td scope="row" >{{$resumenItem->tipo}}</td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Observaciones:</th>
                                            <td>{{$reserva->observ}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            @break
                            @default
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Código</th>
                                            <th scope="col">Tipo</th>
                                            <th scope="col">Marca/Modelo</th>
                                            <th scope="col">Descripción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($reservados)) 
                                            @foreach($reservados as $reservadosItem)
                                                <tr>
                                                    <th scope="row">{{$reservadosItem->codigo }} </th>
                                                    <td>{{$reservadosItem->tipo}}</td>
                                                    <td>{{$reservadosItem->marca.'-'.$reservadosItem->modelo}}</td>
                                                    <td>{{$reservadosItem->descripcion}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Observaciones:</th>
                                            <td colspan="3">{{$reserva->observ}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            @break
                        @endswitch
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            @if ( in_array($reserva->est_reserv_id, array(2,3,8) , true) )
                            <a href="{{ route('pdf.actaReserva',$reserva->id) }}" class="btn btn-block bg-gradient-info btn-sm col-5 float-sm-right">Imprimir</a>
                            @endif
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@endsection
