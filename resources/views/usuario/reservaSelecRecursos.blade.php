@extends('layouts.usuario')
@csrf
@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item">Principal</a></li>
                        <li class="breadcrumb-item "><a href="{{ route('home') }}">Reservas</a></li>
                        <li class="breadcrumb-item">Nueva Reserva</li>
                        <li class="breadcrumb-item active">Selec Recursos</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

  <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
        <div class="row">
            <div class="col"></div>
            <div class="col-9">
                <blockquote class="quote-info mt-0">
                    <h5 id="tip">Se muestran los recursos disponibles en el rango:</h5>
                    <p>{{ 'Desde: '.$dt_desde_fecha.' - '.$dt_desde_hora.
                            ', hasta: '.$dt_hasta_fecha.' - '.$dt_hasta_hora }}</p>
                </blockquote>
                <form id="theForm"  action="{{ route('reserva.resumen') }}" method="POST">
                @csrf
                <input name="dt_desde_fecha" type="hidden" value="{{$dt_desde_fecha}}">
                <input name="dt_hasta_fecha" type="hidden" value="{{$dt_hasta_fecha}}">
                <input name="dt_desde_hora" type="hidden" value="{{$dt_desde_hora}}">
                <input name="dt_hasta_hora" type="hidden" value="{{$dt_hasta_hora}}">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Seleccione Recursos a reservar</h3>
                    </div><!-- /.card-header -->
                    <div class="card-body"><!-- card-body -->
                        <!-- CardCollapsive: Netbooks & Tablets -->
                        <div id="accordion">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                        <a class="d-block w-100" data-toggle="collapse" href="#collapse1" >Netbooks & Tablets</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="collapse hide" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="input-group col-md-6">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Netbooks</span>
                                                </div>
                                                <input type="number" class="form-control col-md-6" placeholder="{{$cantNbksDispon}} disponibles" min="1" max="{{ $cantNbksDispon }}" 
                                                    name="selNbk">
                                            </div>
                                            <div class="input-group col-md-6">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Tablets</span>
                                                </div>
                                                <input type="number" class="form-control col-md-6" placeholder="{{$cantTblDispon}} disponibles" min="1" max="{{ $cantTblDispon }}" 
                                                    name="selTbl">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--FIN: CardCollapsive: Netbooks & Tablets  -->

                        <!-- CardCollapsive: Recursos Audio-Video -->
                        <div id="accordion">
                            <div class="card card-primary"> 
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                        <a class="d-block w-100" data-toggle="collapse" href="#collapse2" >Recursos (Proyector, Sonido, Robótica...)</a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="collapse hide" data-parent="#accordion">
                                    <div class="card-body">
                                        <table id="dtRecursos" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Recursos">
                                            <thead>
                                                <tr role="row">
                                                    <th tabindex="0" aria-controls="Recursos" rowspan="1" colspan="1" cellpadding="1" data-orderable="false">
                                                        #
                                                    </th>
                                                    <th class="sorting" tabindex="1" aria-controls="Recursos" rowspan="1" colspan="1">
                                                        Tipo
                                                    </th>
                                                    <th class="sorting" tabindex="2" aria-controls="Recursos" rowspan="1" colspan="1">
                                                        Marca
                                                    </th>
                                                    <th class="sorting" tabindex="3" aria-controls="Recursos" rowspan="1" colspan="1">
                                                        Descripción
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($recursos as $recursosItem)
                                                <tr>
                                                    <td>{{$recursosItem->id }}</td>
                                                    <td>{{$recursosItem->tipo}}</td>
                                                    <td>{{$recursosItem->marca}}</td>
                                                    <td>{{$recursosItem->descripcion}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--FIN: CardCollapsive: Netbooks & Tablets  -->

                        <!-- CardCollapsive: accesorios -->
                        <div id="accordion">
                            <div class="card card-primary"> 
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                      <a class="d-block w-100" data-toggle="collapse" href="#collapse3" >Accesorios (Prolong. eléctricos, Adaptadores, Cables...)</a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="collapse hide" data-parent="#accordion">
                                    <div class="card-body">
                                        <table id="dtAccesorios" class="display table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Accesorios">
                                            <thead>
                                                <tr role="row">
                                                    <th class="sorting" tabindex="3" aria-controls="Accesorios" rowspan="1" colspan="1">
                                                        Tipo
                                                    </th>
                                                    <th class="sorting" tabindex="2" aria-controls="Accesorios" rowspan="1" colspan="1">
                                                        descripcion
                                                    </th>
                                                    <th class="sorting" tabindex="4" aria-controls="Accesorios" rowspan="1" colspan="1" data-orderable="false">
                                                        Seleccione (ver cant. dispon)
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($accesorios as $key => $value)
                                                <tr>
                                                    <td>{{$value->tipo}}</td>
                                                    <td>{{$value->desc}}</td>
                                                    <td>
                                                        <input type="hidden"  name="accs[{{$key}}][recuId]" value="{{$value->id }}"> 
                                                        <input type="number" class="form-control" placeholder="{{ $value->dispon }} disponibles" min="0" 
                                                            max="{{ $value->dispon }}" name="accs[{{$key}}][cantSel]">
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--FIN: CardCollapsive: accesorios  -->

                        <!-- CardCollapsive: Observaciones -->
                        <div id="accordion">
                            <div class="card card-primary"> 
                                <div class="card-header">
                                    <h4 class="card-title w-100">
                                        <a class="d-block w-100" data-toggle="collapse" href="#collapse4">Observaciones</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="collapse hide" data-parent="#accordion">
                                    <div class="card-body">
                                        <textarea class="form-control" rows="3" placeholder="detalle, por ej: si necesita algún software específico pre-instalado en las netbooks o tablets, o cualquier observacion que necesite realizar sobre esta reserva." name="observ"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--FIN: CardCollapsive: Observaciones -->


                    </div><!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-4">
                                <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-block bg-gradient-primary btn-sm float-sm-right" id="btn_cont">Continuar</button>
                            </div>
                        </div>
                    </div><!-- /.card-footer -->
                    </form>
                </div>
            </div> <!-- /.col-9 -->
            <div class="col"></div>
        </div><!-- /.row -->
    </div><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$(document).ready(function (){
    $('#theForm').on('submit', function(e){
        var form = this;
        var rows_selected = table.column(0).checkboxes.selected();

        // Iterate over all selected checkboxes
        $.each(rows_selected, function(index, rowId){
            $(form).append(                // Create a hidden element
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'recus[]')
                    .val(rowId)
            );
        });
    });

    var table = $('#dtRecursos').DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "pageLength": 5,
            "order": [[ 1, "desc" ]],
            language: {
                "search": "Buscar:",
                "decimal": ",",
                "emptyTable": "No hay datos disponibles en la Tabla",
                "infoThousands": ".",
                "lengthMenu": "Mostrar _MENU_ entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
                "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
                "paginate": {
                    "first": "Primera",
                    "last": "Ultima",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
                "aria": {
                    "sortAscending": ": orden ascendente",
                    "sortDescending": ": orden descendente"
                },
            },
            'columnDefs': [{
                'targets': 0,
                'checkboxes': {'selectRow': true}
            }],
            'select': {'style': 'multi'},
            'order': [[1, 'asc']]
    });

    var tableAccs = $('#dtAccesorios').DataTable( {
        scrollY: 300,
        paging:  false,
        keys:    true,
        "responsive": true,
        "lengthChange": false,
        "autoWidth": true,
        "order": [[ 0, "desc" ]],
        language: {
            "search": "Buscar:",
            "decimal": ",",
            "emptyTable": "No hay datos disponibles en la Tabla",
            "infoThousands": ".",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
            "paginate": {
                "first": "Primera",
                "last": "Ultima",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": orden ascendente",
                "sortDescending": ": orden descendente"
            },
        }
    } );

});

/*
$('#btn_cont').click(function(e){
    e.preventDefault();
    var numberSum = 0;
    var checksOn = $('input[type=checkbox]:checked').length;
    $('input[type=number]').each(
        function(){
            numberSum += Number($(this).val());
        }
    )
    if (numberSum == 0 && !checksOn ) {
        Swal.fire({
            title: 'info!',
            text: 'Debe seleccionar por lo menos un recurso a reservar',
            icon: 'info',
            confirmButtonText: 'Volver'
        });
    }else{
        $("#selecRecursos").submit();
    }
})
*/
</script>

@endsection
