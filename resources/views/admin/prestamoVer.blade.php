@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item ">Prestamo</li>
                        <li class="breadcrumb-item active">Ver Prestamo</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">Prestamo #{{$prestamo->id}}<br></h5>
                                <ul> 
                                    <li>Solicitante: {{$prestamo->solicitante}}</li>
                                    <li>Prestado: {{$prestamo->fecha_prestado}}</li>
                                    <li>Estado: {{ $prestamo->estado }}</li>
                                    <li>Devuelto: {{$prestamo->fecha_devuelto}}</li>
                                </ul>
                            </blockquote>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">tipo</th>
                                        <th scope="col">Marca/Modelo</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($prestados)) 
                                        @foreach($prestados as $prestamosItem)
                                        <tr>
                                            <th scope="row">{{$prestamosItem->codigo }} </th>
                                            <td>{{$prestamosItem->tipo}}</td>
                                            <td>{{$prestamosItem->marca.'-'.$prestamosItem->modelo}}</td>
                                            <td>{{$prestamosItem->cant}}</td>
                                            <td>{{$prestamosItem->descripcion}}</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="callout callout-info">
                                        <h5>Comentarios del Administrador</h5>
                                        <p>{{$prestamo->comentario}}</p>
                                    </div>
                                </div>
                            </div>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            <a href="{{ route('pdf.actaPrestamo',$prestamo->id) }}" class="btn btn-block bg-gradient-info btn-sm col-5 float-sm-right">Imprimir</a>
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@endsection
