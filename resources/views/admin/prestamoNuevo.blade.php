@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Admin</a></li>
                        <li class="breadcrumb-item">Prestamo Express</li>
                        <li class="breadcrumb-item active">Nuevo Prestamo Express</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <form name="selecRecursosPrest" id="theForm" action="{{ route('prestamo.resumen') }}" method="POST">
                    @csrf
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">Prestamo Express:</h5>
                                <p>
                                    *Los recursos deben ser retornados en el transcurso de la jornada laboral de hoy.<br>
                                    *El solicitante no necesariamiente debe estar registrado como usuario. <br>
                                    *Se muestran los recursos que no poseen reserva previa a la fecha.
                                </p>
                            </blockquote>
                            <h4>Datos del solicitante:</h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Apellido & Nombre</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Apellido/s, nombre/s" required>
                                        <span><p class="text-muted">*tipee para buscar un solicitante ya registrado y autocompletar</p></span>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>DNI</label>
                                        <input type="text" class="form-control" id="dni" name="dni" placeholder="DNI o CUIL" pattern=".{8,10}"
                                                oninvalid="this.setCustomValidity('El DNI debe tener entre 8 y 10 digitos')"  
                                                onchange="try{setCustomValidity('')}catch(e){}" required >
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Contacto</label>
                                        <input type="text" class="form-control" id="contacto" name="contacto" placeholder="Teléfono o email" pattern=".{8,}"
                                            oninvalid="this.setCustomValidity('El contacto debe tener un mínimo de 8 caracteres')"  
                                            onchange="try{setCustomValidity('')}catch(e){}" required >
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <!-- CardCollapsive: recursos -->
                            <div id="accordion">
                                <div class="card card-primary"> 
                                    <div class="card-header">
                                        <h4 class="card-title w-100">
                                          <a class="d-block w-100" data-toggle="collapse" href="#collapse1" >Recursos</a>
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="collapse hide" data-parent="#accordion">
                                        <div class="card-body">
                                            <table id="dtRecursos" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Recursos">
                                                <thead>
                                                    <tr role="row">
                                                        <th tabindex="0" aria-controls="Recursos" rowspan="1" colspan="1" cellpadding="1" data-orderable="false">
                                                            #
                                                        </th>
                                                        <th class="sorting" tabindex="1" aria-controls="Recursos" rowspan="1" colspan="1">
                                                            Código
                                                        </th>
                                                        <th class="sorting" tabindex="2" aria-controls="Recursos" rowspan="1" colspan="1">
                                                            Serie
                                                        </th>
                                                        <th class="sorting" tabindex="3" aria-controls="Recursos" rowspan="1" colspan="1">
                                                            Marca
                                                        </th>
                                                        <th class="sorting" tabindex="4" aria-controls="Recursos" rowspan="1" colspan="1">
                                                            Tipo
                                                        </th>
                                                        <th tabindex="5" aria-controls="Recursos" rowspan="1" colspan="1" data-orderable="false" >
                                                            Ver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($recursos as $recursosItem)
                                                    <tr>
                                                        <td>{{ $recursosItem->id }}</td>
                                                        <td scope="row">{{$recursosItem->codigo}}</td>
                                                        <td>{{$recursosItem->serie}}</td>
                                                        <td>{{$recursosItem->marca}}</td>
                                                        <td>{{$recursosItem->tipo}}</td>
                                                        <td>
                                                            <a data-toggle="modal" class="btn btn-default" data-target="#recuVer{{ $recursosItem->id }}" href="" title="Ver">
                                                                <i class="fas fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal con info de recurso -->
                                                    <div class="modal fade" id="recuVer{{ $recursosItem->id }}" tabindex="-1" role="dialog" aria-labelledby="recuVerLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Detalles del recurso ID #{{$recursosItem->id}}</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <ul>
                                                                        <li><strong>Código: </strong>{{$recursosItem->codigo}}</li>
                                                                        <li><strong>Serie: </strong>{{$recursosItem->serie}}</li>
                                                                        <li><strong>Marca: </strong>{{$recursosItem->marca}}</li>
                                                                        <li><strong>Modelo: </strong>{{$recursosItem->modelo}}</li>
                                                                        <li><strong>Descripción: </strong>{{$recursosItem->descripcion}}</li>
                                                                        <li><strong>Tipo: </strong>{{$recursosItem->tipo}}</li>
                                                                        <li><strong>Observaciones: </strong>{{$recursosItem->observacion}}</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--FIN: CardCollapsive: recursos  -->
                            
                            <!-- CardCollapsive: accesorios -->
                            <div id="accordion">
                                <div class="card card-primary"> 
                                    <div class="card-header">
                                        <h4 class="card-title w-100">
                                          <a class="d-block w-100" data-toggle="collapse" href="#collapse2" >Accesorios</a>
                                        </h4>
                                    </div>
                                    <div id="collapse2" class="collapse hide" data-parent="#accordion">
                                        <div class="card-body">
                                            <table id="dtAccesorios" class="display table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Accesorios">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting" tabindex="1" aria-controls="Accesorios" rowspan="1" colspan="1">
                                                            Código
                                                        </th>
                                                        <th class="sorting" tabindex="2" aria-controls="Accesorios" rowspan="1" colspan="1">
                                                            descripcion
                                                        </th>
                                                        <th class="sorting" tabindex="3" aria-controls="Accesorios" rowspan="1" colspan="1">
                                                            Tipo
                                                        </th>
                                                        <th class="sorting" tabindex="4" aria-controls="Accesorios" rowspan="1" colspan="1" data-orderable="false">
                                                            Seleccione (ver cant. dispon)
                                                        </th>
                                                        <th tabindex="5" aria-controls="Accesorios" rowspan="1" colspan="1" data-orderable="false" >
                                                            Ver
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($accesorios as $key => $value)
                                                    <tr>
                                                        <td scope="row">{{$value->codigo}}</td>
                                                        <td>{{$value->desc}}</td>
                                                        <td>{{$value->tipo}}</td>
                                                        <td>
                                                            <input type="hidden"  name="accs[{{$key}}][recuId]" value="{{$value->id }}"> 
                                                            <input type="number" class="form-control" placeholder="{{ $value->dispon }} disponibles" min="0" 
                                                                max="{{ $value->dispon }}" name="accs[{{$key}}][cantSel]">
                                                        </td>
                                                        <td>
                                                            <a data-toggle="modal" data-target="#accsVer{{ $recursosItem->id }}" class="btn btn-default" href=""  title="Ver">
                                                                <i class="fas fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal con info de accesorio -->
                                                    <div class="modal fade" id="accsVer{{ $recursosItem->id }}" tabindex="-1" role="dialog" aria-labelledby="acssVerLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title">Detalles del accesorio ID #{{$value->id }}</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <ul>
                                                                        <li><strong>Código: </strong>{{$value->codigo}}</li>
                                                                        <li><strong>Descripción: </strong>{{$value->desc}}</li>
                                                                        <li><strong>Tipo: </strong>{{$value->tipo}}</li>
                                                                        <li><strong>Observaciones: </strong>{{$value->observ}}</li>
                                                                    </ul>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--FIN: CardCollapsive: accesorios  -->
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:history.back()" class="btn bg-gradient-primary float-left">Volver</a>
                                </div>
                                <div class="col-2"> </div>
                                <div class="col-4">
                                    <button id="btnSubmit" type="submit" class="btn btn-block bg-gradient-primary btn-sm float-sm-right">Continuar</button>
                                </div>
                            </div>
                        </div> <!--fin cardfooter-->   
                    </form>
                </div><!-- FIN CARD -->
            </div>
            <div class="col"></div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@if( 1 == ( session('vacia')  ?? 0 ))
<script type="text/javascript">
    Swal.fire(
        'Antes de continuar...',
        'Debe seleccionar por lo menos un recurso o accesorio',
        'info')
</script>
@endif

<script type="text/javascript">
$(document).ready(function (){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $( "#nombre" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
            $.ajax({
                url:"{{route('getSolicitantes')}}",
                type: 'post',
                dataType: "json",
                data: {
                    _token: CSRF_TOKEN,
                    search: request.term
                },
                success: function( data ) {
                    response( data );
                }
            });
        },
        select: function (event, ui) {
           // Set selection
           $('#nombre').val(ui.item.label); // display the selected text
           $('#dni').val(ui.item.dni)
           $('#contacto').val(ui.item.contacto); // save selected id to input
           return false;
        }
    });

    var table = $('#dtRecursos').DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "pageLength": 5,
        "order": [[ 1, "desc" ]],
        language: {
            "search": "Buscar:",
            "decimal": ",",
            "emptyTable": "No hay datos disponibles en la Tabla",
            "infoThousands": ".",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
            "paginate": {
                "first": "Primera",
                "last": "Ultima",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": orden ascendente",
                "sortDescending": ": orden descendente"
            },
        },
        'columnDefs': [{
            'targets': 0,
            'checkboxes': {'selectRow': true}
        }],
        'select': {'style': 'multi'},
        'order': [[1, 'asc']]
   });

   var tableAccs = $('#dtAccesorios').DataTable( {
        scrollY: 300,
        paging:  false,
        keys:    true,
        "responsive": true,
        "lengthChange": false,
        "autoWidth": true,
        "order": [[ 0, "desc" ]],
        language: {
            "search": "Buscar:",
            "decimal": ",",
            "emptyTable": "No hay datos disponibles en la Tabla",
            "infoThousands": ".",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
            "paginate": {
                "first": "Primera",
                "last": "Ultima",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": orden ascendente",
                "sortDescending": ": orden descendente"
            },
        }
    } );
    /*
    //capturar cuando un input:text cambia
    $("input[type=number]").on('input', function() {
        console.log($(this).attr('name'));
        console.log( $(this).val() );
        $('#theForm').append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'accs[]')
                .val($(this).val())
        );        
    });
    */    
   //capturando el submit del formulario
    $('#theForm').on('submit', function(e){
        var form = this;
        var rows_selected = table.column(0).checkboxes.selected();

        // Iterate over all selected checkboxes
        $.each(rows_selected, function(index, rowId){
            $(form).append(                // Create a hidden element
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'recus[]')
                    .val(rowId)
            );
        });
   });
});

</script>


@endsection
