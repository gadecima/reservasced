@extends('layouts.admin')
@csrf

@section('contenido')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Admin</a></li>
                        <li class="breadcrumb-item "><a href="{{ route('recurso.visor') }}">Recursos</a></li>
                        <li class="breadcrumb-item active">Historico de Recurso</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <div class="float-left">
                                <blockquote class="quote-info mt-0">
                                <h5 id="tip">Historial de movimientos del Recurso: #{{ $codigo }}<br></p></h5>
                            </blockquote>
                            </div>
                        </div><!--fin card-header-->
                        <div class="card-body">
                            <table id="dtHistoric" class="table table-bordered table-striped" role="grid">
                                <tbody>
                                    @foreach($historic->slice(0, 10) as $historicItem) <!-- slice: limita foreach -->
                                        <tr>
                                            @if ($historicItem->Evento == 'prestamo')
                                                <td><i class="fas fa-shopping-cart"></i></td> 
                                                <td>
                                                    <a href="{{route('prestamo.ver',$historicItem->evento_id)}}">( {{$historicItem->evento_id}} )</a>
                                                </td>
                                            @else
                                                <td><i class="far fa-calendar-alt"></i></td>
                                                <td>
                                                    <a href="{{ route('reserva.verAdm',$historicItem->evento_id) }}">( {{$historicItem->evento_id}} )</a>
                                                </td>
                                            @endif
                                            <td><i class="fas fa-sign-out-alt"></i> {{$historicItem->f_desde}}</td>
                                            <td><i class="fas fa-user-alt"></i> {{$historicItem->solicitante}}</td>
                                            @switch($historicItem->estado)
                                                @case('Adeuda')
                                                    <td><i class="fas fa-exclamation-circle" style="color:red"></i></td>
                                                    <td>{{$historicItem->estado}}</td>
                                                    @break
                                                @case('Prestado') @case('En Curso')
                                                    <td><i class="fas fa-play" style="color:SteelBlue"></i></td>
                                                    <td>{{$historicItem->estado}}</td>
                                                    @break
                                                @case('Finalizada')@case('Devuelto')
                                                    <td><i class="far fa-check-circle" style="color:green"></i></td>
                                                    <td><i class="fas fa-sign-out-alt fa-flip-horizontal"></i> {{$historicItem->f_hasta}}</td>
                                                    @break
                                                @default
                                                    <td><i class="fab fa-dropbox"  style="color:blue"></i></td>
                                                    <td>{{$historicItem->estado}}</td>
                                                    @break
                                            @endswitch
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td  colspan='6'>
                                            <p class="text-primary"> 
                                                Se detallan los últimos 10 movimientos del recurso. <br>
                                                Para movimientos más antiguos, puede exportar el detalle completo haciendo <a href="{{ route("pdf.recuHistory",$historic[0]->recurso_id)}}" >click aqui <i class="far fa-file-alt"> </i></a>  
                                            </p>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="{{ route('recurso.visor') }}" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                        </div><!--fin card-footer-->
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@endsection