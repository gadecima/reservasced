@extends('layouts.admin')
@csrf
<!-- cosas a hacer aqui:
    - FALTA PRESENTAR LOS RECURSOS "OTROS" QUE SON POR CANTIDADES 
-->
@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item">Prestamo</li>
                        <li class="breadcrumb-item">Prestamo Express</li>
                        <li class="breadcrumb-item active">Resumen</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                            <h5 id="tip">A continuación se listan los recursos seleccionados:</h5>
                            <p><strong>Solicitante:</strong> {{$nombre}} <br>
                                <strong>DNI/CUIL:</strong> {{$dni}} <br>
                                <strong>Contacto:</strong> {{$contacto}}  
                            </p>
                            <p>Verifique y confirme el prestamo. <br></p>
                            </blockquote>
                            <form name="recPrestados" action="{{ route('prestamo.confirmado') }}" method="POST">
                            @csrf

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">tipo</th>
                                        <th scope="col">Marca/Modelo</th>
                                        <th scope="col">Descripción</th>
                                        <th scope="col">Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(!empty($prestados)) 
                                    @foreach($prestados as $key => $value)
                                    <tr>
                                        <th scope="row">{{$value->codigo }} </th>
                                        <td>{{$value->tipo}}</td>
                                        <td>{{$value->marca.'-'.$value->modelo}}</td>
                                        <td>{{$value->descripcion}}</td>
                                        <td>{{$value->cant}}</td>
                                    </tr>
                                    <input type="hidden"  name="prestados[{{$key}}][recuId]" value="{{$value->id }}">
                                    <input type="hidden"  name="prestados[{{$key}}][cantPrest]" value="{{$value->cant }}">
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <input type="hidden"  name="observ" value="{{ $observ }}">
                            <input type="hidden"  name="nombre" value="{{ $nombre }}">
                            <input type="hidden"  name="dni" value="{{ $dni }}">
                            <input type="hidden"  name="contacto" value="{{ $contacto }}">
                        </div> <!--fin cardbody-->
                    <div class="card-footer">
                        <a href="{{ route('prestamo.nuevo') }}" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                        <button type='submit' class="btn btn-block bg-gradient-success btn-sm col-5 float-sm-right">Confirmar</button>
                    </div>
                </div><!-- FIN CARD -->
                </form>
              </div>
              <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->


@endsection
