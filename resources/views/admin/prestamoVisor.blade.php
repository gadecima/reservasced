@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Admin</a></li>
                        <li class="breadcrumb-item active">Reservas</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="float-right">
                        <a href="{{route('prestamo.nuevo')}}" class="btn btn-success">
                            <i class="fas fa-plus"></i> Prestamo Nuevo
                        </a>
                        <a id='btnExporta'  class="btn btn-info">
                            <i class="fas fa-print"></i> Exportar
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fas fa-play"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">En curso</span>
                                    <span class="info-box-number">{{ $prestEnCurso }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-warning"><i class="fas fa-exclamation"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pendientes de devolución:</span>
                                    <span class="info-box-number">{{$prestAdeuda}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
               
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="dtPrestamos" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Prestamos">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="Prestamos" rowspan="1" colspan="1" cellpadding="1">
                                                    Código
                                                </th>
                                                <th class="sorting" tabindex="1" aria-controls="Prestamos" rowspan="1" colspan="1">
                                                    Fecha prestado
                                                </th>
                                                <th class="sorting" tabindex="2" aria-controls="Prestamos" rowspan="1" colspan="1">
                                                    Solicitante
                                                </th>
                                                <th class="sorting" tabindex="3" aria-controls="Prestamos" rowspan="1" colspan="1">
                                                    Contacto
                                                </th>
                                                <th class="sorting" tabindex="4" aria-controls="Prestamos" rowspan="1" colspan="1">
                                                    Estado
                                                </th>
                                                <th class="sorting" tabindex="5" aria-controls="Prestamos" rowspan="1" colspan="1">
                                                    Fecha devuelto
                                                </th>
                                                <th tabindex="6" aria-controls="Prestamos" rowspan="1" colspan="1" data-orderable="false" >
                                                    Opciones
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($prestamos as $prestamosItem)
                                            <tr>
                                                <th scope="row">{{$prestamosItem->id}}</th>
                                                <td>{{$prestamosItem->fecha_prestado}}</td>
                                                <td>{{$prestamosItem->solicitante}}</td>
                                                <td>{{$prestamosItem->contacto}}</td>
                                                <td>
                                                    <a id="estado" name="{{$prestamosItem->id}}" class="btn btn-block btn-sm
                                                        @switch($prestamosItem->estado)
                                                            @case('Devuelto')
                                                                btn-outline-success">
                                                                @break
                                                            @case('Prestado')
                                                                btn-outline-secondary">
                                                                @break
                                                            @case('Adeuda')
                                                                btn-outline-danger">
                                                                @break
                                                        @endswitch
                                                        {{$prestamosItem->estado}}    
                                                    </a>
                                                </td>
                                                <td>{{$prestamosItem->fecha_devuelto}}</td>
                                                <td>
                                                    <a class="btn btn-default" href="{{route('prestamo.ver',$prestamosItem->id)}}" title="Ver">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    <a id='btnActa' class="btn btn-info" name="{{$prestamosItem->id}}" title="Imprimir">
                                                      <i class="fas fa-print"></i>
                                                    </a>
                                                    <a class="btn btn-primary" name="{{$prestamosItem->id}}" id="btnComent" title="Comentario">
                                                        <i class="fas fa-comment-alt">  </i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

@if( NULL !== (session('result')) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! intente nuevamente.');
        break;
    }
</script>
@endif    

<script type="text/javascript">
$('#dtPrestamos').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 10,
    "order": [[ 1, "desc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );

$('#estado*').click(function(e){
    e.preventDefault();
    Swal.fire({
        title: '¿Cambiar estado del prestamo?',
        input: 'select',
        inputOptions: {
            'Devuelto': 'Devuelto',
            'Prestado': 'Prestado',
            'Adeuda': 'Adeuda',
        },
        inputPlaceholder: 'Seleccione',
        showCancelButton: true,
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value !== '') {
                    resolve();
                } else {
                    resolve('Antes, debe elegir un estado para cambiar');
                }
            });
        }
    }).then((result) => {
        if (result.isConfirmed) {
            window.location = 'PrestCambiaEstado'+this.name+'/'+result.value;
        }
    });
} );

$('#eliminar*').click(function(e){
    e.preventDefault();
    Swal.fire({
        title: '¿Está seguro de elimnar el registro?',
        text: "No podrá recuperarlo",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No'
    }).then((result) => {
            if (result.isConfirmed) {
                window.location = this.href;
            }
        });
});

$("#btnComent*").click(function(e){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var name_input = this.name;
    swal.fire({
        title: "Realizar comentario sobre el prestamo",
        input: "textarea",
        showCancelButton: true,
        confirmButtonColor: "#1FAB45",
        confirmButtonText: "Guardar",
        cancelButtonText: "Cancelar",
        buttonsStyling: true
    }).then(( request ) => {
        if (request.isConfirmed){ //presiona el botón de Guardar
            var data = { _token: CSRF_TOKEN, 
                comentario: request.value, 
                id: name_input };
                console.log(request.value+"-"+data.id);
            $.ajax({
                url: '{{ route('prestamo.coment') }}',
                type: "POST",
                data: data,
                success: function(request) {
                    Swal.fire('Comentario agregado', '', 'success')
                    console.log("ok");
                },
                failure: function (request) {
                    Swal.fire('Error! algo salió mal', '', 'error')
                    console.log('fallo no se envió los datos al servidor');
                }
            });
        }else if(request.isDismissed){ //presiona el botón de Cancelar o omitiendo dar respuesta
            
        }
    }) 
});

$("#btnActa*").click(function(e){
    var name_btn = this.name;
    var url = "{{ route('pdf.actaPrestamo',':name_btn') }}";
    url = url.replace(':name_btn', name_btn);
    console.log(url);
    Swal.fire({
        title: 'Preparando Acta...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrado');
                    Swal.close();
                }
            })
        }
    })    
});

$("#btnExporta").click(function(e){
    url = '{{ route('pdf.exportPrestamos') }}';
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrado');
                    Swal.close();
                }
            })
        }
    })
});

</script>

@endsection
