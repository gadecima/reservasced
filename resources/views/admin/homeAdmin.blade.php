@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <h1 class="m-0">¡¡ Bienvenido @auth {{ Auth::user()->nombre }} @endauth !!</h1>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="small-box bg-primary">
                                <div class="inner">
                                    <h3>{{$resPreparar}}</h3>Reservas a preparar.
                                </div>
                                <div class="icon">
                                    <i class="fab fa-dropbox"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="small-box bg-danger">
                                <div class="inner">
                                    <h3>{{$resAdeuda}}</h3>Pendientes de devolución
                                </div>
                                <div class="icon">
                                    <i class="fas fa-calendar-times"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3>{{$prestEnCurso}}</h3>Prestamos en curso.
                                </div>
                                <div class="icon">
                                    <i class="fas fa-play"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h3>{{$resAdeuda}}</h3>Prestamos Pendientes de devolución
                                </div>
                                <div class="icon">
                                    <i class="fas fa-exclamation"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card"><!-- card -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Tipos de recursos más reservados</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart">
                                                <canvas id="statsReservas" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">Tipos de Recursos más prestados</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart">
                                                <canvas id="statsPrestamos" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">Reservas por meses</h3>
                                            <div class="card-tools">
                                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                                <i class="fas fa-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="chart">
                                                <canvas id="statsResAnual" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
//GRAFICA DONA PARA RECURSOS MAS RESEVADOS
(async () => {
    const respuestaRaw =  await fetch("../public/api/statsReservas"); // Decodificar como JSON
    const respuesta = await respuestaRaw.json(); // Ahora ya tenemos las etiquetas y datos dentro de "respuesta"
    const $grafica = document.querySelector("#statsReservas");
    const etiquetas = respuesta.tipo; // <- Aquí estamos pasando el valor traído usando AJAX
    const datos = { 
        label: "Recursos reservados",// Pasar los datos igualmente desde PHP
        data: respuesta.cant, // <- Aquí estamos pasando el valor traído usando AJAX
        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'], // Color de fondo
        borderColor: 'rgba(54, 162, 235, 1)', // Color del borde
        borderWidth: 1, // Ancho del borde
    };
    new Chart($grafica, {
        type: 'doughnut', // Tipo de gráfica
        data: {
            labels: etiquetas,
            datasets: [ datos,]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Chart.js Doughnut Chart'
                }
            }
        },
    });
} ) ();
//FIN>> GRAFICA DONA PARA RECURSOS MAS RESEVADOS

//GRAFICA DONA PARA RECURSOS MAS Prestados
(async () => {
    const respuestaRaw =  await fetch("../public/api/statsPrestamos"); // Decodificar como JSON
    const respuesta = await respuestaRaw.json(); // Ahora ya tenemos las etiquetas y datos dentro de "respuesta"
    const $grafica = document.querySelector("#statsPrestamos");
    const etiquetas = respuesta.tipo; // <- Aquí estamos pasando el valor traído usando AJAX
    const datos = { 
        label: "Recursos Prestados",// Pasar los datos igualmente desde PHP
        data: respuesta.cant, // <- Aquí estamos pasando el valor traído usando AJAX
        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'], // Color de fondo
        borderColor: 'rgba(54, 162, 235, 1)', // Color del borde
        borderWidth: 1, // Ancho del borde
    };
    new Chart($grafica, {
        type: 'pie', // Tipo de gráfica
        data: {
            labels: etiquetas,
            datasets: [ datos,]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Chart.js Doughnut Chart'
                }
            }
        },
    });
} ) ();
//FIN>> GRAFICA DONA PARA RECURSOS MAS Prestados

//GRAFICA BARRAS PARA reservas por meses
(async () => {
    const respuestaRaw =  await fetch("../public/api/statsResAnual"); // Decodificar como JSON
    const respuesta = await respuestaRaw.json(); // Ahora ya tenemos las etiquetas y datos dentro de "respuesta"
   
    const $grafica = document.querySelector("#statsResAnual");
    const etiquetas = respuesta.mes; // <- Aquí estamos pasando el valor traído usando AJAX
    const datos = {  
        label: "Reservas",// Pasar los datos igualmente desde PHP
        data: respuesta.cant, // <- Aquí estamos pasando el valor traído usando AJAX
        backgroundColor: ['#00c0ef'], // Color de fondo
        borderColor: 'rgba(54, 162, 235, 1)', // Color del borde
        borderWidth: 1, // Ancho del borde
    };
    
    new Chart($grafica, {
        type: 'bar', // Tipo de gráfica
        data: {
            labels: etiquetas,
            datasets: [ datos,]
        },
        options: {
            responsive: true,
            plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Reservas por mes'
                }
            }
        },
    });
} ) ();
//FIN>> GRAFICA BARRAS PARA reservas por meses

</script>

@endsection
