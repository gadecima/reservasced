@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active">Reservas</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <a id='btnExporta' class="btn btn-info float-right">
                        <i class="fas fa-print"></i> Exportar Listado
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fab fa-dropbox"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Reservas a Preparar</span>
                                    <span class="info-box-number">{{$resPreparar}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fas fa-play"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Reservas en curso.</span>
                                    <span class="info-box-number">{{$resEnCurso}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-danger"><i class="far fa-calendar-minus"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Pendientes de devolución</span>
                                    <span class="info-box-number">{{$resAdeuda}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-warning"><i class="far fa-calendar-times"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Reservas canceladas</span>
                                    <span class="info-box-number">{{$resCanceladas}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card"><!-- card -->

                        <div class="card-body">
                            <!-- tabla de Usuario - Mis Reservas -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="Reservas" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="reservas">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1" cellpadding="1">
                                                    Código
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha Creada
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Solicitante
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha desde
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Fecha hasta
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1">
                                                    Estado
                                                </th>
                                                <th tabindex="0" aria-controls="MisReservas" rowspan="1" colspan="1" data-orderable="false" >
                                                    Opciones
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($reservas as $reservasItem)
                                            <tr>
                                                <th scope="row">{{$reservasItem->id}}</th>
                                                <td>{{$reservasItem->created_at}}</td>
                                                <td>{{$reservasItem->user_id}}</td>
                                                <td>{{$reservasItem->fecha_desde}} - {{$reservasItem->hora_desde}}
                                                </td>
                                                <td>{{$reservasItem->fecha_hasta}} - {{$reservasItem->hora_hasta}}
                                                </td>
                                                <td>
                                                    @if($reservasItem->est_reserv_id == 'Vencida')
                                                        <a name="{{$reservasItem->id}}" 
                                                            class="btn btn-block btn-sm btn-outline-secondary" style="cursor: default;">
                                                                {{$reservasItem->est_reserv_id}}
                                                        </a>
                                                    @else
                                                        <a id="estado" name="{{$reservasItem->id}}" class="btn btn-block btn-sm estado
                                                            @switch($reservasItem->est_reserv_id)
                                                            @case('Confirmada')
                                                                btn-outline-primary
                                                                @break
                                                                @case('En Curso') 
                                                                btn-outline-info
                                                                @break
                                                            @case('Preparada') 
                                                                btn-outline-success
                                                                @break
                                                                @case('Adeuda') 
                                                                btn-outline-danger
                                                                @break
                                                                @case('Suspendida')
                                                                btn-outline-dark
                                                                @break
                                                                @case('Cancelada')
                                                                btn-outline-warning
                                                                @break
                                                                @default
                                                                btn-outline-secondary
                                                                @endswitch
                                                                ">{{$reservasItem->est_reserv_id}}
                                                            </a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-default" href="{{ route('reserva.verAdm',$reservasItem->id) }}" title="Ver">
                                                            <i class="fas fa-eye"></i>
                                                    </a>
                                                    <a id="btnActa"  class="btn btn-info" name='{{ $reservasItem->id }}' title="Imprimir">
                                                      <i class="fas fa-print">  </i>
                                                    </a>
                                                    <a class="btn btn-primary" name="{{$reservasItem->id}}" id="btnComent" title="Comentario">
                                                        <i class="fas fa-comment-alt">  </i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

@if(  NULL !== ( session('result') ) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! intente nuevamente.');
        break;
    }
</script>
@endif    

<script type="text/javascript">
$('#estado*').click(function(e){
    e.preventDefault();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var name_input = this.name;
    Swal.fire({
        title: '¿Cambiar estado de la reserva?',
        input: 'select',
        inputOptions: {
            '1': 'Confirmada',
            '2': 'En Curso',
            '3': 'Preparada',
            '5': 'Suspendida',
            '6': 'Finalizada'
        },
        inputPlaceholder: 'Seleccione',
        showCancelButton: true,
        inputValidator: function (value) {
            return new Promise(function (resolve, reject) {
                if (value !== '') {
                    resolve();
                } else {
                    resolve('Antes, debe elegir un estado para cambiar');
                }
            });
        }
    }).then((result) => {
        if (result.isConfirmed) { 
            if(result.value === '2'){
                swal.fire({
                    title: "¿Algun comentario sobre la reserva?",
                    input: "textarea",
                    showCancelButton: true,
                    confirmButtonColor: "#1FAB45",
                    confirmButtonText: "Guardar",
                    cancelButtonText: "No es necesario",
                    buttonsStyling: true
                }).then((request ) => {
                    if (request.isConfirmed){ //presiona el botón de Guardar
                        var data = { _token: CSRF_TOKEN, 
                            comentario: request.value, 
                            id: name_input };
                            console.log(name_input+"-"+data.comentario);
                        $.ajax({
                            url: '{{ route('reserva.coment') }}',
                            type: "POST",
                            data: data,
                            success: function(request) {
                                console.log("OK"+request);
                                window.location = 'cambiaEstado'+name_input+'/'+result.value;
                            },
                            failure: function (request) {
                                console.log('fallo no se envió los datos al servidor');
                            }
                        });
                    }else if(request.isDismissed){ //presiona el botón de Cancelar o omitiendo dar respuesta
                        window.location = 'cambiaEstado'+name_input+'/'+result.value;
                    }
                }) 
            }
            else{ //elige algún estado distinto a 2
                window.location = 'cambiaEstado'+name_input+'/'+result.value;
            }
        }
    });
} );

$("#btnComent*").click(function(e){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var name_input = this.name;
    swal.fire({
        title: "Realizar comentario sobre la reserva",
        html: 'El nuevo comentario reemplazará al anterior.',
        input: "textarea",
        showCancelButton: true,
        confirmButtonColor: "#1FAB45",
        confirmButtonText: "Guardar",
        cancelButtonText: "Cancelar",
        buttonsStyling: true
    }).then(( request ) => {
        if (request.isConfirmed){ //presiona el botón de Guardar
            var data = { _token: CSRF_TOKEN, 
                comentario: request.value, 
                id: name_input };
                console.log(request.value+"-"+data.id);
            $.ajax({
                url: '{{ route('reserva.coment') }}',
                type: "POST",
                data: data,
                success: function(request) {
                    Swal.fire('Comentario agregado', '', 'success')
                    console.log("ok");
                },
                failure: function (request) {
                    Swal.fire('Error! algo salió mal', '', 'error')
                    console.log('fallo no se envió los datos al servidor');
                }
            });
        }else if(request.isDismissed){ //presiona el botón de Cancelar o omitiendo dar respuesta
            
        }
    }) 
});

$("#btnActa*").click(function(e){
    var name_btn = this.name;
    var url = "{{ route('pdf.actaReserva',':name_btn') }}";
    url = url.replace(':name_btn', name_btn);
    console.log(url);
    Swal.fire({
        title: 'Preparando Acta...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrado');
                    Swal.close();
                }
            })
        }
    })    
});

$('#Reservas').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 5,
    "order": [[ 0, "desc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );

$("#btnExporta").click(function(e){
    var url = '{{ route('pdf.reservasAdm') }}';
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrado');
                    Swal.close();
                }
            })
        }
    })
});

</script>

@endsection
