@extends('layouts.usuario')
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item ">Prestamos Express</li>
                        <li class="breadcrumb-item ">Nuevo Prestamo</li>
                        <li class="breadcrumb-item active">Prestamo Listo</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                            <h5 id="tip">Prestamo Listo! <br></p>
                            <h4>El código de prestamo es: #{{ $prestamoID }}</h4>
                            <b>Proceda a imprimir el comprobante si es necesario.</b>
                            </blockquote>
                        </div> <!-- fin card-body-->
                        <div class="card-footer">
                          <a href="{{ route('prestamo.visor') }}" class="btn btn-primary">Volver al menú</a>
                          <a href="{{ route('pdf.actaPrestamo',$prestamoID) }}" class="btn btn-block bg-gradient-info btn-sm col-5 float-sm-right">Imprimir</a>
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->


@endsection