@extends(Auth::user()->rol_id == 1 ? 'layouts.admin' : ((Auth::user()->rol_id == 0) ? 'layouts.super' : 'layouts.super'))
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Principal</a></li>
                        <li class="breadcrumb-item active">Reportes</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body"> <!-- card-body fondo-->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card card-primary card-outline">
                                        <div class="card-header">
                                            Reportes de Recursos
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    Tipo:
                                                    <select id="tipo" class="form-control" name="tipo">
                                                        <option value="-1" selected>Todos</option>
                                                    @foreach($tiposRecu as $tipoItem)
                                                        <option value="{{ $tipoItem->id }}">
                                                            {{ $tipoItem->tipo }}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    Estado:
                                                    <select id="estado" class="form-control" name="estado">
                                                        <option value="-1" selected>Todos</option>
                                                    @foreach($estadosRecu as $estadosItem)
                                                        <option value="{{ $estadosItem->id }}">
                                                            {{ $estadosItem->estado }}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-3">
                                                Disponibilidad:
                                                    <select class="form-control" id="dispon" name="dispon">
                                                        <option value="-1" selected>Todos</option>
                                                        <option value="1">Disponibles</option>
                                                        <option value="0">No disponibles</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-2">
                                                    <a class="btn btn-primary" id="btn_reportRecu" title="Exportar">
                                                        <i class="fas fa-print"> Exportar</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="card card-info card-outline">
                                        <div class="card-header">
                                            Reporte de Reservas
                                        </div>
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="col-md-3">
                                                    Estado:
                                                    <select id="estadoRes" class="form-control">
                                                        <option value="-1" selected>Todos</option>
                                                    @foreach($estadosRes as $estItem)
                                                        <option value="{{ $estItem->id }}">
                                                            {{ $estItem->estado_reserv }}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    Usuario:
                                                    <select id="users" class="form-control">
                                                        <option value="-1" selected>Todos</option>
                                                    @foreach($users as $usersItem)
                                                        <option value="{{ $usersItem->id }}">
                                                            {{ $usersItem->apellido.' '.$usersItem->nombre }}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-2">
                                                Creada desde:
                                                    <input type="text" class="form-control float-right" id="dt_desde_fecha" onkeydown="return false">
                                                </div>

                                                <div class="col-md-2">
                                                Creada Hasta:
                                                    <input type="text" class="form-control float-right" id="dt_hasta_fecha" onkeydown="return false">
                                                </div>

                                                <div class="col-md-2">
                                                    <a class="btn btn-info" id="btn_reportRes" title="Imprimir detalle">
                                                        <i class="fas fa-print"> Exportar</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <div class="card card-success card-outline">
                                        <div class="card-header">
                                            Reporte de Prestamos.
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    Estado:
                                                    <select id="estadoPrest" class="form-control">
                                                        <option value="-1" selected>Todos</option>
                                                        <option value="Prestado">Prestado</option>
                                                        <option value="Devuelto">Devuelto</option>
                                                        <option value="Adeuda">Adeuda</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    Solicitante:
                                                    <select id="solicitantes" class="form-control">
                                                        <option value="-1" selected>Todos</option>
                                                    @foreach($solicitantes as $solicitantesItem)
                                                        <option value="{{ $solicitantesItem->solicitante }}">
                                                            {{ $solicitantesItem->solicitante }}
                                                        </option>
                                                    @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-2">
                                                Prestado desde:
                                                    <input type="text" class="form-control float-right" id="dt_desde_fecha1" onkeydown="return false">
                                                </div>

                                                <div class="col-md-2">
                                                Hasta:
                                                    <input type="text" class="form-control float-right" id="dt_hasta_fecha1" onkeydown="return false">
                                                </div>

                                                <div class="col-md-2">
                                                    <a class="btn btn-success" id="btn_reportPrest" title="Imprimir detalle">
                                                        <i class="fas fa-print"> Exportar</i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card-body fondo-->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
    
$('#btn_reportRecu').click(function(e){
    var xTipo = $('#tipo').val();
    var xEstado = $('#estado').val();
    var xDispon = $('#dispon').val();
    var url = "{{ route('pdf.reporteRecu', [':xTipo', ':xEstado', ':xDispon']) }}";
    url = url.replace(':xTipo', xTipo);
    url = url.replace(':xEstado', xEstado);
    url = url.replace(':xDispon', xDispon);
    console.log(xTipo+' - '+xEstado+'-'+xDispon+' - '+url );
    //e.preventDefault();
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            //window.location = url; al colocarlo aqui, corrige error de doble log, comentar tambien url, type
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('mostrando: Loading');
                },
                success: function () {
                    console.log('cerrando: Loading');
                    Swal.close();
                }
            })
        }
    })
});

$('#btn_reportRes').click(function(e){
    var xEstado = $('#estadoRes').val();
    var xUser = $('#users').val();
    var desdeF = $('#dt_desde_fecha').val();
    var hastaF = $('#dt_hasta_fecha').val();
    var url = "{{ route('pdf.reporteRes', [':xEstadoRes', ':xUser', ':xDesde', ':xHasta']) }}";
    url = url.replace(':xEstadoRes', xEstado);
    url = url.replace(':xUser', xUser);
    url = url.replace(':xDesde', desdeF);
    url = url.replace(':xHasta', hastaF);
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('mostrando: Loading');
                },
                success: function () {
                    console.log('cerrando: Loading');
                    Swal.close();
                }
            })
        }
    })
});

$('#btn_reportPrest').click(function(e){
    var xEstado = $('#estadoPrest').val();
    var xSolicitante = $('#solicitantes').val();
    var desdeF = $('#dt_desde_fecha1').val();
    var hastaF = $('#dt_hasta_fecha1').val();
    var url = "{{ route('pdf.reportePrest', [':xEstadoPrest', ':xSolicitante', ':xDesde', ':xHasta']) }}";
    url = url.replace(':xEstadoPrest', xEstado);
    url = url.replace(':xSolicitante', xSolicitante);
    url = url.replace(':xDesde', desdeF);
    url = url.replace(':xHasta', hastaF);
    console.log('url: ' + url);
    
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('mostrando: Loading');
                },
                success: function () {
                    console.log('cerrando: Loading');
                    Swal.close();
                }
            })
        }
    })

});

//Selector Fecha DESDE
$(function () {
    $('[id*=dt_desde_fecha]').datetimepicker({
        daysOfWeekDisabled: [0, 6],
        format: 'DD-MM-YYYY',
        locale: 'es-MX',
    })
    $('[id*=dt_hasta_fecha]').datetimepicker({
        daysOfWeekDisabled: [0, 6],
        format: 'DD-MM-YYYY',
        locale: 'es-MX',
    })
});
    
</script>


@endsection   
