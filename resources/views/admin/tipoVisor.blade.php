@extends('layouts.admin')

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-10">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Admin</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('recurso.visor') }}">Recursos</a></li>
                        <li class="breadcrumb-item active">Tipos de Recursos</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col"><!-- /.col -->
                    <a href="{{ route('tipo.nuevo') }}" class="btn btn-success float-right">
                        <i class="fas fa-plus"></i> Agregar Tipo
                    </a>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Recursos</div>
                        
                        <div class="card-body">
                            <div class="col">
                                <table id="dtTiposRecu" class="table table-bordered table-striped dataTable dtr-inline" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="tipos" rowspan="1" colspan="1" cellpadding="1">
                                                Id
                                            </th>
                                            <th class="sorting" tabindex="1" aria-controls="tipos" rowspan="1" colspan="1">
                                                Tipo
                                            </th>
                                            
                                            <th class="sorting" tabindex="3" aria-controls="tipos" rowspan="1" colspan="1">
                                                Reservable
                                            </th>
                                            <th tabindex="4" aria-controls="tipos" rowspan="1" colspan="1" data-orderable="false" >
                                                Opciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tiposRecu as $tiposRecuItem)
                                        <tr>
                                            <th scope="row">{{$tiposRecuItem->id}}</th>
                                            <td>{{$tiposRecuItem->tipo}}</td>
                                            <td> {{ $tiposRecuItem->reservable ? 'Si' : 'No' }}</td>
                                            <td>
                                                <form method="POST" action="{{ route('tipo.eliminar', $tiposRecuItem->id) }}">
                                                <a class="btn btn-info" href="{{route('tipo.editar',$tiposRecuItem->id)}}" title="editar">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                @csrf @method('DELETE')
                                                @if ( $tiposRecuItem->id != 0)
                                                    <a id="eliminar" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                                @endif
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> <!-- /.col-6 -->
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-6 -->

                <div class="col-6">
                    <div class="card">
                        <div class="card-header">Accesorios / Insumos</div>
                        <div class="card-body">
                            <div class="col">
                                <table id="dtTiposAccs" class="table table-bordered table-striped dataTable dtr-inline" role="grid">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting" tabindex="0" aria-controls="tipos" rowspan="1" colspan="1" cellpadding="1">
                                                Id
                                            </th>
                                            <th class="sorting" tabindex="1" aria-controls="tipos" rowspan="1" colspan="1">
                                                Tipo
                                            </th>
                                            
                                            <th class="sorting" tabindex="3" aria-controls="tipos" rowspan="1" colspan="1">
                                                Reservable
                                            </th>
                                            <th tabindex="4" aria-controls="tipos" rowspan="1" colspan="1" data-orderable="false" >
                                                Opciones
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tiposAccs as $tiposAccsItem)
                                            <tr>
                                                <th scope="row">{{$tiposAccsItem->id}}</th>
                                                <td>{{$tiposAccsItem->tipo}}</td>
                                                <td> {{ $tiposAccsItem->reservable ? 'Si' : 'No' }}</td>
                                                <td>
                                                    <form method="POST" action="{{ route('tipo.eliminar', $tiposAccsItem->id) }}">
                                                    <a class="btn btn-info" href="{{route('tipo.editar',$tiposAccsItem->id)}}" title="editar">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    @csrf @method('DELETE')
                                                    @if ( $tiposAccsItem->id != 0)
                                                        <a id="eliminar" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                                    @endif
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> <!-- /.col-6 -->
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-6 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

@if(  NULL !== ( session('result') ) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! No puede eliminar un Tipo <br> que tiene recursos asociados.');
        break;
    }
</script>
@endif    

<script type="text/javascript">

$('#eliminar*').click(function(e){
    e.preventDefault();
    var form =  $(this).closest("form");
    Swal.fire({
        title: '¿Está seguro de elimnar el registro?',
        text: "No podrá recuperarlo",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.isConfirmed) {
            form.submit();
        }
    });
});

$('#dtTiposRecu, #dtTiposAccs').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 10,
    "order": [[ 0, "asc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );

</script>

@endsection   
