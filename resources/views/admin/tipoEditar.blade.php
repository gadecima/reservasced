@extends('layouts.admin')

@section('contenido')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Admin</a></li>
                        <li class="breadcrumb-item ">Tipos</li>
                        <li class="breadcrumb-item active">Editar Tipo</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <form method="POST" action="{{ route('tipo.actualizar',$tipo->id) }}">
                    @csrf @method('PATCH')
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">Editar Tipo de Recurso id: #{{$tipo->id}}</p></h5>
                            </blockquote>
                        </div><!--fin card-header-->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td scope="row"><strong>Nombre de Tipo:</strong></td>
                                        <td>
                                            <input id="tipo" type="text" class="form-control @error('tipo') is-invalid @enderror"
                                             name="tipo" required autocomplete="tipo"  autofocus value="{{ $tipo->tipo }}">
                                            @error('tipo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>¿Reservable?:</strong></td>
                                        <td>
                                            <div class="custom-control custom-switch custom-switch-on-success custom-switch-off-danger">
                                                <input type="checkbox" class="custom-control-input" name="swtResOn" id="swtReserb" 
                                                @if(  $tipo->reservable ) checked @endif>
                                                <label class="custom-control-label" for="swtReserb">NO/SI.</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="row">
                                                <div align="center" class="custom-control custom-radio col-sm-6">
                                                    <input class="custom-control-input" type="radio" id="rdRecu" name="recu_accs" value="1"
                                                    {{ $tipo->recu_accs == '1' ? 'checked' : '' }}>
                                                    <label for="rdRecu" class="custom-control-label">Recurso</label>
                                                </div>
                                                <div align="center" class="custom-control custom-radio col-sm-6">
                                                    <input class="custom-control-input" type="radio" id="rdAccs" name="recu_accs" value="0"
                                                    {{ $tipo->recu_accs == '0' ? 'checked' : '' }}>
                                                    <label for="rdAccs" class="custom-control-label">Accesorio</label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            <button type="submit" class="btn btn-block bg-gradient-success btn-sm col-5 float-sm-right"> Guardar cambios </button>
                        </div><!--fin card-footer-->
                    </div><!-- FIN CARD -->
                    </form>
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->
@endsection