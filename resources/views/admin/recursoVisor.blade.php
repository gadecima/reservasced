@extends('layouts.admin')

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-5">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active">Inventario de Recursos</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col"><!-- /.col -->
                    <div class="float-right">
                        <a href="{{ route('recurso.nuevo') }}" class="btn btn-success">
                            <i class="fas fa-plus"></i> Agregar Recurso
                        </a>
                        <a href="{{ route('recurso.nuevoInsumo') }}" class="btn btn-primary">
                            <i class="fas fa-plus"></i> Agregar Accesorios
                        </a>
                        <div class="btn-group">
                            <a class="btn btn-info" href="{{ route('tipo.visor') }}">Tipos de Recursos</a> 
                            <button type="button" class="btn btn-info dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="{{ route('tipo.nuevo') }}">Agregar Tipos de Rec.</a>
                            </div>
                        </div>
                        <a id='btnExporta' class="btn btn-info">
                            <i class="fas fa-print"></i> Exportar Listado
                        </a>
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="dtRecursos" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Recursos">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="Recursos" rowspan="1" colspan="1" cellpadding="1">
                                                    Código
                                                </th>
                                                <th class="sorting" tabindex="1" aria-controls="Recursos" rowspan="1" colspan="1">
                                                    Serie
                                                </th>
                                                <th class="sorting" tabindex="2" aria-controls="Recursos" rowspan="1" colspan="1">
                                                    Marca
                                                </th>
                                                <th class="sorting" tabindex="3" aria-controls="Recursos" rowspan="1" colspan="1">
                                                    Modelo
                                                </th>
                                                <th class="sorting" tabindex="4" aria-controls="Recursos" rowspan="1" colspan="1">
                                                    Tipo
                                                </th>
                                                <th class="sorting" tabindex="5" aria-controls="Recursos" rowspan="1" colspan="1">
                                                    Estado
                                                </th>
                                                <th tabindex="6" aria-controls="Recursos" rowspan="1" colspan="1" data-orderable="false" >
                                                    Opciones
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($recursos as $recursosItem)
                                            <tr>
                                                <th scope="row">{{$recursosItem->codigo}}</th>
                                                <td>{{$recursosItem->serie}}</td>
                                                <td>{{$recursosItem->marca}}</td>
                                                <td>{{$recursosItem->modelo}}</td>
                                                <td>{{$recursosItem->tipo}}</td>
                                                <td>
                                                    <a id="estado" name="{{$recursosItem->id}}" class="btn btn-block btn-sm 
                                                        @if($recursosItem->estado == 'Funcional')
                                                            btn-outline-success">
                                                        @elseif ($recursosItem->estado == 'Robado' OR $recursosItem->estado == 'Baja')
                                                            btn-outline-danger">
                                                        @else
                                                            btn-outline-secondary">
                                                        @endif
                                                        {{$recursosItem->estado}}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a class="btn btn-default" href="{{route('recurso.ver',$recursosItem->id)}}" title="Ver">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    <a class="btn btn-primary" href="{{route('pdf.detalleRecurso',$recursosItem->id)}}" title="Imprimir detalle">
                                                      <i class="fas fa-print"></i>
                                                    </a>
                                                    <a class="btn btn-info" href="{{route('recurso.editar',$recursosItem->id)}}" title="Editar">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                    <a class="btn btn-success" href="{{route('recurso.historico',$recursosItem->id)}}" title="Historico">
                                                        <i class="fas fa-history"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

@if( NULL !== (session('result')) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! intente nuevamente.');
        break;
        case (2): toastr.info('Éste recurso no tiene movimientos para detallar.');
        break;
    }
</script>
@endif    

<script type="text/javascript">
$("#btnExporta").click(function(e){
    url = "{{ route('pdf.exportRecursos') }}";
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('mostrando: Loading');
                },
                success: function () {
                    console.log('cerrando: Loading');
                    Swal.close();
                }
            })
        }
    })
})

$('#estado*').click(function(e){
    e.preventDefault();
    Swal.fire({
        title: '¿Cambiar estado del recurso?',
        input: 'select',
        inputOptions: {
        '1': 'Funcional',
        '2': 'Dañado',
        '3': 'Robado',
        '4': 'Baja',
        '5': 'Extraviado',
        '6': 'Transferido'
        },
        inputPlaceholder: 'Seleccione',
        showCancelButton: true,
        inputValidator: function (value) {
        return new Promise(function (resolve, reject) {
            if (value !== '') {
            resolve();
            } else {
            resolve('Antes, debe elegir un estado para cambiar');
            }
        });
        }
    }).then((result) => {
        if (result.isConfirmed) {
            window.location = 'recursoCambiaEstado'+this.name+'/'+result.value;
        }
    });
} );

$('#eliminar*').click(function(e){
    e.preventDefault();
    var form =  $(this).closest("form");
    Swal.fire({
        title: '¿Está seguro de elimnar el recurso?',
        text: "No podrá recuperarlo",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.isConfirmed) {
            form.submit();
        }
    });
});

$('#dtRecursos').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 10,
    "order": [[ 1, "desc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );
</script>

@endsection   
