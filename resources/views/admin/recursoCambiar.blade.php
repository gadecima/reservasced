@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Admin</a></li>
                        <li class="breadcrumb-item ">Reserva #{{$idReserva}}</li>
                        <li class="breadcrumb-item active">Cambiar recurso en reserva</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <table id="dtRecursosDispon" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Recursos">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="Recursos" rowspan="1" colspan="1" cellpadding="1">
                                            Código
                                        </th>
                                        <th class="sorting" tabindex="1" aria-controls="Recursos" rowspan="1" colspan="1">
                                            Serie
                                        </th>
                                        <th class="sorting" tabindex="2" aria-controls="Recursos" rowspan="1" colspan="1">
                                            Marca
                                        </th>
                                        <th class="sorting" tabindex="4" aria-controls="Recursos" rowspan="1" colspan="1">
                                            Tipo
                                        </th>
                                        <th tabindex="6" aria-controls="Recursos" rowspan="1" colspan="1" data-orderable="false" >
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($recursos as $recursosItem)
                                    <tr>
                                        <th scope="row">{{$recursosItem->codigo}}</th>
                                        <td>{{$recursosItem->serie}}</td>
                                        <td>{{$recursosItem->marca}}</td>
                                        <td>{{$recursosItem->tipo}}</td>
                                        <form method="POST" action="{{ route('reserva.recuCambiado') }}">
                                        @csrf @method('PATCH')
                                        <td>
                                            <button type="submit" id="seleccionado" class="btn btn-success" href="" title="Seleccionar">
                                                <i class="far fa-check-square"></i>
                                            </button>
                                        </td>
                                            <input type="hidden" name="idRecuNuevo" value="{{$recursosItem->id}}">
                                            <input type="hidden" name="idReserva" value="{{$idReserva}}">
                                            <input type="hidden" name="idRecuViejo" value="{{$idRecu}}">
                                        </form>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

<script>
$('#dtRecursosDispon').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 10,
    "order": [[ 1, "desc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );
</script>



@endsection
