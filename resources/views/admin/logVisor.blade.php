@extends(Auth::user()->rol_id == 1 ? 'layouts.admin' : ((Auth::user()->rol_id == 0) ? 'layouts.super' : 'layouts.super'))
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-7">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Principal</a></li>
                        <li class="breadcrumb-item active">LOG del sistema</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-5">
                    <a id='btnExporta' class="btn btn-info float-right">
                        <i class="fas fa-print"></i> Exportar LOG
                    </a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="dtLogs" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Logs">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="Logs" rowspan="1" colspan="1" cellpadding="1">
                                                    id
                                                </th>
                                                <th class="sorting" tabindex="1" aria-controls="Logs" rowspan="1" colspan="1">
                                                    Usuario
                                                </th>
                                                <th class="sorting" tabindex="2" aria-controls="Logs" rowspan="1" colspan="1">
                                                    Evento
                                                </th>
                                                <th class="sorting" tabindex="3" aria-controls="Logs" rowspan="1" colspan="1">
                                                    Detalle del evento
                                                </th>
                                                <th class="sorting" tabindex="4" aria-controls="Logs" rowspan="1" colspan="1">
                                                    Fecha/hora
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($logs as $logsItem)
                                            <tr>
                                                <th scope="row">{{$logsItem->id}}</th>
                                                <td>{{$logsItem->evento_usr}}</td>
                                                <td>{{$logsItem->evento}}</td>
                                                <td>{{$logsItem->evento_detalle}}</td>
                                                <td>{{$logsItem->created_at}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
$("#btnExporta").click(function(e){
    url = '{{ route('pdf.exportLog') }}';
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            window.location = url;
            Swal.showLoading();
            $.ajax({
                //url: url,
                //type: "GET",
                beforeSend: function () {
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrrado');
                    Swal.close();
                }
            })
        }
    })
})

$('#dtLogs').DataTable( {
    "responsive": true,
    "lengthChange": false,
    "autoWidth": false,
    "pageLength": 10,
    "order": [[ 0, "desc" ]],
    language: {
        "search": "Buscar:",
        "decimal": ",",
        "emptyTable": "No hay datos disponibles en la Tabla",
        "infoThousands": ".",
        "lengthMenu": "Mostrar _MENU_ entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
        "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
        "paginate": {
            "first": "Primera",
            "last": "Ultima",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": orden ascendente",
            "sortDescending": ": orden descendente"
        },
    }
} );
</script>

@endsection   
