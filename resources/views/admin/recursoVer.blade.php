@extends(Auth::user()->rol_id == 1 ? 'layouts.admin' : ((Auth::user()->rol_id == 0) ? 'layouts.super' : 'layouts.super'))
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item ">Recurso</li>
                        <li class="breadcrumb-item active">Ver Recurso</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" colspan="2">Detalles del recurso ID #{{$recurso->id}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row"><strong>Código:</strong></td>
                                        <td>{{$recurso->codigo}}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Serie:</strong></td>
                                        <td>{{$recurso->serie}}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Marca:</strong></td>
                                        <td>{{$recurso->marca}}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Modelo:</strong></td>
                                        <td>{{$recurso->modelo}}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Descripción:</strong></td>
                                        <td>{{$recurso->descripcion}}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Tipo:</strong></td>
                                        <td>{{$recurso->tipo}}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Disponibilidad:</strong></td>
                                        @if ($recurso->disponible)
                                            <td>Disponible</td>
                                        @else
                                            <td>NO disponible</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Cantidad:</strong></td>
                                        <td>{{$recurso->cant}}</td>
                                    </tr>
                                    <tr >
                                        <td scope="row"><strong>Observaciones:</strong></td>
                                        <td>{{$recurso->observacion}}</td>
                                    </tr>
                                    <tr> 
                                        <!-- ESTO DA ERROR CUANDO ES NULL: debo llenarlo al aplicar la migracion -->
                                        <td scope="row"><strong>Creado fecha:</strong></td>
                                        <td>{{$recurso->created_at->format('d-m-Y')}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <form method="POST" action="{{route('recurso.eliminar',$recurso->id)}}">
                                            @csrf @method('DELETE')
                                            <a class="btn btn-default" href="{{route('pdf.detalleRecurso',$recurso->id)}}" title="Imprimir">
                                                <i class="fas fa-print"> Imprimir detalles</i>
                                            </a>
                                            <a class="btn btn-info" href="{{route('recurso.editar',$recurso->id)}}" title="editar">
                                                <i class="fas fa-edit"> Editar</i>
                                            </a>
                                            @if( Auth::user()->rol_id == 0 )
                                            <a class="btn btn-danger" type="submit" id="eliminar" title="Eliminar">
                                                <i class="fas fa-trash-alt"> Eliminar</i>
                                            </a>
                                            @endif
                                            </form>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="javascript:history.back()" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

@endsection
