@extends('layouts.admin');
@csrf

@section('contenido')

<!-- Estas funciones precargan los selecetbox/checkbox con los valores según la BD -->
<script type="text/javascript">
$(document).ready(function() {
    var tipo_preselec = {{$recurso->tipo}};
    $('#tipo > option[value="'+ tipo_preselec +'"]').attr('selected', 'selected');

    var estado_preselec = {{$recurso->estado}};
    $('#estado > option[value="'+ estado_preselec +'"]').attr('selected', 'selected');

    var dispon_preselec = {{$recurso->disponible}};
    if (dispon_preselec == 1){
        $('#disponible').attr("checked", true);
    }else{
        $('#disponible').attr("checked", false);
    }
});
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Admin</a></li>
                        <li class="breadcrumb-item "><a href="{{ route('recurso.visor') }}">Recursos</a></li>
                        <li class="breadcrumb-item active">Editar Recurso</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <form method="POST" action="{{ route('recurso.actualizar', $recurso->id) }}">
                    @csrf @method('PATCH')
                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <div class="float-left">
                                <blockquote class="quote-info mt-0">
                                <h5 id="tip">Editar Recurso id #{{ $recurso->id }}<br></p></h5>
                                - Modifique los datos del recurso, considere:<br>
                                *No puede haber 2 o más recursos con el mismo código.<br>
                                *No puede haber 2 o más recursos con el mismo número de serie.<br>
                            </blockquote>
                            </div>
                        </div><!--fin card-header-->
                        <div class="card-body">
                        @csrf
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td scope="row"><strong>Código:</strong></td>
                                        <td>
                                            <input id="codigo" type="text" class="form-control @error('codigo') is-invalid @enderror" name="codigo" value="{{ $recurso->codigo }}" required autocomplete="codigo" autofocus>
                                            @error('codigo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Serie:</strong></td>
                                        <td><input id="serie" type="text" class="form-control @error('serie') is-invalid @enderror" name="serie" value="{{ $recurso->serie }}" autocomplete="serie">
                                        @error('serie')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Marca:</strong></td>
                                        <td>
                                            <input id="marca" type="text" class="form-control @error('marca') is-invalid @enderror" name="marca" value="{{$recurso->marca}}" required autocomplete="marca">
                                            @error('marca')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Modelo:</strong></td>
                                        <td>
                                            <input id="modelo" type="text" class="form-control @error('modelo') is-invalid @enderror" name="modelo" value="{{$recurso->modelo}}" required autocomplete="modelo">
                                            @error('modelo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Descripción:</strong></td>
                                        <td>
                                            <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{ $recurso->descripcion}}" autocomplete="descripcion">
                                            @error('descripcion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Tipo:</strong></td>
                                        <td>
                                            <select id="tipo" class="form-control" name="tipo" value="{{old('tipo')}}"  autocomplete="tipo">
                                                @foreach($tipo as $tipoItem)
                                                    <option value="{{ $tipoItem->id }}">
                                                        {{ $tipoItem->tipo }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <td scope="row"><strong>Estado:</strong></td>
                                        <td>
                                            <select id="estado" class="form-control" name="estado" value="{{ old('estado') }}" autocomplete="estado">
                                                @foreach($estado as $estadoItem)
                                                    <option value="{{ $estadoItem->id }}">
                                                        {{ $estadoItem->estado}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                        <td scope="row"><strong>Disponibilidad:</strong></td>
                                        <td>
                                            <select class="form-control" id="disponible" name="disponible" value="{{old('disponible')}}" autocomplete="disponible">
                                                <option value="1">Disponible</option>
                                                <option value="0">NO Disponible</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr >
                                        <td scope="row"><strong>Observaciones:</strong></td>
                                        <td>
                                            <textarea id="observacion" class="form-control @error('observacion') is-invalid @enderror" name="observacion" autocomplete="observacion">
                                            {{$recurso->observacion}}
                                            @error('observacion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            </textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td scope="row"><strong>Cantidad:</strong></td>
                                        <td>
                                            <input id="cantidad" type="number" class="form-control @error('cantidad') is-invalid @enderror" name="cant" value="{{ $recurso->cant}}" required autocomplete="cant" min="1">
                                            @error('cantidad')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <a href="{{ route('recurso.visor') }}" class="btn btn-block bg-gradient-primary btn-sm col-5 float-sm-left">Volver</a>
                            <button type="submit" class="btn btn-block bg-gradient-success btn-sm col-5 float-sm-right"> Guardar cambios </button>
                        </div><!--fin card-footer-->
                    </div><!-- FIN CARD -->
                    </form>
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->
@endsection