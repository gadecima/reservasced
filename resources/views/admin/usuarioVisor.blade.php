@extends('layouts.admin')
@csrf

@section('contenido')
<div class="content-wrapper">
    <div class="content-header"><!-- Content Header (Page header) -->
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-8">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active">Usuario</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col">
                    <div class="float-right">
                        <a href="{{ route('usuario.nuevo') }}" class="btn btn-success">
                            <i class="fas fa-plus"></i> Agregar Usuario
                        </a>
                        <a id='btnExporta' class="btn btn-info">
                            <i class="fas fa-print"></i> Exportar Listado
                        </a>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->
    
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="dtUsuarios" class="table table-bordered table-striped dataTable dtr-inline" role="grid" aria-describedby="Usuarios">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="Usuarios" rowspan="1" colspan="1" cellpadding="1">
                                                    #ID
                                                </th>
                                                <th class="sorting" tabindex="1" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    Rol
                                                </th>
                                                <th class="sorting" tabindex="2" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    eMail
                                                </th>
                                                <th class="sorting" tabindex="3" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    Apellido Y Nombre
                                                </th>
                                                <th class="sorting" tabindex="4" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    DNI
                                                </th>
                                                <th class="sorting" tabindex="5" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    Teléfono
                                                </th>
                                                <th class="sorting" tabindex="6" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    Cargo - Área - Dependencia
                                                </th>
                                                <th class="sorting" tabindex="7" aria-controls="Usuarios" rowspan="1" colspan="1">
                                                    Opciones 
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($usuarios as $usuariosItem)
                                            <tr>
                                                <td>{{ $usuariosItem->id }}</td>
                                                <td>{{ $usuariosItem->rol_nombre }}</td>
                                                <td>{{ $usuariosItem->email}}</td>
                                                <td>{{ $usuariosItem->apellido.', '.$usuariosItem->nombre }}</td>
                                                <td>{{ $usuariosItem->dni}}</td>
                                                <td>{{ $usuariosItem->telefono}}</td>
                                                <td>{{ $usuariosItem->cargo}} <br> {{ $usuariosItem->ofi_area}} <br> {{$usuariosItem->dependencia}}</td>
                                                <td>
                                                    @if($usuariosItem->rol_id == '2')
                                                        <form method="POST" action="{{ route('usuario.eliminar', $usuariosItem->id) }}">
                                                        <a class="btn btn-info" href="{{route('usuario.editar',$usuariosItem->id)}}" title="editar">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        @csrf @method('DELETE')
                                                        <a id="eliminar" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div><!-- /.content-wrapper -->


@if( NULL !== (session('result')) )
<script type="text/javascript">
    switch ( {{ session('result')}} ) {
        case (1): toastr.success('Operación exitosa!');
        break;
        case (0): toastr.error('Error! Verifique que el usuario no posea reservas creadas.');
        break;
    }
</script>
@endif    

<script type="text/javascript">

    $('#eliminar*').click(function(e){
        e.preventDefault();
        var form =  $(this).closest("form");
        Swal.fire({
            title: '¿Está seguro de elimnar el usuario?',
            text: "No podrá recuperarlo",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                form.submit();
            }
        });
    });

$("#btnExporta").click(function(e){
    url = "{{ route('pdf.exportUsers') }}";
    Swal.fire({
        title: 'Preparando reporte...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: false,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('mostrando: Loading');
                },
                success: function () {
                    console.log('cerrando: Loading');
                    Swal.close();
                }
            })
        }
    })
})

$('#dtUsuarios').DataTable( {
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "pageLength": 10,
        "order": [[ 1, "desc" ]],
        language: {
            "search": "Buscar:",
            "decimal": ",",
            "emptyTable": "No hay datos disponibles en la Tabla",
            "infoThousands": ".",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "zeroRecords": "No se encontraron registros que coincidan con la búsqueda",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 entradas",
            "paginate": {
                "first": "Primera",
                "last": "Ultima",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": orden ascendente",
                "sortDescending": ": orden descendente"
            },
        }
    } );
</script>

@endsection   
