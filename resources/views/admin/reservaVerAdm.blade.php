@extends(Auth::user()->rol_id == 1 ? 'layouts.admin' : ((Auth::user()->rol_id == 0) ? 'layouts.super' : 'layouts.super'))
@csrf

@section('contenido')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item ">Reservas</li>
                        <li class="breadcrumb-item active">Ver Reserva</li>
                    </ol>
                </div><!-- /.col -->
                <div class="col-sm-6"></div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Contenido Principal -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col-9">
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <blockquote class="quote-info mt-0">
                                <h5 id="tip">Reserva #{{$reserva->id}}<br></h5>
                                <ul> 
                                    <li>Del {{$reserva->fecha_desde}} - {{$reserva->hora_desde}} al {{$reserva->fecha_hasta}} - {{$reserva->hora_hasta}} </li>
                                    <li>Creada el: {{$reserva->created_at}} </li>
                                    <li>Creado por: {{$creador->apellido}}, {{$creador->nombre}}</li>
                                    <li>Estado: {{ $estado_reserv }} </li>
                                </ul>
                            </blockquote>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">tipo</th>
                                        <th scope="col">Marca/Modelo</th>
                                        <th scope="col">Cantidad</th>
                                        <th scope="col">Descripción</th>
                                        <!-- <th scope="col">Acciones</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($reservados)) 
                                        @foreach($reservados as $reservadosItem)
                                            <tr>
                                                <th scope="row">{{$reservadosItem->codigo }} </th>
                                                <td>{{$reservadosItem->tipo}}</td>
                                                <td>{{$reservadosItem->marca.'-'.$reservadosItem->modelo}}</td>
                                                <td>{{$reservadosItem->cant}}</td>
                                                <td>{{$reservadosItem->descripcion}}</td>
                                            <!--
                                                <td>
                                                    <a class="btn btn-info" id="btnCambiar" name='{{$reserva->id}}' title='{{$reservadosItem->id}}'>
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </td>
                                            -->
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                                <tfoot>
                                    <td><strong>Observaciones <br> del solicitante:</strong></td>
                                    <td colspan="5">{{$reserva->observ}}</td>
                                </tfoot>
                            </table>
                            <br>
                            <div class="row">
                                <div class="col">
                                    <div class="callout callout-info">
                                        <h5>Comentarios del Administrador</h5>
                                        <p>{{$reserva->comentario}}</p>
                                    </div>
                                </div>
                            </div>
                        </div> <!--fin cardbody-->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="javascript:history.back()" class="btn bg-gradient-primary float-left"><i class="fas fa-chevron-left">  Volver</i></a>
                                </div>
                                <div class="col">
                                    <div class="float-right">
                                        <a id="btnActa"  class="btn bg-gradient-info" name='{{ $reserva->id }}' title="Imprimir">
                                            <i class="fas fa-print"> Imprimir </i>
                                        </a>
                                        @if ($reserva->est_reserv_id == 1)
                                            <a href="{{ route('reserva.cambiaEstado',[$reserva->id, 3]) }}" class="btn bg-gradient-success">
                                                <i class="fas fa-check"> Marcar Preparada</i>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- FIN CARD -->
                </div>
                <div class="col"></div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.Fin contenido principal -->
</div><!-- /.content-wrapper -->

<script>
$("#btnActa*").click(function(e){
    var name_btn = this.name;
    var url = "{{ route('pdf.actaReserva',':name_btn') }}";
    url = url.replace(':name_btn', name_btn);
    console.log(url);
    Swal.fire({
        title: 'Preparando Acta...',
        html: 'Espere por favor...',
        allowEscapeKey: false,
        allowOutsideClick: true,
        didOpen: function () {
            Swal.showLoading();
            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    window.location = url;
                    Swal.showLoading();
                    console.log('Loading');
                },
                success: function () {
                    console.log('cerrado');
                    Swal.close();
                }
            })
        }
    })    
});


$("#btnCambiar*").click(function(e){
    var name_btn = this.name;
    var title_btn = this.title;
    var url = "{{ route('reserva.recuCambiar',[':name_btn', ':title_btn'] ) }}";
    url = url.replace(':name_btn', name_btn);
    url = url.replace(':title_btn', title_btn);
    console.log(url);
    $.ajax({
        type: "GET",
        url: url,
        success:function(data) {
            window.location = url;
                    }
    })
});
</script>

@endsection
