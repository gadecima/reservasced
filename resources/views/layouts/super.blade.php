<!DOCTYPE html>
<html lang="es">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reserva de Recursos - CED</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="adminlte/css/adminlte.min.css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
    <!-- DataTable -->
    <link rel="stylesheet" href="adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <!-- Toastr popups alerts -->
    <link rel="stylesheet" href="css/toastr.min.css">

    <!-- SCRIPTS REQUERIDOS -->
    <!-- jQuery -->
    <script src="js/jquery36.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
    <!-- Bootstrap 4 -->
    <script src="adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="adminlte/js/adminlte.js"></script>
    <!-- popper JS -->
    <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
    <!-- MomentJS   -->
    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <!-- SweetAlert -->
    <script src="js/sweetalert2.all.min.js"></script>
    <!-- DataTable -->
    <script type="text/javascript" charset="utf8" src="adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="adminlte/plugins/datatables-bs4\js\dataTables.bootstrap4.min.js"></script>
    <!-- Toastr popups alerts -->
    <script type="text/javascript" charset="utf8" src="js/toastr.min.js"></script>
    <!-- ChartJS graficas estadisticas -->
    <script type="text/javascript" charset="utf8" src="js/chart.min.js"></script> 
    <!-- Fin SCRIPTS REQUERIDOS -->

</head>
<body class="hold-transition layout-top-nav dark-mode ">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-dark">
        <div class="container">
            <a href="../../index3.html" class="navbar-brand">
                <img src="images/logoCEDMEDGOB.jpg" alt="Logo CED" class="brand-image" style="opacity: .8">
            </a>
            <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ route('home')}}" class="nav-link">Usuarios</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('reserva.visorSuper') }}" class="nav-link">Reservas</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('recurso.visor') }}" class="nav-link">Recursos</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('config.visor') }}" class="nav-link">Configuraciones</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('reportes') }}" class="nav-link">Reportes</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('log.visor') }}" class="nav-link">LOGs</a>
                    </li>
                </ul>

                <!-- derecha navbar links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('usuario.password') }}" role="button">
                            <i class="fas fa-key"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt"></i>
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- CONTENIDO PRINCIPAL -->
    @yield('contenido')
    <!-- FIN CONTENIDO PRINCIPAL -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Usuario Opciones</h5>
        </div>
    </aside>
    <!-- /.control-sidebar -->
    
    <!-- Main Footer -->
    <footer class="main-footer">
        <div class="float-right d-none d-sm-inline">
        Ministerio de Educación de Tucumán
        </div>
        <strong>&copy; 2022 <a href="https://coordinacion.educaciondigitaltuc.gob.ar">Coordinación de Educación Digital</a>.</strong> Todos los derechos reservados.
    </footer>
</div>
<!-- ./wrapper -->
</body>
</html>
    
    