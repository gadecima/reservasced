<!DOCTYPE html>
<!--Esto es el Layout para la sesión de USUARIO-->
<html lang="es">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reserva de Recursos - CED</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="adminlte/css/adminlte.min.css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
    <!-- DataTable -->
    <link rel="stylesheet" href="adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/keytable/2.7.0/css/keyTable.dataTables.min.css" />
    <!-- Toastr popups alerts -->
    <link rel="stylesheet" href="css/toastr.min.css">

    <!-- SCRIPTS REQUERIDOS -->
    <script src="js/jquery36.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->
    <!-- Bootstrap 4 -->
    <script src="adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="adminlte/js/adminlte.js"></script>
    <!-- popper JS -->
    <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
    <!-- MomentJS -->
    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <!-- SweetAlert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <!-- DataTable -->
    <script type="text/javascript" charset="utf8" src="adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="adminlte/plugins/datatables-bs4\js\dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
    <script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/keytable/2.7.0/js/dataTables.keyTable.min.js"></script>
    <!-- Toastr popups alerts -->
    <script type="text/javascript" charset="utf8" src="js/toastr.min.js"></script>
    
    <!-- Fin SCRIPTS REQUERIDOS -->

</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-primary navbar-dark">
    <div class="container">
      <a href="../../index3.html" class="navbar-brand">
        <img src="images/logoCEDMED.jpg" alt="logoCED" class="brand-image" style="opacity: .8">
        <span class="brand-text font-weight-light">Reserva de Recursos </span>
      </a>

      <!-- elementos derecha de la navbar  -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-user-circle">{{ '  '. Auth::user()->nombre.'  ' }}</i>
            </a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

    <!-- CONTENIDO PRINCIPAL -->
    @yield('contenido')
    <!-- FIN CONTENIDO PRINCIPAL -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Usuario Opciones</h5>
        <a class="nav-link" href="{{ route('usuario.perfil') }}">
            <i class="fas fa-user-edit"></i>
            {{ __('Editar perfil') }}
        </a>
        <a class="nav-link" href="{{ route('usuario.password') }}">
            <i class="fas fa-key"></i>
            {{ __('Cambiar Contraseña') }}
        </a>
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
            {{ __('Cerrar sesión') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Ministerio de Educación de Tucumán
    </div>
    <!-- Default to the left -->
    <strong>&copy; 2021 <a href="https://coordinacion.educaciondigitaltuc.gob.ar">Coordinación de Educación Digital</a>.</strong> Todos los derechos reservados.
  </footer>
</div>
<!-- ./wrapper -->

</body>
</html>