<!DOCTYPE html>
<html lang="es">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reserva de Recursos - CED</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Jquery-UI -->
    <link rel="stylesheet" type="text/css" href="jquery-ui-1131/jquery-ui.min.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="adminlte/plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="adminlte/css/adminlte.min.css">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
    <!-- DataTable -->
    <link rel="stylesheet" href="adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/keytable/2.7.0/css/keyTable.dataTables.min.css" />
    <!-- Toastr popups alerts -->
    <link rel="stylesheet" href="css/toastr.min.css">

    <!-- SCRIPTS REQUERIDOS -->
    <!-- jQuery -->
    <script src="js/jquery36.min.js"></script>
    <script src="jquery-ui-1131/jquery-ui.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="adminlte/js/adminlte.js"></script>
    <!-- popper JS -->
    <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
    <!-- MomentJS   -->
    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <!-- SweetAlert -->
    <script src="js/sweetalert2.all.min.js"></script>
    <!-- DataTable -->
    <script type="text/javascript" charset="utf8" src="adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="adminlte/plugins/datatables-bs4\js\dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>
    <script type="text/javascript" charset="utf8"  src="https://cdn.datatables.net/keytable/2.7.0/js/dataTables.keyTable.min.js"></script>
    <!-- Toastr popups alerts -->
    <script type="text/javascript" charset="utf8" src="js/toastr.min.js"></script>
    <!-- ChartJS graficas estadisticas -->
    <script type="text/javascript" charset="utf8" src="js/chart.min.js"></script> 
    <!-- Fin SCRIPTS REQUERIDOS -->

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- izquierda navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- derecha navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i class="fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </nav><!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="row">
        <a href="#" class="brand-link">
            <img src="images/logoCEDMED.jpg" alt="logoCED" class="brand-image" style="opacity: .8">
            <span class="brand-text font-weight-light"></span>
        </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="adminlte/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('home') }}" class="d-block">@auth {{ Auth::user()->nombre }} @endauth</a>
            </div>
        </div>

      <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('reservas.admin') }}" class="nav-link">
                        <i class="far fa-calendar-alt"></i>
                        <p>Reservas</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('prestamo.visor') }}" class="nav-link">
                        <i class="fas fa-shopping-cart"></i>
                        <p>Prestamos Express</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('recurso.visor') }}" class="nav-link">
                        <i class="fab fa-dropbox"></i>
                        <p> Recursos </p>
                    </a>
                </li>
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link">
                        <i class="fas fa-tools"></i>
                        <p>
                        Administración
                        <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('usuario.visor') }}" class="nav-link">
                                <i class="fas fa-users"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('reportes') }}" class="nav-link">
                                <i class="far fa-file-alt"></i>
                                <p>Reportes</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('log.visor') }}" class="nav-link">
                                <i class="fas fa-eye"></i>
                                <p>LOGs</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- CONTENIDO PRINCIPAL -->
  @yield('contenido')
  <!-- FIN CONTENIDO PRINCIPAL -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Usuario Opciones</h5>
        <a class="nav-link" href="{{ route('usuario.perfil') }}">
            <i class="fas fa-user-edit"></i>
            {{ __('Editar perfil') }}
        </a>
        <a class="nav-link" href="{{ route('usuario.password') }}">
            <i class="fas fa-key"></i>
            {{ __('Cambiar Contraseña') }}
        </a>
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>
            {{ __('Cerrar sesión') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Ministerio de Educación de Tucumán
    </div>
    <!-- Default to the left -->
    <strong>&copy; 2021 <a href="https://coordinacion.educaciondigitaltuc.gob.ar">Coordinación de Educación Digital</a>.</strong> Todos los derechos reservados.
  </footer>
</div>
<!-- ./wrapper -->
</body>

<script>
$(document).ready(function ($) {
    var url = window.location.href;
    var activePage = url;
    $('.nav-item a').each(function () {
        var linkPage = this.href;
        //console.log('linkPage:'+linkPage+'--activePage:'+activePage);

        if (activePage == linkPage) {
            $(this).closest("a").addClass("active");
        }
    });
});
</script>

</html>
