<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu contraseña fue reestablecida!',
    'sent' => 'Hemos enviado un link de restablecimiento a tu casilla de correo!',
    'throttled' => 'Por favor espera un momento, y vuelve a intentarlo.',
    'token' => 'El código de reestablecimiento de contraseña es invalido.',
    'user' => "No podemos encontrar ese correo en nuestros registros.",

];
