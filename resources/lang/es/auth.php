<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estás credenciales no coinciden con un registro guardado',
    'throttle' => 'Demasiados intentos de sesión. Intente nuevamente en:seconds segundos.',

];
