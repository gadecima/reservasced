<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRecurso extends Model{

    protected $fillable = [
        'tipo',
        'recu_accs',
        'reservable',
    ];

    public static function TipoSegunID($idTipo){
        return TipoRecurso::where('id', $idTipo)->value('tipo');
    }

    public static function esRecuoAccs($tipo){
        return TipoRecurso::where('id', $tipo)->value('recu_accs');
    }

    

}
