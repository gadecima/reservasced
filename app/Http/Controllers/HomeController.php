<?php

namespace App\Http\Controllers;
use App\EstadoReserva;
use App\Recurso;
use App\Reserva;
use App\Prestamo;
use App\User;
use App\Log;
use App\Role;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        Log::logNuevo(Auth::user()->id,'LogIn');
        switch (Auth::user()->rol_id) { 
            case '0'://Super
                $usuarios = User::orderBy('id','desc')->get();
                foreach($usuarios as $usuariosItem){
                    $usuariosItem->rol_nombre = Role::getRol($usuariosItem->rol_id);
                }
                return view('/superAdmin/homeSuper', compact('usuarios'));
            break;
            case '1': //Admin
                Reserva::marcarResVencidas();
                Reserva::marcarResAdeuda();
                $resAdeuda = Reserva::ResAdeudaAdmin(); //contador reservas adeudadas
                $resPreparar = Reserva::ReservasPreparar();
                $resEnCurso = Reserva::ReservasEnCurso();
                $resCanceladas = Reserva::ReservasCanceladas();

                Prestamo::marcarPrestAdeuda();
                $prestEnCurso = Prestamo::prestamosEnCurso();
                $prestAdeuda = Prestamo::prestamosDeuda();

                return view('/admin/homeAdmin', compact( 'resAdeuda','resPreparar','resEnCurso',
                                                        'resCanceladas','prestEnCurso','prestAdeuda'));
            break;
            case '2': //usuario: gestor
                return redirect()->route('reservas.usuario');
            break;
        }

    }

}
