<?php

namespace App\Http\Controllers;

use App\RecursoReserva;
use Illuminate\Http\Request;

class RecursoReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecursoReserva  $recursoReserva
     * @return \Illuminate\Http\Response
     */
    public function show(RecursoReserva $recursoReserva)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecursoReserva  $recursoReserva
     * @return \Illuminate\Http\Response
     */
    public function edit(RecursoReserva $recursoReserva)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecursoReserva  $recursoReserva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecursoReserva $recursoReserva)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecursoReserva  $recursoReserva
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecursoReserva $recursoReserva)
    {
        //
    }
}
