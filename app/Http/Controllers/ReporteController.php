<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recurso;
use App\TipoRecurso;
use Carbon\Carbon;
use App\Reserva;
use App\Prestamo;
use App\EstadoRecurso;
use App\EstadoReserva;
use App\RecursoReserva;
use App\RecursoPrestamo;
use App\User;
use Response;

class ReporteController extends Controller{

    public function reportes(){
        /*
        $prest = RecursoPrestamo::cuentaxTipoPrestado();
        $recus = RecursoReserva::cuentaxTipoReserv();

        return $prest;
        */
        
        $tiposRecu = TipoRecurso::orderBy('tipo','asc')->get();
        $estadosRecu = EstadoRecurso::orderBy('estado','asc')->get();
        $estadosRes = EstadoReserva::orderBy('estado_reserv','asc')->get();
        $users = User::select('id','nombre','apellido')->orderBy('apellido','asc')->get();
        $solicitantes = Prestamo::select('solicitante')->orderBy('solicitante','asc')->get();
        return view('/admin/reportes', compact('tiposRecu','estadosRecu','estadosRes','users','solicitantes') );
    }
    
    //este metodo funciona como API para entregar al chartJS datos en formato Json
    public function statsReservas(){ 
        $recus = RecursoReserva::cuentaxTipoReserv();
        foreach ($recus as $key => $value) {
            $recus[$key]->tipo = TipoRecurso::TipoSegunId( ($recus[$key]->tipo) );
            $json['tipo'][$key] = $recus[$key]->tipo;
            $json['cant'][$key] = $recus[$key]->tipos;
        }
        return Response::json($json);   
    }

    //este metodo funciona como API para entregar al chartJS datos en formato Json
    public function statsPrestamos(){ 
        $recus = RecursoPrestamo::cuentaxTipoPrestado();
        foreach ($recus as $key => $value) {
            $recus[$key]->tipo = TipoRecurso::TipoSegunId( ($recus[$key]->tipo) );
            $json['tipo'][$key] = $recus[$key]->tipo;
            $json['cant'][$key] = $recus[$key]->tipos;
        }
        return Response::json($json);   
    }
    
    public function statsResAnual(){ 
        $reservas = Reserva::cuentaXmes();
        foreach ($reservas as $key => $value) {
            $json['mes'][$key] = $reservas[$key]->mes;
            $json['cant'][$key] = $reservas[$key]->cant;
        }
        return Response::json($json);   
    }

    public function statsPrestAnual(){ 
        $prestamos = Prestamo::cuentaXmes();
        foreach ($prestamos as $key => $value) {
            $json['mes'][$key] = $prestamos[$key]->mes;
            $json['cant'][$key] = $prestamos[$key]->cant;
        }
        return Response::json($json);   
    }
}
