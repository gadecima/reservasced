<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;
use App\Recurso;
use App\Reserva;
use App\TipoRecurso;
use App\RecursoReserva;
use App\User;
use App\EstadoReserva;
use Carbon\Carbon;
use App\Log;
use App\Mail\mailabReservaRecib;
use App\Mail\mailabReservaCancel;
use App\Mail\mailabReservaSuspen;
use App\Mail\mailabReservaPrepara;
use App\Mail\mailabReservaNueva;
use \PDF;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;

class ReservaController extends Controller{

//*******METODOS PARA SUPER***********
public function resVisorSuper(){
    Reserva::marcarResVencidas();
    Reserva::marcarResAdeuda();
    $reservas = Reserva::orderBy('created_at','desc')->get();
    foreach($reservas as $reservasItem){
        $reservasItem->fecha_desde = date("d/m/Y", strtotime($reservasItem->fecha_desde));
        $reservasItem->fecha_hasta = date("d/m/Y", strtotime($reservasItem->fecha_hasta));
        $reservasItem->est_reserv_id = EstadoReserva::GetEstado($reservasItem->est_reserv_id);
        //$reservasItem->created_at = Reserva::getCreatedAtAttribute($reservasItem->created_at); //no h ace falta xq esta formatiado en la clase
        $reservasItem->user_id = User::GetUsuario($reservasItem->user_id)->apellido.' '.User::GetUsuario($reservasItem->user_id)->nombre;
    }
    Log::logNuevo(Auth::user()->id,'LogIn');
    return view('/superAdmin/resVisorSuper', compact('reservas') );
}

public function reservaEliminar($id){
    $reserva = Reserva::find($id);
    $result = $reserva->delete();
    return redirect()->route('reserva.visorSuper')->with('result',$result ? '1' : '0');
}

//*******METODOS PARA ADMIN***********
    public function reservasAdmin(){
        $reservas = Reserva::orderBy('created_at','desc')->get();
        Reserva::marcarResVencidas();
        Reserva::marcarResAdeuda();
        $resAdeuda = Reserva::ResAdeudaAdmin(); //contador reservas adeudadas
        $resPreparar = Reserva::ReservasPreparar();
        $resEnCurso = Reserva::ReservasEnCurso();
        $resCanceladas = Reserva::ReservasCanceladas();

        foreach($reservas as $reservasItem){
            $reservasItem->fecha_desde = date("d/m/Y", strtotime($reservasItem->fecha_desde));
            $reservasItem->fecha_hasta = date("d/m/Y", strtotime($reservasItem->fecha_hasta));
            $reservasItem->est_reserv_id = EstadoReserva::GetEstado($reservasItem->est_reserv_id);
            $reservasItem->created_at = date('d-m-Y H:i:s',strtotime($reservasItem->created_at));
            $reservasItem->user_id = User::GetUsuario($reservasItem->user_id)->apellido.' '.User::GetUsuario($reservasItem->user_id)->nombre;
        }
        return view('/admin/reservasAdmin', compact('reservas','resPreparar','resEnCurso','resCanceladas', 'resAdeuda'));
    }

    public function reservaVerAdm($id){
        $recursos = RecursoReserva::recursosByReservaId($id); //$recursos[collection]
        $reserva = Reserva::ReservaById($id);
        
        foreach ($recursos as $key => $value) {
            $recu[$key] = Recurso::RecursoSegunId($value->recurso_id);
            $recu[$key]->tipo = TipoRecurso::TipoSegunId( ($recu[$key]->tipo) );
            $recu[$key]->cant = $value->cant_res;
            $reservados = collect($recu); //convierto arreglo en collection.
        }

        return view('/admin/reservaVerAdm',compact('reservados'))
            ->with('reserva', $reserva)
            ->with('creador', User::GetUsuario($reserva->user_id))
            ->with('estado_reserv', EstadoReserva::GetEstado($reserva->est_reserv_id));
    }

    //El Admin cambia estado de la reserva.
    public function cambiaEstado($id,$idEstado){
        $reserva = Reserva::find($id);
        $reserva->est_reserv_id = $idEstado;
        $result = $reserva->save();
        switch ($idEstado) {
            case '5':
                Mail::to( User::mailUser($reserva->user_id) )->send(new mailabReservaSuspen($reserva));
            break;
            case '3':
                /*aqui debo preparar el PDF para adjuntar al mail*/
                $recursos = RecursoReserva::recursosByReservaId($id); //$recursos[matriz]
                foreach ($recursos as $key => $value) {
                    $recu[$key] = Recurso::RecursoSegunId($value->recurso_id);
                    $recu[$key]->tipo = TipoRecurso::TipoSegunId( ($recu[$key]->tipo) );
                    $reservados = collect($recu); //convierto arreglo en collection.
                }
                $pdf = PDF::loadView('/pdf/actaReserva', 
                                        ['reservados' => $reservados,
                                        'codigo' => $id,
                                        'fecha_hasta' => Carbon::parse($reserva->fecha_hasta)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y'),
                                        'hora_hasta' => $reserva->hora_hasta,
                                        'creador' => User::GetUsuario($reserva->user_id),
                                        'hoy' => Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')
                                        ])
                        ->setPaper('a4', 'portrait');
                $outputPDF = $pdf->output();
                Mail::to(User::mailUser($reserva->user_id) )->send(new mailabReservaPrepara($reserva,$outputPDF));
                
                //Mail::to( User::mailUser($reserva->user_id) )->send(new mailabReservaPrepara($reserva));
            break;
        }
        Log::logNuevo(Auth::user()->id,'Editar','Cambia Estado Reserva: #'.$id);
        if ( Auth::user()->rol_id == 0) {
            return redirect()->route('reserva.visorSuper')->with('result', $result ? '1' : '0');
        }else {
            return redirect()->route('reservas.admin')->with('result', $result ? '1' : '0');
        }
    }

    // Comentario del Admin a una reserva
    public function reservaComent(Request $request){
        $data = ['comentario' => $request->comentario ];
        return $result = Reserva::find($request->id)->update($data);
    }

    //El Admin cambia un recurso de una reserva por algun motivo.
    public function reservaRecuCambiar($idReserva, $idRecu){
        $tipoRecu = Recurso::RecursoSegunId($idRecu)->tipo;
        $recuOrAccs = TipoRecurso::esRecuoAccs($tipoRecu);
        $reserva = Reserva::ReservaById($idReserva);
        $request = new Request( array($reserva->fecha_desde,
                                        $reserva->fecha_hasta,
                                        $reserva->hora_desde,
                                        $reserva->hora_hasta) );
        
        $recursos = Recurso::RecursoDisponible($request, [$tipoRecu], $recuOrAccs);

        return view('/admin/recursoCambiar')
                ->with('idReserva', $idReserva)
                ->with('idRecu', $idRecu)
                ->with('recursos', $recursos);
    }

    //Con el recurso a cambiar seleccionado se guarda los cambios en la reserva.
    public function reservaRecuCambiado(Request $request){
        //return $request;
        return $result = RecursoReserva::where('recurso_id',$idRecuViejo)
                            ->where('reserva_id',$idReserva)
                            ->get();
        /*        	
        try {
			if ($result = RecursoReserva::find($idReserva)) {
				Log::logNuevo(Auth::user()->id,'Eliminar','Eliminar Recurso: #'.$id);
				//return redirect()->route('recurso.visor',)->with('result', $result ? '1' : '0' );
			}else {
				//return redirect()->route('recurso.visor',)->with('result', $result ? '1' : '0' );	
			}
		}
		catch (\Exception $e) {
            //return redirect()->route('recurso.visor')->with('result', '0');
        }

$idRecuNuevo	
$idReserva	
$idRecuViejo
*/
        
        //return view('/admin/recursoCambiar', compact('recursos') );
    }
        
//*******METODOS PARA USUARIO***********
    //visor de reservas usuario.
    public function reservasUsuario(){
        Reserva::marcarResVencidas();
        Reserva::marcarResAdeuda();
        $reservUser = Reserva::ReservaByUser(Auth::user()->id);
        $resAdeuda = Reserva::ReservasAdeuda();
        $resListas = Reserva::ReservasListas();
        $resConcluidas = Reserva::ReservasConcluidas();
        foreach($reservUser as $reservUserItem){
            $reservUserItem->fecha_desde = date("d/m/Y", strtotime($reservUserItem->fecha_desde));
            $reservUserItem->fecha_hasta = date("d/m/Y", strtotime($reservUserItem->fecha_hasta));
            $reservUserItem->est_reserv_id = EstadoReserva::GetEstado($reservUserItem->est_reserv_id);
            $reservUserItem->created_at = date('d-m-Y H:i:s',strtotime($reservUserItem->created_at));
        }
        return view('/usuario/homeUsuario', compact('reservUser','resAdeuda','resListas','resConcluidas'));
    }

    //El usuario visualiza los detalles de una determinada reserva
    public function reservaVer($id){
        $recursos = RecursoReserva::recursosByReservaId($id); //$recursos[matriz]
        $reserva = Reserva::ReservaById($id);
        $resumen = RecursoReserva::cuentaXtipoEnReserva($id); //resumen de recursos reservados, devuelve cantidades x tipo.
        
        foreach ($recursos as $key => $value) {
            $recu[$key] = Recurso::RecursoSegunId($value->recurso_id);
            //guardo cada array en colecciones y luego las unifico para hacer una.
            $recu[$key]->tipo = TipoRecurso::TipoSegunId( ($recu[$key]->tipo) );
            $reservados = collect($recu);
        }
        return view('/usuario/reservaVer',compact('reservados'))
                        ->with('reserva', $reserva)
                        ->with('resumen', $resumen)
                        ->with('creador', User::GetUsuario($reserva->user_id))
                        ->with('estado_reserv', EstadoReserva::GetEstado($reserva->est_reserv_id));
    }

    //El usuario creará una nueva reserva
    public function reservaNueva(){
        return view('/usuario/reservaNuevaUsr');
    }

    //El usuario selecciona los recursos para su nueva reserva
    public function reservaSelecRecursos(Request $request){
    	$cantNbksDispon = Recurso::RecursoDisponible($request, [1],'1')->count();
    	$cantTblDispon = Recurso::RecursoDisponible($request, [2],'1')->count();

        $tipos_recu = TipoRecurso::where('recu_accs', '1')
                        ->where('id','!=',1) //no necesito las tipo netbooks/tablets
                        ->where('id','!=',2)
                        ->pluck('id');
        $recursos = Recurso::RecursoDisponible($request, $tipos_recu,'1');

        $tipos_accs = TipoRecurso::where('recu_accs', '0')->pluck('id');
        $accesorios = Recurso::RecursoDisponible($request, $tipos_accs,'0');
                
        return view('/usuario/reservaSelecRecursos',
                        compact('cantNbksDispon', 
                                'cantTblDispon',
                                'recursos',
                                'accesorios'
                                ) )
    					->with('dt_desde_fecha', $request->dt_desde_fecha)
    					->with('dt_hasta_fecha', $request->dt_hasta_fecha)
                        ->with('dt_desde_hora', $request->dt_desde_hora)
                        ->with('dt_hasta_hora', $request->dt_hasta_hora);
    }

    //El usuario observa un resumen de los recursos seleccionados para su nueva reserva
    public function reservaResumen(Request $request){
        //return $request->all();
        $NbkRes = Recurso::RecuReservByCant($request,[1],$request->selNbk);
        $TblRes = Recurso::RecuReservByCant($request,[2],$request->selTbl);
        $reservados = array();
        $i = 0;
        if (!empty($request->accs)) { //accs es un arreglo 
            foreach ($request->accs as $key => $value) {
                if (!is_null($value['cantSel'])){
                    $reservados[$i] = Recurso::RecursoSegunId($value['recuId']);
                    $reservados[$i]['tipo'] = TipoRecurso::TipoSegunId($reservados[$i]['tipo']);
                    $reservados[$i]['cant'] = (int)$value['cantSel'];
                    $i = $i + 1;
                }
            }
        }
        if (!empty($request->recus)) {
            foreach ($request->recus as $key => $value) {
                $reservados[$i] = Recurso::RecursoSegunId($value);
                $reservados[$i]['tipo'] = TipoRecurso::TipoSegunId($reservados[$i]['tipo']);
                $i = $i + 1;
            }
        }
        $reservados = collect([$NbkRes,$TblRes,$reservados]);
        $reservados = $reservados->collapse(); //uno todas las collection en una
        
        if( $reservados->isNotEmpty() ){
            return view('/usuario/reservaResumen',compact('reservados'))
            ->with('selNbk', $request->selNbk)
            ->with('selTbl', $request->selTbl)
            ->with('dt_desde_fecha', $request->dt_desde_fecha)
            ->with('dt_hasta_fecha', $request->dt_hasta_fecha)
            ->with('dt_desde_hora', $request->dt_desde_hora)
            ->with('dt_hasta_hora', $request->dt_hasta_hora)
            ->with('observ', $request->observ);
        }
        else{  
            return redirect()->route('reserva.nueva')->with('error','1');
        }
    }

    //USUARIO CONFIRMA LA RESERVA CON LOS RECURSOS SELECCIONADOS 
    public function ReservaConfirmada(Request $request){
        $desde = Carbon::parse($request->dt_desde_fecha)->locale('es')->format('Y-m-d');
        $hasta = Carbon::parse($request->dt_hasta_fecha)->locale('es')->format('Y-m-d');
        $h_desde = Carbon::parse($request->dt_desde_hora)->locale('es')->format('H:i:s');
        $h_hasta = Carbon::parse($request->dt_hasta_hora)->locale('es')->format('H:i:s');
        
        try {
            Reserva::create([
                'user_id' => Auth::user()->id,
                'fecha_desde' => $desde,
                'hora_desde' => $h_desde,
                'fecha_hasta' => $hasta,
                'hora_hasta' => $h_hasta,
                'est_reserv_id' => 1, //por defecto las reservas se confirman
                'observ' => $request->observ,
            ]);
            $reservaID = Reserva::ReservaUltimaXuser( Auth::user()->id )->id;
            foreach ($request->reservados as $reservadosItem) {
                RecursoReserva::create(['reserva_id' => $reservaID, 'recurso_id'=> $reservadosItem ]);
            }
            $reserva = Reserva::reservaById($reservaID);
            $reservados = RecursoReserva::cuentaXtipoEnReserva($reservaID);
            Mail::to(Auth::user()->email)->send(new mailabReservaRecib($reserva,$reservados));

            $adminsMails = User::mailsAdmins(); //email hacia los Administradores
            Mail::to($adminsMails)->send(new mailabReservaNueva($reserva));

            Log::logNuevo(Auth::user()->id,'Crear','Crea nueva reserva: #'.$reservaID);
            return view('/usuario/reservaConfirmada', compact('reservaID') );

        } catch (\Exception $e) {
            return redirect()->route('reservas.usuario')->with('result', '0');
        }
        //return new mailabReservaRecib($reservaID); //ver email enviado en navegador
    }

    //El usuario Cancela una determinada reserva (el registro no se elimina, cambia de estado)
    public function reservaCancelar($id){
        $adminsMails = User::mailsAdmins();
        $reserva = Reserva::find($id);
        $reserva->est_reserv_id = '7';
        $result  = $reserva->save();
       
        Mail::to($adminsMails)->send(new mailabReservaCancel($reserva));
        Log::logNuevo(Auth::user()->id,'Editar','Canceló reserva: #'.$id);
        return redirect()->route('reservas.usuario')->with('result', $result ? '1' : '0');
    }

    //public function PruebaMailReserva($reservaID){    }

}
