<?php

namespace App\Http\Controllers;

use App\EstadoReserva;
use Illuminate\Http\Request;

class ReservaEstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstadoReserva  $estadoReserva
     * @return \Illuminate\Http\Response
     */
    public function show(EstadoReserva $estadoReserva)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstadoReserva  $estadoReserva
     * @return \Illuminate\Http\Response
     */
    public function edit(EstadoReserva $estadoReserva)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstadoReserva  $estadoReserva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstadoReserva $estadoReserva)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstadoReserva  $estadoReserva
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstadoReserva $estadoReserva)
    {
        //
    }
}
