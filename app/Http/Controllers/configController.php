<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Config;
use App\Log;
use Auth;

class configController extends Controller
{
    public function configVisor(){
        return view('/superAdmin/configVisor');
    }

    public function validarEditar(array $data) {
        return Validator::make($data, [
        	'diasAntes' => ['required','numeric','min:1', 'max:28'],
	        'diasUso' => ['required','numeric','min:1', 'max:28'],
	        'desde_hora' => ['required'],
	        'hasta_hora' => ['required','horaMayorQue:desde_hora']
	    ]);
    }

    public function configActualizar(Request $request){
        $this->validarEditar($request->all())->validate(); 
        $data = [
            'dias_antes' => request('diasAntes'),
            'dias_uso' => request('diasUso'),
            'desde_hora' => request('desde_hora'),
            'hasta_hora' => request('hasta_hora'),
        ];
        if(!isset($request->swtResOn)){
            $data['reservas_on'] = "0";
        } else{
            $data['reservas_on'] = "1";
        }
        
        if ( $result = Config::find('1')->update($data) ) {
            Log::logNuevo(Auth::user()->id,'Editar','Editar Configuración');
            return redirect()->route('config.visor')->with('result', '1');
        }else {
            return redirect()->route('usuario.visor')->with('result', '0');
        }
        /*
        return $data;
        */
    }
}
