<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public static function logVisor(){
        $logs = Log::orderBy('id','desc')->get();
        foreach ($logs as $logsItem) {
            $logsItem->created_at = Log::getCreatedAtAttribute($logsItem->created_at);
            $logsItem->evento_usr = User::GetUsuario($logsItem->evento_usr)->apellido.', '.User::GetUsuario($logsItem->evento_usr)->nombre;
        }
        return view('/admin/logVisor', compact('logs'));
    }

    
}
