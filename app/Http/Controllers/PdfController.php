<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;
use App\Recurso;
use App\Reserva;
use App\Prestamo;
use App\TipoRecurso;
use App\RecursoReserva;
use App\RecursoPrestamo;
use App\EstadoRecurso;
use App\User;
use App\EstadoReserva;
use App\Role;
use App\Log;
use \PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class PdfController extends Controller
{
    //Exporta las reservas del Usuario de la vista HomeUsuario
    public function ExportUsrReservas(){
        $reservUser = Reserva::ReservaByUser(Auth::user()->id);
        foreach($reservUser as $reservUserItem){
            $reservUserItem->fecha_desde = date("d/m/Y", strtotime($reservUserItem->fecha_desde));
            $reservUserItem->fecha_hasta = date("d/m/Y", strtotime($reservUserItem->fecha_hasta));
            $reservUserItem->est_reserv_id = EstadoReserva::GetEstado($reservUserItem->est_reserv_id);
            $reservUserItem->created_at = date('d-m-Y H:i:s',strtotime($reservUserItem->created_at));
        }
        //pasar parametros al pdf PDF::loadView('vista',['index' => $valor]);
        $pdf = PDF::loadView('/pdf/userReservas', ['reservUser' => $reservUser])
                        ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','listado de reservas');                
        return $pdf->download('MisReservas.pdf');
    }

    public function ExportAdmReservas(){
        $reservas = Reserva::orderBy('created_at','desc')->get();
        foreach($reservas as $reservasItem){
                $reservasItem->fecha_desde = date("d/m/Y", strtotime($reservasItem->fecha_desde));
                $reservasItem->fecha_hasta = date("d/m/Y", strtotime($reservasItem->fecha_hasta));
                $reservasItem->est_reserv_id = EstadoReserva::GetEstado($reservasItem->est_reserv_id);
                //$reservasItem->created_at = date('d-m-Y H:i:s',strtotime($reservasItem->created_at)); //no h ace falta xq esta formatiado en la clase
                $reservasItem->user_id = User::GetUsuario($reservasItem->user_id)->apellido.' '.User::GetUsuario($reservasItem->user_id)->nombre;
        }
        $pdf = PDF::loadView('/pdf/admReservas', ['reservas' => $reservas])
                        ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','listado de reservas');                        
        return $pdf->download('TodasReservas.pdf');
    }

    public function ActaReserva($id){
        $recursos = RecursoReserva::recursosByReservaId($id); //$recursos[matriz]
        $reserva = Reserva::reservaById($id);
        foreach ($recursos as $key => $value) {
            $recu[$key] = Recurso::RecursoSegunId($value->recurso_id);
            $recu[$key]->tipo = TipoRecurso::TipoSegunId( ($recu[$key]->tipo) );
            $reservados = collect($recu); //convierto arreglo en collection.
        }
        $pdf = PDF::loadView('/pdf/actaReserva', 
                                ['reservados' => $reservados,
                                 'codigo' => $id,
                                 'fecha_hasta' => Carbon::parse($reserva->fecha_hasta)->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y'),
                                 'hora_hasta' => $reserva->hora_hasta,
                                 'creador' => User::GetUsuario($reserva->user_id),
                                 'hoy' => Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y')
                                ])
                ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Acta de reserva:'.$id);
        return $pdf->download('actaReserva'.$id.'.pdf');
    }

    public function ActaPrestamo($id){
        $recursos = RecursoPrestamo::recursosByPrestamoId($id); //$recursos[matriz]
        $prestamo = Prestamo::prestamoById($id);
        foreach ($recursos as $key => $value) {
            $recu[$key] = Recurso::RecursoSegunId($value->recurso_id);
            $recu[$key]->tipo = TipoRecurso::TipoSegunId( ($recu[$key]->tipo) );
            $prestados = collect($recu); //convierto arreglo en collection.
        }
        $pdf = PDF::loadView('/pdf/actaPrestamo', 
                                ['prestados' => $prestados,
                                 'prestamo' => $prestamo,
                                 'hoy' => Carbon::now()->locale('es')->isoFormat('dddd D \d\e MMMM \d\e\l Y'),
                                 'responsable' => Auth::user()->apellido.','.Auth::user()->nombre
                                ])
                ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Acta de prestamo:'.$id);
        return $pdf->download('prestamo'.$id.'.pdf');
    }

    //Exporta los PRESTAMOS de la vista HomeAdmin->prestamos
    public function exportPrestamos(){
        $prestamos = Prestamo::orderBy('created_at','desc')->get();
        foreach($prestamos as $prestamosItem){
            $prestamosItem->fecha_prestado = Carbon::parse($prestamosItem->fecha_prestado)->locale('es')->format('d-m-Y H:i:s');
            if($prestamosItem->fecha_devuelto == NULL){
                $prestamosItem->fecha_devuelto = '-';
            }else{
                $prestamosItem->fecha_devuelto = Carbon::parse($prestamosItem->fecha_devuelto)->locale('es')->format('d-m-Y H:i:s');
            }
        }
        $pdf = PDF::loadView('/pdf/admPrestamos', ['prestamos' => $prestamos])
                        ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Listado de Prestamos');
        return $pdf->download('TodosPrestamos.pdf');
    }

    //Exporta TODOS los RECURSOS de la vista HomeAdmin->recursosVisor
    public function exportRecursos(){
        $recursos = Recurso::orderBy('created_at','desc')->get();
        foreach($recursos as $recursosItem){
            $recursosItem->tipo = TipoRecurso::TipoSegunId($recursosItem->tipo);
            $recursosItem->estado = EstadoRecurso::GetEstadoRecu($recursosItem->estado);
        }
        $pdf = PDF::loadView('/pdf/admRecursos', ['recursos' => $recursos])
                        ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Listado de Recursos');
        return $pdf->download('TodosRecursos.pdf');
    }

    //Exporta DETALLES de un determinado RECURSO
    public function detalleRecurso($id){
        $recurso = Recurso::RecursoSegunId($id);
        $recurso->tipo = TipoRecurso::TipoSegunId(($recurso->tipo));
        $recurso->estado = EstadoRecurso::GetEstadoRecu($recurso->estado);
        $pdf = PDF::loadView('/pdf/detalleRecurso',['recurso' => $recurso])
                ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Detalle de recurso:'.$id);
        return $pdf->download('detalle_recurso_'.$id.'.pdf');
    }

    public function exportUsuarios(){
        $usuarios = User::orderBy('created_at','desc')->get();
        foreach($usuarios as $usuariosItem){
            $usuariosItem->rol_id = Role::getRol($usuariosItem->rol_id);
        }
        $pdf = PDF::loadView('/pdf/admUsuarios', ['usuarios' => $usuarios])
                        ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Listado de Usuarios');
        return $pdf->stream('TodosUsuarios.pdf');
    }

    public function reporteRecursos($xTipo, $xEstado, $xDispon){
        Log::logNuevo(Auth::user()->id,'Exportar','Reporte de recursos');
        $recursos = Recurso::reportRecursos($xTipo, $xEstado, $xDispon);
        foreach($recursos as $recursosItem){
            $recursosItem->tipo = TipoRecurso::TipoSegunId($recursosItem->tipo);
            $recursosItem->estado = EstadoRecurso::GetEstadoRecu($recursosItem->estado);
        }
        $pdf = PDF::loadView('/pdf/reporteRecursos', ['recursos' => $recursos])
                        ->setPaper('a4', 'portrait');
        return $pdf->download('ReporteRecursos.pdf');
    }

    public function reporteReservas($xEstadoRes,$xUser,$xDesde,$xHasta){
        Log::logNuevo(Auth::user()->id,'Exportar','Reporte de Reservas');
        $reservas = Reserva::reportReservas($xEstadoRes,$xUser,$xDesde,$xHasta);
        foreach($reservas as $reservasItem){
            $reservasItem->fecha_desde =Carbon::parse($reservasItem->fecha_desde)->locale('es')->format('d-m-Y');
            $reservasItem->fecha_hasta =Carbon::parse($reservasItem->fecha_desde)->locale('es')->format('d-m-Y');
            $reservasItem->est_reserv_id = EstadoReserva::GetEstado($reservasItem->est_reserv_id);
            $reservasItem->created_at = Carbon::parse($reservasItem->created_at)->locale('es')->format('d-m-Y');
            $reservasItem->user_id = User::GetUsuario($reservasItem->user_id)->apellido.' '.User::GetUsuario($reservasItem->user_id)->nombre;
        }
        $pdf = PDF::loadView('/pdf/reporteReservas', ['reservas' => $reservas])
                        ->setPaper('a4', 'portrait');
        return $pdf->download('ReporteReservas.pdf');
    }

    public function reportePrestamos($xEstadoPrest,$xSolicitante,$xDesde,$xHasta){
        Log::logNuevo(Auth::user()->id,'Exportar','Reporte de Prestamos');
        $prestamos = Prestamo::reportPrestamos($xEstadoPrest,$xSolicitante,$xDesde,$xHasta); //me falta hacer desde aqui
        
        foreach($prestamos as $prestamosItem){
            $prestamosItem->fecha_prestado = Carbon::parse($prestamosItem->fecha_prestado)->locale('es')->format('d/m/Y H:i:s');
            if ($prestamosItem->estado === 'Devuelto') {
                $prestamosItem->fecha_devuelto =Carbon::parse($prestamosItem->fecha_devuelto)->locale('es')->format('d/m/Y H:i:s');
            }
        }
        $pdf = PDF::loadView('/pdf/reportePrestamos', ['prestamos' => $prestamos])->setPaper('a4', 'portrait');
        return $pdf->download('ReportePrestamos.pdf');
    }

    public function exportLog(){
        $logs = Log::orderBy('created_at','desc')->get();
        foreach($logs as $logsItem){
            $logsItem->created_at = Log::getCreatedAtAttribute($logsItem->created_at);
            $logsItem->evento_usr = User::GetUsuario($logsItem->evento_usr)->apellido.' '.User::GetUsuario($logsItem->evento_usr)->nombre;
        }
        $pdf = PDF::loadView('/pdf/reporteLog', ['logs' => $logs])
                        ->setPaper('a4', 'portrait');
        Log::logNuevo(Auth::user()->id,'Exportar','Reporte LOG');
        return $pdf->download('ReporteLog.pdf');
    }

    public function recuHistory($recu){
        Log::logNuevo(Auth::user()->id,'Exportar','Reporte Histórico de recurso: id#'.$recu);
        $historic = Recurso::recursoHistory($recu);
        foreach($historic as $historicItem){
			$historicItem->f_desde = Carbon::parse($historicItem->f_desde)->locale('es')->format('d-m-Y H:i:s');
			$historicItem->f_hasta = Carbon::parse($historicItem->f_hasta)->locale('es')->format('d-m-Y H:i:s');
		}
		$codigo = Recurso::RecursoSegunId($recu)->codigo;

        $pdf = PDF::loadView('/pdf/reporteRecuHistoric',['historic' => $historic, 'codigo' => $codigo])
                ->setPaper('a4', 'portrait');
        return $pdf->download('historico_recurso_'.$codigo.'.pdf');
    }

}
