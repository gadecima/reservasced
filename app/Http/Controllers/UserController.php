<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Log;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller{
    
    public function usuarioVisor(){
        $usuarios = User::orderBy('created_at','desc')->get();
        foreach($usuarios as $usuariosItem){
            $usuariosItem->rol_nombre = Role::getRol($usuariosItem->rol_id);
        }
        return view('/admin/usuarioVisor', compact('usuarios'));
    }

    public function usuarioNuevo(){
        if ( Auth::user()->rol_id == 0) {
            return view('/superAdmin/usrNuevoSuper');
        }else {
            return view('/admin/usuarioNuevo');
        }
    }

    public function validarAgregar(array $data) {
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'dni' => ['required', 'numeric','min:1111111','max:99999999', 'unique:users'],
            'telefono' => ['required', 'string', 'max:15'],
            'cargo' => ['required', 'string', 'max:100'],
            'dependencia' => ['required', 'string', 'max:100'],
            'ofi_area' => ['required', 'string', 'max:255'],
            'password' => ['required', 'min:8'],
            'pass_confirm' => ['required', 'min:8', 'same:password']
        ], [
            'email.unique' => 'Este eMail ya está registrado',
            'password.min' => 'Debe tener un mínimo de 8 caracteres',
            'pass_confirm.same' => 'Las Contraseñas no coinciden'
        ]);
    }

    public function usuarioAgregar(Request $request){
        $this->validarAgregar($request->all())->validate();
        $result = User::create([
            'nombre' => request('nombre'),
            'apellido' => request('apellido'),
            'email' => request('email'),
            'dni' => request('dni'),
            'telefono' => request('telefono'),
            'cargo' => request('cargo'),
            'dependencia' => request('dependencia'),
            'ofi_area' => request('ofi_area'),
            'rol_id' => request('rol_id'),
            'password' => Hash::make(request('password'))
        ]);
        if ( Auth::user()->rol_id == 0 ) {
            Log::logNuevo(Auth::user()->id,'Agregar Usuario','Agregó a: #'.$request->email);
            return redirect()->route('home')->with('result', $result ? '1' : '0');
        }else {
            Log::logNuevo(Auth::user()->id,'Agregar Usuario','Agregó a: #'.$request->email);
            return redirect()->route('usuario.visor')->with('result', $result ? '1' : '0');
        }
    }

    public function usuarioEditar($id){
        $usuario = User::find($id);
        if ( Auth::user()->rol_id == 0) {
            return view('/superAdmin/usrEditarSuper', compact('usuario'));
        }else {
            return view('/admin/usuarioEditar', compact('usuario'));
        }
    }

    public function validarActualizar($id, array $data){ 
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users','email')->ignore($id)],
            'dni' => ['required', 'numeric','min:1111111','max:99999999',Rule::unique('users','dni')->ignore($id)],
            'telefono' => ['required', 'string', 'max:15'],
            'cargo' => ['required', 'string', 'max:100'],
            'dependencia' => ['required', 'string', 'max:100'],
            'ofi_area' => ['required', 'string', 'max:255'],
            'password' => ['min:8', 'nullable']
        ]);
    }

    public function usuarioActualizar($id, Request $request){
        $this->validarActualizar($id, array_filter($request->all()) )->validate(); 
        $data = [
            'nombre' => request('nombre'),
            'apellido' => request('apellido'),
            'email' => request('email'),
            'dni' => request('dni'),
            'telefono' => request('telefono'),
            'cargo' => request('cargo'),
            'dependencia' => request('dependencia'),
            'ofi_area' => request('ofi_area'),
            'rol_id' => request('rol_id'),
        ];
        if ( !is_null($request->password) ){ //ignora el input si esta vacio/null
            $data['password'] = Hash::make(request('password'));
        }
        $result = User::find($id)->update($data);
        if ( Auth::user()->rol_id == 0) {
            Log::logNuevo(Auth::user()->id,'Editar Usuario','Editó a:'.$request->email);
            return redirect()->route('home')->with('result', $result ? '1' : '0');
        }else {
            Log::logNuevo(Auth::user()->id,'Editar Usuario','Editó a:'.$request->email);
            return redirect()->route('usuario.visor')->with('result', $result ? '1' : '0');
        }
    }

    public function usuarioEliminar($id){
        $usr_mail = User::GetUsuario($id)->email;
        try {
            $result = User::destroy($id);
            if ( Auth::user()->rol_id == 0) {
                Log::logNuevo(Auth::user()->id,'Eliminó Usuario','eliminó a:'.$usr_mail);
                return redirect()->route('home')->with('result', $result ? '1' : '0');
            }else {
                Log::logNuevo(Auth::user()->id,'Eliminó Usuario','eliminó a:'.$usr_mail);
                return redirect()->route('usuario.visor')->with('result', $result ? '1' : '0');
            }
        } catch (\Exception $e) {
            if ( Auth::user()->rol_id == 0) {
                return redirect()->route('home')->with('result', '0');
            }else {
                return redirect()->route('usuario.visor')->with('result', '0');
            }

        }
    }

    public function usuarioPerfil(){
        $usuario = User::find(Auth::user()->id);
        return view('/usuario/usuarioPerfil', compact('usuario'));
    }

    public function validarPerfil(array $data){ 
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', Rule::unique('users','email')->ignore(Auth::user()->id)],
            'dni' => ['required', 'numeric','min:1111111','max:99999999',Rule::unique('users','dni')->ignore(Auth::user()->id)],
            'telefono' => ['required', 'string', 'max:15'],
            'cargo' => ['required', 'string', 'max:100'],
            'dependencia' => ['required', 'string', 'max:100'],
            'ofi_area' => ['required', 'string', 'max:255']
        ]);
    }

    public function usrPerfilActualizar(Request $request){
        $this->validarPerfil( array_filter($request->all()) )->validate(); 
        $data = [
            'nombre' => request('nombre'),
            'apellido' => request('apellido'),
            'email' => request('email'),
            'dni' => request('dni'),
            'telefono' => request('telefono'),
            'cargo' => request('cargo'),
            'dependencia' => request('dependencia'),
            'ofi_area' => request('ofi_area'),
        ];
        $result = User::find(Auth::user()->id)->update($data);

        return redirect()->route( 'home')->with('result',$result ? '1' : '0');
    }

    public function usuarioPassword(){
        return view('/usuario/usuarioPassword');
    }

    public function validarPassword(array $data){ 
        return Validator::make($data, [
            'passVieja' => ['required', 'string',function ($attribute, $value, $fail) {
                if (!\Hash::check($value, Auth::user()->password)) {
                    return $fail(__('La contraseña anterior no es correcta'));
                }
            }],
            'passNueva' => ['required', 'string','min:8', 'different:passVieja'],
            'passNueva2' => ['required', 'string','min:8','same:passNueva']
        ], [
            'passNueva.different' => 'La contraseña Nueva debe ser distinta de la contraseña anterior',
            'passNueva2.same' => 'Las Contraseñas no coinciden'
        ]);
    }

    public function usrPassActualizar(Request $request){
        $this->validarPassword( array_filter($request->all()) )->validate(); 
        $data = [
            'password' => Hash::make(request('passNueva')),
        ];
        $result = User::find(Auth::user()->id)->update($data);
        return redirect()->route( 'home')->with('result',$result ? '1' : '0');
    }




}
