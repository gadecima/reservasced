<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoRecurso;
use App\Log;
use App\Role;
use Illuminate\Support\Facades\Validator;
use Auth;
Use Exception;
use Illuminate\Validation\Rule;

class TipoRecursoController extends Controller{
    
    public function tipoVisor(){ 
    	$tiposRecu = TipoRecurso::where('recu_accs','1')->orderBy('id','desc')->get(); //recu_accs => 1: recurso, 0: accesorio
        $tiposAccs = TipoRecurso::where('recu_accs','0')->orderBy('id','desc')->get();

        //return $tipos;
		return view('/admin/tipoVisor', compact('tiposRecu','tiposAccs'));
    }

    public function tipoNuevo(){ 
        return view('/admin/tipoNuevo');
    }

    public function validar(array $data) {
        return Validator::make($data, [
        	'tipo' => ['required', 'max:20', 'unique:tipo_recursos']
	    ]);
    }

    public function tipoAgregar( Request $request ){
        $this->validar($request->all())->validate(); 
        $data = [
            'tipo' => request('tipo'),
            'recu_accs' => request('recu_accs'),
        ];
        if(!isset($request->swtResOn)){
            $data['reservable'] = "0";
        } else{
            $data['reservable'] = "1";
        }

        if ( $result = TipoRecurso::create($data) ) {
            Log::logNuevo(Auth::user()->id,'Crear','Crear nuevo tipo de recurso');
            return redirect()->route('tipo.visor')->with('result', '1');
        }else {
            return redirect()->route('tipo.visor')->with('result', '0');
        }
    }

    public function tipoEliminar($id){
        $tipo = TipoRecurso::TipoSegunID($id);
        try {
            $result = TipoRecurso::destroy($id); 
            Log::logNuevo(Auth::user()->id,'Eliminar','eliminó:'.$tipo);
            return redirect()->route('tipo.visor')->with('result', '1');
        } catch (\Exception $e) {
            return redirect()->route('tipo.visor')->with('result', '0');
        }
    }

    public function tipoEditar($id){
        $tipo = TipoRecurso::find($id);

        return view('/admin/tipoEditar', compact('tipo'));
    }

    public function tipoActualizar($id, Request $request){
        $this->validarActualizar($id, array_filter($request->all()) )->validate();
        $data = [
            'tipo' => request('tipo'),
            'recu_accs' => request('recu_accs'),
        ];
        if(!isset($request->swtResOn)){
            $data['reservable'] = "0";
        } else{
            $data['reservable'] = "1";
        }

        if ( $result = TipoRecurso::find($id)->update($data) ) { 
            Log::logNuevo(Auth::user()->id,'Actualizar','Actualizar tipo de recurso:'.$id);
            return redirect()->route('tipo.visor')->with('result', '1');
        }else {
            return redirect()->route('tipo.visor')->with('result', '0');
        }
    }

    public function validarActualizar($id, array $data) {
        return Validator::make($data, [
        	'tipo' => ['required', 'max:20', Rule::unique('tipo_recursos','tipo')->ignore($id)]
	    ]);
    }


}
