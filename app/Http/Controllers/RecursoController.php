<?php

namespace App\Http\Controllers;

use App\Recurso; //importamos Modelo Recurso
use App\EstadoRecurso;
use App\RecursoReserva;
use App\RecursoPrestamo;
use App\TipoRecurso;
use Carbon\Carbon;	
use App\Log;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Auth;

class RecursoController extends Controller{

    public function recursoVisor(){
		$recursos = Recurso::orderBy('created_at','desc')->get();
    	foreach($recursos as $recursosItem){
    		$recursosItem->tipo = TipoRecurso::TipoSegunId($recursosItem->tipo);
    		$recursosItem->estado = EstadoRecurso::GetEstadoRecu($recursosItem->estado);
    	}
		if ( Auth::user()->rol_id == 0) {
			return view('/superAdmin/recVisorSuper', compact('recursos'));
		}else{
			return view('/admin/recursoVisor', compact('recursos'));
		}
    }

	public function recursoNuevo(){
	   	$estado = EstadoRecurso::get();
    	$tipo = TipoRecurso::orderBy('tipo','asc')->get();
    	return view('/admin/recursoNuevo', compact('estado', 'tipo'));
    }

    public function validarAgregar(array $data) {
        return Validator::make($data, [
	        'codigo' => ['required', 'string', 'min:6', 'max:6','unique:recursos'],
	        'serie' => ['required', 'string', 'max:50','unique:recursos'],
	        'marca' => ['required', 'string', 'max:20'],
	        'modelo' => ['required', 'string', 'max:30'],
	        'cant' => ['required', 'integer', 'max:999', 'min:1'],
	    ]);
    }

    public function recursoAgregar(Request $request){
    	$this->validarAgregar($request->all())->validate();
    	$result = Recurso::create([
    		'codigo' => request('codigo'),
			'serie' => request('serie'),
			'marca' => request('marca'),
			'modelo' => request('modelo'),
			'descripcion' => request('descripcion'),
			'observacion' => request('observacion'),
			'tipo' => request('tipo'),
			'estado' => request('estado'),
			'disponible' => request('disponible'),
			'cant' => request('cant'),
    	]);
    	Log::logNuevo(Auth::user()->id,'Crear','Agregar recurso');
    	return redirect()->route('recurso.visor')->with('result', $result ? '1' : '0' );
    }

    public function recursoNuevoInsumo(){
    	$estado = EstadoRecurso::get();
    	$tipo = TipoRecurso::orderBy('tipo','asc')->get();
    	return view('/admin/recursoNuevoInsumo', compact('estado', 'tipo'));
    }

    public function validarAgregarInsumo(array $data) {
    	//Se recomienda usar los mismos nombres de variables en request que las reglas a definir.
        return Validator::make($data, [ 
	        'codigo' => ['required', 'string', 'min:6','max:6','unique:recursos'],
	        'cant' => ['required', 'integer', 'max:999', 'min:1'],
	    ]);
    }

    public function recursoAgregarInsumo(Request $request){
    	$this->validarAgregarInsumo($request->all())->validate();
    	$result = Recurso::create([
    		'codigo' => request('codigo'),
			'serie' => request('serie') ?? 'N/E',
			'marca' => request('marca') ?? 'N/E',
			'modelo' => request('modelo') ?? 'N/E',
			'descripcion' => request('descripcion'),
			'observacion' => request('observacion'),
			'tipo' => request('tipo'),
			'estado' => request('estado'),
			'disponible' => request('disponible'),
			'cant' => request('cant'),
    	]);
    	Log::logNuevo(Auth::user()->id,'Crear','Agregar Insumo');
		return redirect()->route('recurso.visor')->with('result', $result ? '1' : '0' );
    }

    public function recursoEditar($id){
    	$recurso = Recurso::find($id);
     	$estado = EstadoRecurso::get();
    	$tipo = TipoRecurso::get();
		return view('/admin/recursoEditar', compact('recurso', 'estado','tipo'));
    }

    public function validarEditar($id, array $data) {
        return Validator::make($data, [
        	'codigo' => ['required', 'string', 'min:6','max:6',Rule::unique('recursos','codigo')->ignore($id)],
        	'serie' => ['string','max:20','exclude_if:serie,N/E',Rule::unique('recursos','serie')->ignore($id)],
	        'marca' => ['required', 'string', 'max:20'],
	        'modelo' => ['required', 'string', 'max:30'],
	        'cant' => ['required', 'integer', 'max:999', 'min:1'],
	    ]);
    }

    public function recursoActualizar($id, Request $request){
    	$this->validarEditar($id,$request->all())->validate(); //valida los valores introducidos
		$result = Recurso::find($id)->update($request->all());
    	Log::logNuevo(Auth::user()->id,'Editar','Editar Recurso: #'.$id);
        return redirect()->route('recurso.visor')->with('result', $result ? '1' : '0' );
    }

    public function recursoEliminar($id){
		try {
			if ($result = Recurso::destroy($id)) {
				Log::logNuevo(Auth::user()->id,'Eliminar','Eliminar Recurso: #'.$id);
				return redirect()->route('recurso.visor',)->with('result', $result ? '1' : '0' );
			}else {
				return redirect()->route('recurso.visor',)->with('result', $result ? '1' : '0' );	
			}
		}
		catch (\Exception $e) {
            return redirect()->route('recurso.visor')->with('result', '0');
        }
    }

    public function recursoVer($id){
    	$recurso = Recurso::RecursoSegunId($id);
		$recurso->tipo = TipoRecurso::TipoSegunId(($recurso->tipo));
    	$recurso->estado = EstadoRecurso::GetEstadoRecu($recurso->estado);
    	return view('/admin/recursoVer', compact('recurso'));
    }

    public function recuCambiaEstado($id,$idEstado){
    	$recurso = Recurso::find($id);
        $recurso->estado = $idEstado;
        $result = $recurso->save();
        Log::logNuevo(Auth::user()->id,'Edita','Cambia Estado Recurso: #'.$id);
        return redirect()->route('recurso.visor')->with('result',$result ? '1' : '0');
    }

	public function ultimoCodigoTipo($tipo){ //devuelve el ultimo codigo cargado de un determinado (ej: NBK101)
		return Recurso::ultimoDelTipo($tipo);
	}

	public function recursoHistorico($id){
		$historic = Recurso::recursoHistory($id);
		if (!$historic->isEmpty()) {
			foreach($historic as $historicItem){
				$historicItem->f_desde = Carbon::parse($historicItem->f_desde)->locale('es')->format('d-m-Y H:i:s');
				$historicItem->f_hasta = Carbon::parse($historicItem->f_hasta)->locale('es')->format('d-m-Y H:i:s');
			}
			$codigo = Recurso::RecursoSegunId($id)->codigo;
			return view('/admin/recursoHistorico', compact('historic', 'codigo'));
		}
		else{
			$result = 2;
			return redirect()->route('recurso.visor')->with('result',$result);
		}
		
	}
}
