<?php

namespace App\Http\Controllers;
use App\Recurso;
use App\Prestamo;
use App\RecursoPrestamo;
use App\TipoRecurso;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\User;
use App\Log;
use Auth;

class PrestamoController extends Controller{
    public function prestamoVisor(){
        Prestamo::marcarPrestAdeuda();
        $prestEnCurso = Prestamo::prestamosEnCurso();
        $prestAdeuda = Prestamo::prestamosDeuda();
        $prestamos = Prestamo::orderBy('created_at','desc')->get();
        foreach($prestamos as $prestamosItem){
            $prestamosItem->fecha_prestado = Carbon::parse($prestamosItem->fecha_prestado)->locale('es')->format('d-m-Y H:i:s');
            if($prestamosItem->fecha_devuelto == NULL){
                $prestamosItem->fecha_devuelto = '-';
            }else{
                $prestamosItem->fecha_devuelto = Carbon::parse($prestamosItem->fecha_devuelto)->locale('es')->format('d-m-Y H:i:s');
            }
        }
        return view('/admin/prestamoVisor', compact('prestamos','prestEnCurso','prestAdeuda'));
    }

    public function prestamoVer($id){
        $recursos = RecursoPrestamo::recursosByPrestamoId($id); //$recursos[matriz]
        $prestamo = Prestamo::prestamoById($id);
        foreach ($recursos as $key => $value) {
            $recu[$key] = Recurso::RecursoSegunId($value->recurso_id);
            $recu[$key]->tipo = TipoRecurso::TipoSegunId( ($recu[$key]->tipo) );
            $recu[$key]->cant = $value->cant_prest;
            $prestados = collect($recu); //convierto arreglo en collection.
        }
        return view('/admin/prestamoVer',compact('prestados'))
                        ->with('prestamo', $prestamo);
    }

    public function prestamoNuevo(){
        $request = new Request( array('dt_desde_fecha' => Carbon::now()->locale('es')->format('d-m-Y'),
        'dt_hasta_fecha' => Carbon::now()->locale('es')->format('d-m-Y'),
        'dt_desde_hora' => Carbon::now()->locale('es')->format('H:i'),
        'dt_hasta_hora' => Carbon::now()->locale('es')->format('H:i') ));
    
        $tipos_recu = TipoRecurso::where('recu_accs', '1')->pluck('id');
        $recursos = Recurso::RecursoDisponible($request, $tipos_recu,'1');
 
        $tipos_accs = TipoRecurso::where('recu_accs', '0')->pluck('id');
        $accesorios = Recurso::RecursoDisponible($request, $tipos_accs,'0');

        return view('/admin/prestamoNuevo', compact('recursos','accesorios'));
    }

    public function prestamoResumen(Request $request){
        //return $request->accs;
        $prestados = array();
        $i = 0;
        if (!empty($request->accs)) { //accs es un arreglo 
            foreach ($request->accs as $key => $value) {
                if (!is_null($value['cantSel'])){
                    $prestados[$i] = Recurso::RecursoSegunId($value['recuId']);
                    $prestados[$i]['tipo'] = TipoRecurso::TipoSegunId($prestados[$i]['tipo']);
                    $prestados[$i]['cant'] = (int)$value['cantSel'];
                    $i = $i + 1;
                }
            }
        }
        if (!empty($request->recus)) {
            foreach ($request->recus as $key => $value) {
                $prestados[$i] = Recurso::RecursoSegunId($value);
                $prestados[$i]['tipo'] = TipoRecurso::TipoSegunId($prestados[$i]['tipo']);
                $i = $i + 1;
            }
        }
        
        if (!empty($prestados)) {
            return view('/admin/prestamoResumen',compact('prestados'))
            ->with('observ', $request->observ)
            ->with('nombre', $request->nombre)
            ->with('dni', $request->dni)
            ->with('contacto', $request->contacto);
        }else{
            return back()->with('vacia','1');
        }
    }
    
    public function prestamoConfirmado(Request $request){
        Prestamo::create([
            'solicitante' => $request->nombre,
            'dni' => $request->dni,
            'contacto' => $request->contacto,
            'fecha_prestado' => Carbon::now(),
            'fecha_devuelto' => NULL,
            'estado' => 'Prestado',
            'observ' => $request->observ,
            'user_id' => Auth::user()->id,
        ]);
        $prestamoID = Prestamo::PrestamoUltima()->id;

        foreach ($request->prestados as $prestadosItem) {
            RecursoPrestamo::create([
                'prestamo_id' => $prestamoID, 
                'recurso_id' => $prestadosItem['recuId'],
            ]);
        }
        Log::logNuevo(Auth::user()->id,'Crear','Creado Prestamo: #'.$prestamoID);
        return view('/admin/prestamoRealizado', compact('prestamoID') );
    }
        
    public function prestamoEliminar($id){
        $prestamo = Prestamo::find($id)->delete();
        Log::logNuevo(Auth::user()->id,'Eliminar','Eliminar Prestamo: #'.$id);
        return redirect()->route('prestamo.visor')->with('eliminado','1');
    }    

    public function prestCambiaEstado($id,$opc){
        $prestamo = Prestamo::find($id);
        $prestamo->estado = $opc;
        if ($opc == 'Devuelto') {
            $prestamo->fecha_devuelto = Carbon::now()->locale('es');
        }else{
            $prestamo->fecha_devuelto = NULL;
        }
        $result = $prestamo->save();
        Log::logNuevo(Auth::user()->id,'Editar','Cambia Estado Prestamo: #'.$id);
        return redirect()->route('prestamo.visor')->with('result', $result ? '1' : '0');;
    }

    public static function getSolicitantes(Request $request){
        $search = $request->search;
        if($search == ''){
            $solicitante = Prestamo::orderby('solicitante','asc')->select('id','solicitante','dni', 'contacto')
                                ->limit(1)->get();
        }else{
            $solicitante = Prestamo::orderby('solicitante','asc')->select('id','solicitante','dni', 'contacto')
                                ->where('solicitante', 'like', '%' .$search . '%')->limit(1)->get();
        }
        $response = array();
        foreach($solicitante as $solicitantes){
            $response[] = array("value"=>$solicitantes->solicitante,
                                "dni"=>$solicitantes->dni,
                                "contacto"=>$solicitantes->contacto,
                                "id"=>$solicitantes->id
                            );
        }
        return response()->json($response);
    }

    // Comentario del Admin a un prestamo Express
    public function prestamoComent(Request $request){
        $data = ['comentario' => $request->comentario ];
        return $result = Prestamo::find($request->id)->update($data);
    }
    
}