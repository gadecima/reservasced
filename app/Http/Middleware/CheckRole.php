<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles){
        $roleIds = ['Super' => 0, 'Admin' => 1, 'Usuario' => 2];
        $allowedRoleIds = [];
        foreach ($roles as $role){
           if(isset($roleIds[$role])){
               $allowedRoleIds[] = $roleIds[$role];
            }
        }
        $allowedRoleIds = array_unique($allowedRoleIds); 

        if(Auth::check()) {
            if(in_array(Auth::user()->rol_id, $allowedRoleIds)) {
                return  $next($request);
            }
        }
            return abort(401, 'Acceso no autorizado.');
        

    }
}
