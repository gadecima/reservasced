<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RecursoPrestamo extends Model
{
    protected $fillable = [
        'prestamo_id',
        'recurso_id'
    ];

    public static function recursosByPrestamoId($id){
        return RecursoPrestamo::where('prestamo_id',$id)->get();
    }

    public static function cuentaxTipoPrestado(){
        return RecursoPrestamo::select('recursos.tipo',DB::raw('count(recursos.tipo) as tipos') )
        ->join('recursos','recurso_prestamos.recurso_id','=','recursos.id')
        ->groupBy('recursos.tipo')->take(5)->get();
    }

}
