<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Log extends Model
{
    protected $fillable = [
        'evento_usr',
        'evento',
        'evento_detalle'
    ];

    public static function logNuevo($user, $event, $detail = NULL){
        Log::create([
            'evento_usr' => $user,
            'evento' => $event,
            'evento_detalle' =>$detail,
        ]);
    }

    public static function getCreatedAtAttribute($date){
        return Carbon::parse($date)->locale('es')->format('d-m-Y H:i:s');
    }
}
