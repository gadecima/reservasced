<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoReserva extends Model
{
    protected $fillable = [
        'estado_reserv',
    ];
    //Devuelve el nombre del estado de una reserva segun id recibido.
    public static function GetEstado($idEstado){
        return EstadoReserva::where('id', $idEstado)->value('estado_reserv');
        //con value obtengo el valor de la celda, no un arreglo o una matriz.
    }
}
