<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class mailabReservaPrepara extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Reserva lista para retirar - Sistema de Reservas CED';
    public $mensaje; 


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensaje, $outputPDF){
        $this->mensaje = $mensaje;
        $this->outputPDF = $outputPDF;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.mailReservaPrepara')
            ->attachData($this->outputPDF, 'reserva'.$this->mensaje->id.'.pdf', ['mime' => 'application/pdf']);;
    }
}
