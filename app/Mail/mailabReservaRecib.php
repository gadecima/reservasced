<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class mailabReservaRecib extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = 'Reserva Recibida - Sistema de Reservas CED';
    public $mensaje; 
    public $outputPDF;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($mensaje,$reservados){
        $this->mensaje = $mensaje;
        $this->reservados = $reservados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build(){
        return $this->view('emails.mailReservaRecib')->with('reservados',$this->reservados);
    }

    /* Envia con PDF adjunto.*****
    public function __construct($mensaje, $outputPDF){
        $this->mensaje = $mensaje;
        $this->outputPDF = $outputPDF;
    }

    public function build(){
        return $this->view('emails.mailReservaRecib')
                        ->attachData($this->outputPDF, 'reserva'.$this->mensaje->id.'.pdf', ['mime' => 'application/pdf']);
    }
    */


}
