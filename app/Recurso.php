<?php

//modelo eloquent para la tabla recurso
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
use App\TipoRecurso;
use App\RecursoPrestamo;
use App\RecursoReserva;
use App\Users;
use App\EstadoReserva;

class Recurso extends Model{
	protected $fillable = [
        'codigo',
        'serie',
        'marca',
        'modelo',
        'descripcion',
        'tipo',
        'categoria',
        'observacion',
        'cantidad',
        'estado',
        'disponible',
        'cant'
    ];

	//retorna los datos de un Recurso determinado, segun su ID.
	public static function RecursoSegunId($idRecurso){
		return Recurso::where('id',$idRecurso)->first();
	}

	//retorna los recursos disponibles en un rango de fecha, según un tipo especificado
	public static function RecursoDisponible($request, $tipos, $recuOrAccs){ // $tipo => array() - recuOrAccs => 1: recurso, 0: accesorio
		$desde = Carbon::parse($request->dt_desde_fecha)->locale('es')->format('Y-m-d');
		$hasta = Carbon::parse($request->dt_hasta_fecha)->locale('es')->format('Y-m-d');
		$h_desde = Carbon::parse($request->dt_desde_hora)->locale('es')->format('H:i:s');
		$h_hasta = Carbon::parse($request->dt_hasta_hora)->locale('es')->format('H:i:s');
		$prestado_desde = Carbon::parse($request->dt_desde_fecha)->locale('es')->format('Y-m-d 00:00:00');
		$prestado_hasta = Carbon::parse($request->dt_hasta_fecha)->locale('es')->format('Y-m-d 23:59:00');

		if ($recuOrAccs){ 
			$recursos = Recurso::select('recursos.id','recursos.codigo','recursos.marca','recursos.serie',
										'recursos.descripcion','recursos.cant','tipo_recursos.tipo')
				->join('tipo_recursos','tipo_recursos.id','=','recursos.tipo')
				->whereIn('recursos.tipo',$tipos)
				->where('recursos.disponible','1')
				->where('recursos.estado','1')
				->where('tipo_recursos.reservable','1')
				->where('tipo_recursos.recu_accs',$recuOrAccs)
				//verifica si no están reservados
					->whereNotIn('recursos.id', function ($query) use ($desde, $hasta, $h_desde, $h_hasta){
						$query->select('recurso_id')
							->from('recurso_reservas')
							->join('reservas','reservas.id','=','recurso_reservas.reserva_id')
						->where(function($q) {
							$q->where('reservas.est_reserv_id','1')
							->OrWhere('reservas.est_reserv_id','2')
							->OrWhere('reservas.est_reserv_id','3')
							->OrWhere('reservas.est_reserv_id','4');
						})
						->where(function($k) use ($desde, $hasta, $h_desde, $h_hasta) {
							$k->whereBetween('fecha_hasta', [$desde , $hasta])  
							->orWhereBetween('fecha_desde', [$desde , $hasta])
							->orWhere('fecha_desde', '<=', $desde)->Where('fecha_hasta', '>=', $desde)
							->orWhere('fecha_desde', '>=', $hasta)->Where('fecha_hasta', '>=', $hasta)
							->where('hora_desde', '>=', $h_desde)->Where('hora_hasta', '<=' , $h_hasta);
						});
					})
				//verificando si no estan prestados:
					->whereNotIn('recursos.id', function ($queryP) use ($prestado_desde, $prestado_hasta) {
						$queryP->select('recurso_id')
							->from('recurso_prestamos')
							->join('prestamos','prestamos.id','=','recurso_prestamos.prestamo_id')
							->whereBetween('fecha_prestado', [$prestado_desde , $prestado_hasta]);
				}) 
				->get();
				return $recursos;
		}else {
			$sql = 
				'SELECT `a`.`id`,`a`.`codigo`, `a`.`tipo`, `a`.`desc`, `a`.`observ`, `a`.`cant`,IFNULL(`b`.`cantRes`,0) AS `CantRes`,
					IFNULL((`a`.`cant` - `CantRes`),`a`.`cant`) AS `dispon`
				FROM(
					select `recursos`.`id` AS `id`,
						`recursos`.`cant`, 
						`recursos`.`codigo` AS `codigo`,
						`recursos`.`observacion` AS `observ`,
						`recursos`.`descripcion` AS `desc`,
						`tipo_recursos`.`tipo` AS `tipo`
					from `recursos` 
					INNER JOIN `tipo_recursos` ON `tipo_recursos`.`id` = `recursos`.`tipo`
					WHERE (`tipo_recursos`.`recu_accs` = 0)
				) as a
				LEFT JOIN (
					select `recurso_reservas`.`recurso_id` AS `id`, 
							`recurso_reservas`.`cant_res` AS `cantRes`
					from `recurso_reservas` 
					inner join `reservas` ON `reservas`.`id` = `recurso_reservas`.`reserva_id`
					INNER JOIN `recursos` ON `recurso_reservas`.`recurso_id` = `recursos`.`id`
					inner join `tipo_recursos` on `tipo_recursos`.`id` = `recursos`.`tipo`
					where (`tipo_recursos`.`recu_accs` = 0) AND
							(`reservas`.`est_reserv_id` = 1 OR 
							`reservas`.`est_reserv_id` = 2 or 
							`reservas`.`est_reserv_id` = 3 or
							`reservas`.`est_reserv_id` = 4)
							and (`fecha_hasta` between '.$desde.' and '.$hasta.'
								or `fecha_desde` between '.$desde.' and '.$hasta.' 
								or `fecha_desde` <= '.$desde.' and `fecha_hasta` >= '.$desde.' 
								or `fecha_desde` >= '.$hasta.' and `fecha_hasta` >= '.$hasta.' 
								and `hora_desde` >= "'.$h_desde.'" and `hora_hasta` <= "'.$h_hasta.'")
					UNION ALL
					select `recurso_prestamos`.`recurso_id` AS `id`, 
							`recurso_prestamos`.`cant_prest` AS `cantRes`
					from `recurso_prestamos` 
					inner join `prestamos` ON `prestamos`.`id` = `recurso_prestamos`.`prestamo_id`
					INNER JOIN `recursos` ON `recurso_prestamos`.`recurso_id` = `recursos`.`id`
					where `fecha_prestado` between "'.$prestado_desde.'" and "'.$prestado_hasta.'"
				) as b
				ON `a`.`id` = `b`.`id`';
			return $accesorios = DB::select($sql);
		}
	}

	//retorna los recursos disponibles en un rango de fecha, según un tipo especificado
	public static function RecursOtroDispon($request){
		$desde = Carbon::parse($request->dt_desde_fecha)->locale('es')->format('Y-m-d');
		$hasta = Carbon::parse($request->dt_desde_fecha)->locale('es')->format('Y-m-d');
		$h_desde = Carbon::parse($request->dt_desde_hora)->locale('es')->format('H:i:s');
		$h_hasta = Carbon::parse($request->dt_hasta_hora)->locale('es')->format('H:i:s');

		return Recurso::select('recursos.id', 'recursos.codigo', 'recursos.marca', 'recursos.descripcion', 'tipo_recursos.tipo')
			->join('tipo_recursos','tipo_recursos.id','=','recursos.tipo')
			->where('recursos.tipo', '>',22) //desde 22 se considera un tipo de recurs no predefinido por el cliente, estos aaparecerán como OTROS en el sistema.
			->where('recursos.disponible','1')
			->where('tipo_recursos.reservable','1')
			->whereNotIn('recursos.id', function ($query) use ($desde, $hasta, $h_desde, $h_hasta){
				$query->select('recurso_id')
					->from('recurso_reservas')
					->join('reservas','reservas.id','=','recurso_reservas.reserva_id')
					->whereBetween('fecha_hasta', [$desde , $hasta])  
					->orWhereBetween('fecha_desde', [$desde , $hasta])
					->orWhere('fecha_desde', '=<', $desde)->Where('fecha_hasta', '=>', $desde)
					->orWhere('fecha_desde', '=>', $hasta)->Where('fecha_hasta', '=>', $desde)
					->where('hora_desde', '=>', $h_desde)->Where('hora_hasta', '=<' , $h_hasta);
			}) 
			->get();
	}
	//retorno IDs de recursos según tipo y cantidad a reservar, en un rango de fechas determinado.
	public static function RecuReservByCant($request, $tipo, $cant){ //$tipo => array()
		//return TipoRecurso::esRecuoAccs($tipo);
		if ($cant > 0) {
			return Recurso::RecursoDisponible($request, $tipo,TipoRecurso::esRecuoAccs($tipo) )->slice(0,$cant);
		}else{
			return Recurso::RecursoDisponible($request, $tipo, TipoRecurso::esRecuoAccs($tipo) )->slice(0,0);
		}
	}

	public static function reportRecursos($tipo, $estado, $dispon){
		$query = DB::table('Recursos');
		if ($tipo != -1) {
			$query = $query->where('tipo', $tipo);
		}
		if ($estado != -1) {
			$query = $query->where('estado', $estado);
		}
		if ($dispon != -1) {
			$query = $query->where('disponible',$dispon);
		}
		return $query->get();
	}

	public static function ultimoDelTipo($tipo){
		return Recurso::where('tipo',$tipo)
						->orderBy('tipo', 'DESC')
						->pluck('codigo')
						->last();
	}

	public static function recursoHistory($id){ //UNION entre dos consultas ELOQUENT
        $histoRes = RecursoReserva::select('recurso_id','reserva_id AS evento_id', DB::raw('"reserva" AS Evento'),'recursos.codigo',
                                    DB::raw('concat(users.apellido,", ",users.nombre) AS solicitante'),
                                    'reservas.fecha_desde AS f_desde', 'reservas.fecha_hasta AS f_hasta',
                                    'estado_reservas.estado_reserv AS estado')
                                ->join('reservas', 'reservas.id', '=', 'recurso_reservas.reserva_id')
                                ->join('recursos', 'recursos.id', '=', 'recurso_reservas.recurso_id')
                                ->join('users', 'users.id', '=', 'reservas.user_id')
                                ->join('estado_reservas', 'estado_reservas.id', '=', 'reservas.est_reserv_id')
                                ->where('recurso_reservas.recurso_id' , $id);
								
		$historic = RecursoPrestamo::select('recurso_id','prestamo_id AS evento_id',DB::raw('"prestamo" AS Evento'), 'recursos.codigo', 
                                    'prestamos.solicitante','prestamos.fecha_prestado AS f_desde', 'prestamos.fecha_devuelto AS f_hasta',
                                    'prestamos.estado')
                                ->join('prestamos', 'prestamos.id', '=', 'recurso_prestamos.prestamo_id')
                                ->join('recursos', 'recursos.id', '=', 'recurso_prestamos.recurso_id')
                                ->where('recurso_prestamos.recurso_id' , $id)
								->union($histoRes)
								->orderby('f_desde', 'DESC')
								->get();
		return $historic;
    }

}
