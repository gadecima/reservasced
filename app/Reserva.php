<?php

namespace App;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $fillable = [
        'user_id',
        'fecha_desde',
        'hora_desde',
        'fecha_hasta',
        'hora_hasta',
        'est_reserv_id',
        'observ',
        'comentario'
    ];

    public static function getCreatedAtAttribute($date){
        return Carbon::parse($date)->locale('es')->format('d-m-Y H:i:s');
    }

    public static function ReservaById($id){
        return Reserva::where('id', $id)->first();
        //uso ->first() para obtener un arreglo puro y no una collection.
    }

    public static function ReservaByUser($id){
        return Reserva::where('user_id', $id)->orderBy('id','desc')->get();
    }

    //devuelve el id de la última reserva creada
    public static function ReservaUltimaXuser($id){
        return Reserva::where('user_id',$id)->latest()->first('id');
    }

    public static function reportReservas($xEstadoRes,$xUser,$xDesde,$xHasta){
        $xDesde = Carbon::parse($xDesde)->locale('es')->format('Y-m-d 00:00:00');
        $xHasta = Carbon::parse($xHasta)->locale('es')->format('Y-m-d 00:00:00');

        $query = DB::table('Reservas');
        if ($xEstadoRes != -1) {
            $query = $query->where('est_reserv_id', $xEstadoRes);
        }
        if ($xUser != -1) {
            $query = $query->where('user_id', $xUser);
        }
        return $query->whereBetween('created_at', [$xDesde , $xHasta])->get();
    }

    public static function cuentaXmes(){
        DB::statement(DB::raw('SET lc_time_names = "es_Es"'));
        $query = 'select MonthName(created_at) as mes, count(*) as cant
                    from reservas
                    where year(created_at) = year(curdate())
                    group by MonthName(created_at)';
        return DB::select($query);
    }

    public static function ReservasAdeuda(){
        return Reserva::where('fecha_hasta','<', NOW())
                        ->where('est_reserv_id','8')
                        ->where('user_id',Auth::user()->id)
                        ->count();
    }

    public static function ResAdeudaAdmin(){
        return Reserva::where('fecha_hasta','<', NOW())
                        ->where('est_reserv_id','8')
                        ->count();
    }

    public static function marcarResVencidas(){
        return Reserva::where('fecha_hasta','<', NOW())
                        ->where(function($q) {
                            $q->where('est_reserv_id','1')
                            ->OrWhere('est_reserv_id','3');
                        })
                        ->update(['est_reserv_id' => 4]);
                        //->get();
    }

    public static function marcarResAdeuda(){
        return Reserva::where('fecha_hasta','<', NOW())
                        ->where(function($q) {
                            $q->OrWhere('est_reserv_id','2');
                        })
                        ->update(['est_reserv_id' => 8]);
                        //->get();
    }

    public static function ReservasPreparar(){
        return Reserva::where('est_reserv_id','1')
                        ->count();
    }

    public static function ReservasEnCurso(){
        return Reserva::where('est_reserv_id','2')
                        ->count();
    }

    public static function ReservasCanceladas(){
        return Reserva::where('est_reserv_id','7')
                        ->count();
    }

    public static function ReservasListas(){
        return Reserva::where('est_reserv_id','3')
                        ->where('user_id',Auth::user()->id)
                        ->count();
    }

    public static function ReservasConcluidas(){
        return Reserva::where('est_reserv_id','6')
                        ->where('user_id',Auth::user()->id)
                        ->count();
    }

}
