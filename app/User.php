<?php

namespace App;
use Auth;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellido',
        'dni',
        'telefono',
        'email',
        'cargo',
        'dependencia',
        'ofi_area',
        'rol_id',
        'password',
    ];
    //esta funcion formatea a created_at para usarlo formateado en todas las instancias de la clase.
    public static function getCreatedAtAttribute($date){
        return Carbon::parse($date)->locale('es')->format('d-m-Y H:i:s');
    }

    //devuelve el registro completo (->first) de un Usuario segun Id.
    public static function GetUsuario($UserId){
        return User::where('id', $UserId)->first();
    }

    public static function mailsAdmins(){
        return User::where('rol_id','1')->pluck('email')->toArray();
    }

    public static function mailUser($id_user){
        return User::where('id',$id_user)->value('email');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

     /**
     * Send the password reset notification.
     *
     * Envia eMail con link para reestablecer la contraseña
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
