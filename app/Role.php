<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'id',
        'rol'
    ];

    //Devuelve el nombre del ROL de un usuario segun id recibido.
    public static function getRol($idRol){
        return Role::where('id', $idRol)->value('rol');
        //con value obtengo el valor de la celda, no un arreglo o una matriz.
    }

}
