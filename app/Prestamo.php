<?php

namespace App;
use DB;
use Auth;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    protected $fillable = [
        'solicitante',
        'dni',
        'contacto',
        'fecha_prestado',
        'fecha_devuelto',
        'estado',
        'comentario',
        'user_id'
    ];

    public static function prestamoById($id){
        return Prestamo::where('id', $id)->first();
        //uso ->first() para obtener un arreglo puro y no una collection.
    }

    public static function PrestamoUltima(){
        return Prestamo::latest()->first('id');
    }

    public static function cuentaXmes(){
        DB::statement(DB::raw('SET lc_time_names = "es_Es"'));
        $query = 'select MonthName(created_at) as mes, count(*) as cant
                    from prestamos
                    where year(created_at) = year(curdate())
                    group by MonthName(created_at)';
        return DB::select($query);
    }

    public static function marcarPrestAdeuda(){
        $query = 'UPDATE prestamos SET estado="Adeuda"
                    WHERE ( (TIMESTAMPDIFF(HOUR, `fecha_prestado`, NOW()) > 5) AND  (`estado` = "Prestado") )';
        return DB::select($query);
    }

    public static function prestamosEnCurso(){
        return Prestamo::where('estado','Prestado')->count();
    }

    public static function prestamosDeuda(){
        return Prestamo::where('estado','Adeuda')->count();
    }

    public static function reportPrestamos($xEstadoPrest,$xSolicitante,$xDesde,$xHasta){
        $xDesde = Carbon::parse($xDesde)->locale('es')->format('Y-m-d 00:00:00');
        $xHasta = Carbon::parse($xHasta)->locale('es')->format('Y-m-d 00:00:00');

        $query = DB::table('Prestamos');
        if ($xEstadoPrest != -1) {
            $query = $query->where('estado', $xEstadoPrest);
        }
        if ($xSolicitante != -1) {
            $query = $query->where('solicitante', $xSolicitante);
        }
        return $query->whereBetween('fecha_prestado', [$xDesde , $xHasta])->get();
    }

}
