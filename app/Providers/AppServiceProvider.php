<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Config;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot(){
        //variables globales de la clase Config para el sistema
        view()->composer('*', function ($view) {
            $config = Config::find('1');
            
            $iniHour = idate('H', strtotime($config->desde_hora) );
            $finHour = idate('H', strtotime($config->hasta_hora) );
            $horasAten = json_encode(range($iniHour, $finHour-1));

            $view->with('config', $config)
                ->with('horasAten', $horasAten );
        });

        //VALIDADOR personalizado para comparar 2 horas.
        Validator::extend('horaMayorQue', function($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value > $min_value;
          });   
        Validator::replacer('horaMayorQue', function($message, $attribute, $rule, $parameters) {
            return sprintf('La "hora hasta" debe ser mayor que "hora desde"', $attribute, $parameters[0]);
        });
        
    }
}
