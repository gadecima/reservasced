<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoRecurso extends Model
{
    protected $fillable = [
        'estado',
    ];
    //Devuelve el nombre del estado de un recurso segun id recibido.
    public static function GetEstadoRecu($idEstado){
        return EstadoRecurso::where('id', $idEstado)->value('estado');
        //con value obtengo el valor de la celda, no un arreglo o una matriz.
    }


}
