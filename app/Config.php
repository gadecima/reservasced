<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
            'reservas_on',
            'user_create_on',
            'dias_antes',
            'dias_uso',
            'desde_hora',
            'hasta_hora',
    ];

    public static function getDiasAntes(){
        return Config::get()->value('dias_antes');
    }

    public static function getHorario(){
        return Config::select('desde_hora','hasta_hora')->get();
    }

}
