<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RecursoReserva extends Model
{
    protected $fillable = [
        'reserva_id',
        'recurso_id',
        'cant_res'
    ];

    public static function recursosByReservaId($id){
        return RecursoReserva::where('reserva_id',$id)->get();
    }

    public static function cuentaxTipoReserv(){
            return RecursoReserva::select('recursos.tipo',DB::raw('count(recursos.tipo) as tipos') )
            ->join('recursos','recurso_reservas.recurso_id','=','recursos.id')
            ->groupBy('recursos.tipo')->take(5)->get();
    }

    public static function cuentaXtipoEnReserva($reservaID){
		return RecursoReserva::select('recursos.tipo','tipo_recursos.tipo', DB::raw('COUNT(`tipo_recursos`.`tipo`) AS cant'))
                                ->join('recursos', 'recursos.id', '=', 'recurso_reservas.recurso_id')
								->join('tipo_recursos', 'tipo_recursos.id', '=', 'recursos.tipo')
                                ->where('reserva_id' , $reservaID)
                                ->groupBy('recursos.tipo')
                                ->get();
		
	}

}
