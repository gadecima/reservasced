<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/auth/login'); //pagina principal al ingresar al sistema.
});

Auth::routes(); //de estas rutas solo usamos lo referido a Login, por seguridad.
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['checkRole:Usuario'])->group(function(){
    Route::get('/reservaNueva', 'ReservaController@reservaNueva')->middleware('checkRole:Usuario')->name('reserva.nueva');
    Route::post('/reservaSelecRecursos', 'ReservaController@reservaSelecRecursos')->name('reserva.selecRecursos');
    Route::post('/reservaResumen', 'ReservaController@reservaResumen')->name('reserva.resumen');
    Route::post('/reservaConfirmada', 'ReservaController@reservaConfirmada')->name('reserva.confirmada');
    Route::get('/reservaCancelar{id}','ReservaController@reservaCancelar')->name('reserva.cancelar');
    Route::get('/reservaVer{id}', 'ReservaController@reservaVer')->name('reserva.ver');

    Route::get('/reservasUsuario', 'ReservaController@reservasUsuario')->name('reservas.usuario');
    
    Route::get('/ExportUsrReservas', 'PdfController@ExportUsrReservas')->name('pdf.reservasUsr');
});

Route::middleware(['checkRole:Admin'])->group(function(){
    Route::get('/usuarioVisor','UserController@usuarioVisor')->name('usuario.visor');

    Route::get('/reservasAdmin', 'ReservaController@reservasAdmin')->name('reservas.admin');
    Route::post('/reservaComent', 'ReservaController@reservaComent')->name('reserva.coment');
    Route::get('/reservaRecuCambiar/{idReserva}/{idRecu}','ReservaController@reservaRecuCambiar')->name('reserva.recuCambiar');
    Route::patch('/reservaRecuCambiado','ReservaController@reservaRecuCambiado')->name('reserva.recuCambiado');

    Route::get('/prestamoVisor','PrestamoController@prestamoVisor')->name('prestamo.visor');
    Route::get('/prestamoNuevo','PrestamoController@prestamoNuevo')->name('prestamo.nuevo');
    Route::post('/prestamoResumen', 'PrestamoController@prestamoResumen')->name('prestamo.resumen');
    Route::post('/prestamoConfirmado', 'PrestamoController@prestamoConfirmado')->name('prestamo.confirmado');
    Route::get('/prestamoVer{id}', 'PrestamoController@prestamoVer')->name('prestamo.ver');
    //Route::get('/prestamoEliminar{id}', 'PrestamoController@prestamoEliminar')->name('prestamo.eliminar');
    Route::get('/PrestCambiaEstado{id}/{opc}', 'PrestamoController@prestCambiaEstado')->name('prestamo.cambiaEstado');
    Route::post('getSolicitantes','PrestamoController@getSolicitantes')->name('getSolicitantes');
    Route::post('/prestamoComent', 'PrestamoController@prestamoComent')->name('prestamo.coment');
    
    Route::get('/recursoNuevo','RecursoController@recursoNuevo')->name('recurso.nuevo');
    Route::post('/recursoAgregar','RecursoController@recursoAgregar')->name('recurso.agregar');
    Route::get('/recursoNuevoInsumo','RecursoController@recursoNuevoInsumo')->name('recurso.nuevoInsumo');
    Route::post('/recursoAgregarInsumo','RecursoController@recursoAgregarInsumo')->name('recurso.agregarInsumo');
    Route::get('/recursoEditar{id}','RecursoController@recursoEditar')->name('recurso.editar');
    Route::patch('/recursoActualizar{id}','RecursoController@recursoActualizar')->name('recurso.actualizar');
    
    Route::get('/ultimoDelTipo/{tipo}','RecursoController@ultimoCodigoTipo')->name('recurso.ultimoCodigo');

    Route::get('/tipoVisor','TipoRecursoController@tipoVisor')->name('tipo.visor');
    Route::get('/tipoNuevo','TipoRecursoController@tipoNuevo')->name('tipo.nuevo');
    Route::post('/tipoAgregar','TipoRecursoController@tipoAgregar')->name('tipo.agregar');
    Route::delete('/tipoEliminar{id}','TipoRecursoController@tipoEliminar')->name('tipo.eliminar');
    Route::get('/tipoEditar{id}','TipoRecursoController@tipoEditar')->name('tipo.editar');
    Route::patch('/tipoActualizar{id}','TipoRecursoController@tipoActualizar')->name('tipo.actualizar');

    Route::get('/ActaPrestamo{id}', 'PdfController@actaPrestamo')->name('pdf.actaPrestamo');
    Route::get('/ExportPrestamos', 'PdfController@exportPrestamos')->name('pdf.exportPrestamos');

    //prueba de mail- Route::get('/pruebaMail/{id}', 'ReservaController@PruebaMailReserva')->name('mail.prueba');
});

Route::middleware(['checkRole:Super'])->group(function(){
    Route::get('/usrNuevoSuper','UserController@usrNuevoSuper')->name('usuario.nuevoSuper');
    
    Route::get('/resVisorSuper', 'ReservaController@resVisorSuper')->name('reserva.visorSuper');
    Route::delete('/reservaEliminar{id}','ReservaController@reservaEliminar')->name('reserva.eliminar');

    Route::delete('/{id}','RecursoController@recursoEliminar')->name('recurso.eliminar');

    Route::get('/configVisor', 'ConfigController@configVisor')->name('config.visor');
    Route::post('/configActualizar', 'ConfigController@configActualizar')->name('config.actualizar');
});

Route::middleware(['checkRole:Super,Admin'])->group(function(){
    Route::delete('/usuarioEliminar{id}','UserController@usuarioEliminar')->name('usuario.eliminar');
    Route::get('/usuarioNuevo','UserController@usuarioNuevo')->name('usuario.nuevo');
    Route::post('/usuarioAgregar','UserController@usuarioAgregar')->name('usuario.agregar');
    Route::get('/usuarioEditar{id}','UserController@usuarioEditar')->name('usuario.editar');
    Route::patch('/usuarioActualizar{id}','UserController@usuarioActualizar')->name('usuario.actualizar');
    
    Route::get('/ExportUsuarios', 'PdfController@exportUsuarios')->name('pdf.exportUsers');

    Route::get('/reserva{id}', 'ReservaController@reservaVerAdm')->name('reserva.verAdm');
    Route::get('/cambiaEstado{id}/{opc}', 'ReservaController@cambiaEstado')->name('reserva.cambiaEstado');
    Route::get('/ExportAdmReservas', 'PdfController@ExportAdmReservas')->name('pdf.reservasAdm');

    Route::get('/recursoVisor{op?}','RecursoController@recursoVisor')->name('recurso.visor');
    Route::get('/recursoVer{id}','RecursoController@recursoVer')->name('recurso.ver');
    Route::get('/recursoCambiaEstado{id}/{opc}', 'RecursoController@recuCambiaEstado');
    Route::get('/ExportRecursos', 'PdfController@exportRecursos')->name('pdf.exportRecursos');
    Route::get('/DetalleRecurso{id}', 'PdfController@detalleRecurso')->name('pdf.detalleRecurso');
    Route::get('/recursoHistorico{id}','RecursoController@recursoHistorico')->name('recurso.historico');

    Route::get('/reportes', 'ReporteController@reportes')->name('reportes');
    Route::get('/reporteRecursos/{xTipo}/{xEstado}/{xDispon}', 'PdfController@reporteRecursos')->name('pdf.reporteRecu');
    Route::get('/reporteReservas/{xEstadoRes}/{xUser}/{xDesde}/{xHasta}', 'PdfController@reporteReservas')->name('pdf.reporteRes');
    Route::get('/reportePrestamos/{xEstadoPrest}/{xSolicitante}/{xDesde}/{xHasta}', 'PdfController@reportePrestamos')->name('pdf.reportePrest');

    Route::get('/logVisor', 'LogController@logVisor')->name('log.visor');
    Route::get('/ExportLog', 'PdfController@exportLog')->name('pdf.exportLog');

    Route::get('/ExportRecuHistory{recu}', 'PdfController@recuHistory')->name('pdf.recuHistory');
});

Route::middleware(['checkRole:Super,Admin,Usuario'])->group(function(){
    Route::get('/ActaReserva{id}', 'PdfController@actaReserva')->name('pdf.actaReserva');

    Route::patch('/usuarioPerfilActualiza','UserController@usrPerfilActualizar')->name('usuario.perfil.actualizar');
    Route::get('/usuarioPassword','UserController@usuarioPassword')->name('usuario.password');
    Route::patch('/usuarioPassActualiza','UserController@usrPassActualizar')->name('usuario.pass.actualizar');
    Route::get('/usuarioPerfil','UserController@usuarioPerfil')->name('usuario.perfil');
});
