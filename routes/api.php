<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/statsReservas', 'ReporteController@statsReservas');
Route::get('/statsPrestamos', 'ReporteController@statsPrestamos');
Route::get('/statsResAnual', 'ReporteController@statsResAnual');
//Route::get('/statsPrestAnual', 'ReporteController@statsPrestAnual');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
