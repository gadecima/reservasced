﻿# Sistema de Reserva de Recursos!

Gestor de Recursos tecnológicos cuya función principal es gestionar el prestamo de estos recursos a distintas áreas del Ministerio de Educación de la Prov. de Tucumán, facilitando así a los responsables de ellos, el control y la logistica de este procedimiento.
Particularmente este sistema está desarrollado para una dependencia del Ministerio de Educación de la Provincia de Tucumán, la Coordinación de Educación Digital, con su equipos de responsables de velar por el cuidado y organización de los recursos tecnológicos de su patrimonio.

## *Funciones principales.
- Gestionar el prestamo (**Reservas**) de los recursos tecnológicos de la dependencia ministerial. 
- Inventario de recursos tecnológicos. (Estado de los recursos, Altas, bajas y modificaciones) 
- Reportes de estados sobre Inventario, reservas y prestamos.

### *Funciones secundarias.
- Gestión de usuarios (Administradores del sistema y Gestores de reservas)
- Envío de notificaciones por email.
- Reporte de reservas para Gestores 
-Los "Gestores de reservas" son los usuarios finales del sistema, son quienes realizar la reserva de los recursos.

### Sobre el desarrollo del sistema.
Es un sistema web, desarrollado en lenguaje PHP con la ayuda del Framework Laravel 7.x como backend y Bootstrap 4 para el FrontEnd.
Como motor de Base de datos,  MySQL.

A continuación se detalla todo lo necesario para poner en marcha el entorno de desarrollo y montar el sistema local.

## Preparación del entorno de desarrollo
### Framework Laragon.

Instalar el conjunto de herramientas Laragon:
https://github.com/leokhoa/laragon/releases/download/5.0.0/laragon-wamp.exe

Para sistemas 32bits:
https://github.com/leokhoa/laragon/releases/download/5.0.0/laragon-wamp.x86.exe
Descargar, ejecutar e iniciar.

### Editor de código fuente
Recomendamos SublimeText:
https://www.sublimetext.com/download

 ### Preparación del proyecto 
Desde la terminal que nos provee Laragon, ejecutar uno a uno los siguientes comandos:
 
    composer global require laravel/installer
    composer create-project --prefer-dist laravel/laravel:^7.0 reservasced

Ubicarse en la carpeta del reciente proyecto creado:
...laragon/www/reservasced/    

Ejecutar los siguientes comandos, uno despues de otro:
**Requeridos para el FrontEnd:**

    composer require laravel/ui:^2.4
    php artisan ui bootstrap --auth
    composer require "almasaeed2010/adminlte=~2.4"
    composer require twbs/bootstrap:5.1.0
    npm install --save @fortawesome/fontawesome-free
    composer require barryvdh/laravel-debugbar --dev  
 
 **Requeridos para Jquery:**
 
    npm install jquery
    npm i @popperjs/core   
    npm install && npm run dev
 
**Plugins requeridos:**

    composer require barryvdh/laravel-dompdf
    php artisan vendor:publish --provider="Barryvdh\DomPDF\ServiceProvider"    

En el archivo ..\config\app.php , agregar las siguientes lines según corresponda:
'providers' => [ 
  Barryvdh\DomPDF\ServiceProvider::class,
],


'aliases' => [
 'PDF' => Barryvdh\DomPDF\Facade::class,
]

    composer require nesbot/carbon
    composer update

**ChartJS (para gráficos estadisticos)**
npm install chart.js


### Obtener los archivos del proyecto desde el repositorio remoto.
Ubicarnos en la carpeta "...laragon/www/" ejecutar los siguientes comandos:
    
    git clone https://gadecima@bitbucket.org/gadecima/reservasced.git temp
    cp -r temp/* reservasced/
    rm -rf temp


### Preparación de la Base de Datos
Para facilitar la gestión de la base de datos, recomendamos usar **PhpMyAdmin**:
Descargar: https://files.phpmyadmin.net/phpMyAdmin/5.1.1/phpMyAdmin-5.1.1-all-languages.zip
Descomprimir todo el contenido del archivo descargado en `{LARAGON_DIR}\etc\apps\phpMyAdmin`
Luego podrán acceder a él mediante [http://localhost/phpmyadmin](http://localhost/phpmyadmin)

Creamos la Base de Datos para luego aplicar la migración de Tablas y Registros de prueba.
Ejecutando uno despues de otro, los comandos:
    
    mysql -u root
    create database reservasced;
    exit

En el archivo .env, ubicado en la raiz de nuestro proyecto editar las siguientes líneas:

APP_NAME=reservasced
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=localhost
DB_DATABASE=reservasced
DB_USERNAME=root
DB_PASSWORD=

Guardamos los cambios y continuamos...
 
Para inicializar las tablas y registros minímos para el funcionamiento del sistema, valerse de las Migraciones ya preparas en el sistema., mediante el comando:

    php artisan migrate:fresh --seed

### Listo!
Para acceder al sistema localmente:
1- Reiniciar los procesos de Laragon
2- Acceder mediante: localhost/reservasced/public/ o reservasced.test

## Usuarios de prueba:
Admin: gadecima@gmail.com 123456789
Usuario: feroa88@gmail.com 123456789

### FIN Readme.md
Última actualización 21/02/2022 23:23



