<?php

use Illuminate\Database\Seeder;

class carga_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '0',
            'nombre' => 'Super',
            'apellido' => 'Admin',
            'dni' => '99999999',
            'telefono' => '3813666350',
            'email' => 'superadmin@reservasced.com',
            'cargo' => 'SuperAdmin',
            'dependencia' => 'CED',
            'ofi_area' => 'Soporte Técnico',
            'rol_id' => '0',
            'password' => Hash::make('12345678'),
        ]);

        DB::table('users')->insert([
            'id' => '1',
            'nombre' => 'Williams',
            'apellido' => 'Anthony',
            'dni' => '31382160',
            'telefono' => '3816405884',
            'email' => 'gadecima@gmail.com',
            'cargo' => 'Referente',
            'dependencia' => 'CED',
            'ofi_area' => 'Soporte Técnico',
            'rol_id' => '1',
            'password' => Hash::make('12345678'),
        ]);

        DB::table('users')->insert([
            'id' => '2',
            'nombre' => 'Fernando',
            'apellido' => 'Figueroa',
            'dni' => '11222333',
            'telefono' => '3816123456',
            'email' => 'fernando@gmail.com',
            'cargo' => 'Tecnico',
            'dependencia' => 'FORMAR',
            'ofi_area' => 'FORMAR',
            'rol_id' => '2',
            'password' => Hash::make('12345678'),
        ]);

        DB::table('users')->insert([
            'id' => '3',
            'nombre' => 'Florencia',
            'apellido' => 'Decima',
            'dni' => '34276566',
            'telefono' => '3816123456',
            'email' => 'florencia@gmail.com',
            'cargo' => 'Administracion',
            'dependencia' => 'CED',
            'ofi_area' => 'Recursos Humanos',
            'rol_id' => '2',
            'password' => Hash::make('12345678'),
        ]);
    }
}
