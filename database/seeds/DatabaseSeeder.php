<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(carga_roles::class);
        $this->call(carga_tipos::class);
        $this->call(carga_estados::class);
        $this->call(carga_users::class);
        $this->call(carga_estado_reserv::class);
        $this->call(carga_recursos::class);
        $this->call(carga_reservas::class);
        $this->call(carga_recu_reserv::class);
        $this->call(carga_prestamos::class);
        $this->call(carga_recu_prestam::class);
        $this->call(carga_config::class);
    }
}
