<?php

use Illuminate\Database\Seeder;

class carga_estados extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_recursos')->insert([
            'id' => '1',
            'estado' => 'Funcional',
        ]);
        DB::table('estado_recursos')->insert([
            'id' => '2',
            'estado' => 'Dañado',
        ]);
        DB::table('estado_recursos')->insert([
            'id' => '3',
            'estado' => 'Robado',
        ]);
        DB::table('estado_recursos')->insert([
            'id' => '4',
            'estado' => 'Baja',
        ]);
        DB::table('estado_recursos')->insert([
            'id' => '5',
            'estado' => 'Extraviado',
        ]);
        DB::table('estado_recursos')->insert([
            'id' => '6',
            'estado' => 'Transferido',
        ]);
    }
}
