<?php

use Illuminate\Database\Seeder;

class carga_recu_reserv extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(database_path().'\recuReserv_prueba.sql');
        DB::statement($sql);    
    }
}
