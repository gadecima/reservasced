<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class carga_roles extends Seeder
{
    /**
     * Run the database seeds.
     * ***************************
     *ESTE SEEDER CARGA 3 VALORES EN LA TABLA 'ROLES'
     * 0: SuperAdmin
     * 1: ADMINISTRADOR
     * 2: USUARIO ESTANDAR
     * ***************************
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => '0',
            'rol' => 'SuperAdmin',
        ]);

        DB::table('roles')->insert([
            'id' => '1',
            'rol' => 'Admin',
        ]);

        DB::table('roles')->insert([
            'id' => '2',
            'rol' => 'Usuario',
        ]);
    }
}
