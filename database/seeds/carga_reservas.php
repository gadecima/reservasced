<?php

use Illuminate\Database\Seeder;

class carga_reservas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(database_path().'\reservas_prueba.sql');
        DB::statement($sql);
    }
}
