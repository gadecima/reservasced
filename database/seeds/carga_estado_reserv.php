<?php

use Illuminate\Database\Seeder;

class carga_estado_reserv extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estado_reservas')->insert([
            'id' => '1',
            'estado_reserv' => 'Confirmada', //reserva lista par retirar
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '2',
            'estado_reserv' => 'En Curso', //Los recursos fueron retirados
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '3',
            'estado_reserv' => 'Preparada', //lista para ser retirada por el solicitante
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '4',
            'estado_reserv' => 'Vencida', //Reserva que cumplío su tiempo y no fue devuelta
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '5',
            'estado_reserv' => 'Suspendida', //suspendida por el Admin, no se elimina el registro
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '6',
            'estado_reserv' => 'Finalizada', //todos recursos prestados fueron devueltos
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '7',
            'estado_reserv' => 'Cancelada', //El solicitante cancela la reserva, no se elimina el registro
        ]);
        DB::table('estado_reservas')->insert([
            'id' => '8',
            'estado_reserv' => 'Adeuda', //El solicitante retiro la reserva pero no la devolvió a tiempo
        ]);

    }
}
