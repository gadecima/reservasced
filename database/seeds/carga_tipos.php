<?php

use Illuminate\Database\Seeder;

class carga_tipos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   //recursos reservables predefinidos por el cliente
        DB::table('tipo_recursos')->insert([
            'id' => '1',
            'tipo' => 'netbook',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '2',
            'tipo' => 'Tablet',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '3',
            'tipo' => 'Proyector',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);    
        DB::table('tipo_recursos')->insert([
            'id' => '4',
            'tipo' => 'Camára',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '5',
            'tipo' => 'Pizarra Digital',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '6',
            'tipo' => 'cable video',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '7',
            'tipo' => 'Adaptador Video',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '8',
            'tipo' => 'Parlante',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '9',
            'tipo' => 'Auricular',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '10',
            'tipo' => 'microfono',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '11',
            'tipo' => 'cable audio',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '12',
            'tipo' => 'Adaptador Audio',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '13',
            'tipo' => 'Kit Robotica',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '14',
            'tipo' => 'Kit Raspberry',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '15',
            'tipo' => 'Servidor Pedagogico',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '16',
            'tipo' => 'Drone',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '17',
            'tipo' => 'VRbox',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '18',
            'tipo' => 'cargador netbook',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '19',
            'tipo' => 'cargador USB',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '20',
            'tipo' => 'Multitoma',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '21',
            'tipo' => 'Prolongador',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        //FIN: recursos reservables predefinidos por el cliente
        DB::table('tipo_recursos')->insert([
            'id' => '22',
            'tipo' => 'notebook',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '23',
            'tipo' => 'PC',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '24',
            'tipo' => 'All in One',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '25',
            'tipo' => 'Celular',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '26',
            'tipo' => 'Servidor',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '27',
            'tipo' => 'Switch',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '28',
            'tipo' => 'Access Point',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '29',
            'tipo' => 'Router',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '30',
            'tipo' => 'Rack',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '31',
            'tipo' => 'Cable Ethernet',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '32',
            'tipo' => 'cable power',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '33',
            'tipo' => 'cable usb',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '34',
            'tipo' => 'estabilizador',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '35',
            'tipo' => 'Fuente Pc',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '36',
            'tipo' => 'Batería',
            'recu_accs' => '0',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '37',
            'tipo' => 'Impresora',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '38',
            'tipo' => 'Monitor',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '39',
            'tipo' => 'Mouse',
            'recu_accs' => '1',
            'reservable' => '1',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '40',
            'tipo' => 'Teclado',
            'recu_accs' => '1',
            'reservable' => '0',
        ]);
        DB::table('tipo_recursos')->insert([
            'id' => '41',
            'tipo' => 'Adaptador Red',
            'recu_accs' => '0',
            'reservable' => '1',
        ]);
        
    }
}
