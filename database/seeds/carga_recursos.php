<?php

use Illuminate\Database\Seeder;
//use Illuminate\Database\Facades\DB;

class carga_recursos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(database_path().'\recursos_prueba.sql');
        DB::statement($sql);
    }
}
