<?php

use Illuminate\Database\Seeder;

class carga_config extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'id' => '1',
            'reservas_on' => '1',
            'user_create_on' => '1',
            'dias_antes' => '1',
            'dias_uso' => '5',
            'desde_hora' => '09:00',
            'hasta_hora' => '15:00',
        ]);
    }
}
