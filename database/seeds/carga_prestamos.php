<?php

use Illuminate\Database\Seeder;

class carga_prestamos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prestamos')->insert([
            'id' => '201000',
            'solicitante' => 'Montenegro, Nancy',
            'dni' => '34766276',
            'contacto' => '3813666350',
            'fecha_prestado' => '2022-01-20 10:00:00',
            'fecha_devuelto' => '2022-01-20 13:00:00',
            'estado' => 'Devuelto',
            'comentario' => 'blah blah blah',
            'user_id' => 1, //responsable de prestamo
        ]);
        DB::table('prestamos')->insert([
            'id' => '201001',
            'solicitante' => 'Olivera, Paola',
            'dni' => '35086399',
            'contacto' => '381123456',
            'fecha_prestado' => '2022-01-21 09:00:00',
            'estado' => 'Prestado',
            'comentario' => 'comentario del administrador',
            'user_id' => 1,
        ]);
        DB::table('prestamos')->insert([
            'id' => '201002',
            'solicitante' => 'Homero, Thompson',
            'dni' => '27589698',
            'contacto' => '381666333',
            'fecha_prestado' => '2022-05-08 09:00:00',
            'estado' => 'Prestado',
            'user_id' => 1, 
        ]);
        DB::table('prestamos')->insert([
            'id' => '201003',
            'solicitante' => 'Landa, Lalo',
            'dni' => '33666999',
            'contacto' => '381333666',
            'fecha_prestado' => '2022-05-08 09:30:00',
            'estado' => 'Prestado',
            'user_id' => 1, 
        ]);
        DB::table('prestamos')->insert([
            'id' => '201004',
            'solicitante' => 'Jimmy, Otul',
            'dni' => '22111222',
            'contacto' => '381000666',
            'fecha_prestado' => '2022-05-08 11:00:00',
            'estado' => 'Prestado',
            'user_id' => 1, 
        ]);
    }
}
