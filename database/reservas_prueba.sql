INSERT INTO `reservas` (`id`, `user_id`, `fecha_desde`, `hora_desde`, `fecha_hasta`, `hora_hasta`, `est_reserv_id`, `observ`, `comentario`, `created_at`, `updated_at`) 
	VALUES 	(101001, 2, '2022-05-10', '09:00:00', '2022-05-12', '12:00:00', 1, NULL, NULL, NOW(), NOW()),
		   	(101002, 2, '2022-05-12', '09:00:00', '2021-05-15', '12:00:00', 2, NULL, NULL, NOW(), NOW()),
 			(101003, 2, '2022-05-06', '09:00:00', '2022-05-08', '12:00:00', 3, NULL, NULL, NOW(), NOW()),
			(101004, 3, '2022-05-23', '09:00:00', '2022-05-26', '12:00:00', 4,NULL, NULL, NOW(), NOW()),
	 		(101005, 3, '2022-05-18', '09:00:00', '2022-05-19', '12:00:00', 5,NULL, NULL, NOW(), NOW()),
	 		(101006, 2, '2022-05-25', '09:00:00', '2022-03-27', '12:00:00', 6,NULL, NULL, NOW(), NOW());

