<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRecursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recursos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('codigo', 6)->unique();
            $table->string('serie',20)->default($value = 'N/E')->nullable($value = true);
            $table->string('marca',30)->default($value = 'N/E')->nullable($value = true);;
            $table->string('modelo',30)->default($value = 'N/E')->nullable($value = true);
            $table->string('descripcion',200)->nullable($value = true);
            $table->string('observacion',200)->nullable($value = true);
            $table->bigInteger('tipo')->unsigned();
            $table->bigInteger('estado')->unsigned();
            $table->bigInteger('disponible')->unsigned();
            $table->bigInteger('cant')->unsigned();
            $table->timestamps();

            $table->foreign('tipo')
                    ->references('id')->on('tipo_recursos')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');

            $table->foreign('estado')
                    ->references('id')->on('estado_recursos')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recursos');
    }
}
