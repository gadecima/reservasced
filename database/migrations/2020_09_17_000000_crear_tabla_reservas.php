<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned(); //clave foranea a la tabla users
            $table->date('fecha_desde');
            $table->time('hora_desde');
            $table->date('fecha_hasta');
            $table->time('hora_hasta');
            $table->bigInteger('est_reserv_id')->unsigned(); //clave foranea a la tabla estado_reserva
            $table->text('observ')->nullable($value = true);
            $table->text('comentario')->nullable($value = true);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('est_reserv_id')
                ->references('id')->on('estado_reservas')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
