<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRecuReservas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurso_reservas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('reserva_id')->unsigned(); //clave foranea a la tabla reserva
            $table->bigInteger('recurso_id')->unsigned(); //clave foranea a la tabla recurso
            $table->smallInteger('cant_res')->default($value = '1')->nullable($value = true);

            $table->foreign('reserva_id')
                ->references('id')->on('reservas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('recurso_id')
                ->references('id')->on('recursos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
            $table->timestamps(); //agrega 2 columnas Created_at y Updated_at
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurso_reservas');
    }
}
