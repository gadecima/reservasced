<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('dni',8)->unique();
            $table->string('telefono',15);
            $table->string('email')->unique();
            $table->string('cargo',100);
            $table->string('dependencia',100);
            $table->string('ofi_area',100);
            $table->bigInteger('rol_id')->unsigned();
            $table->string('password');
            $table->rememberToken(); //esto es para guardar el login de sesión en el naveg.
            $table->timestamps(); //agrega 2 columnas Created_at y Updated_at

            $table->foreign('rol_id')
                    ->references('id')->on('roles')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
