<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPrestamos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->id();
            $table->text('solicitante'); //Apellido-nombre del solicitante
            $table->char('dni',10); 
            $table->text('contacto'); //datos de contacto del solicitante
            $table->datetime('fecha_prestado')->nullable($value = true);
            $table->datetime('fecha_devuelto')->nullable($value = true);
            $table->text('estado'); //Prestada o Devuelto
            $table->text('comentario')->nullable($value = true);
            $table->bigInteger('user_id')->unsigned(); //clave foranea a la tabla users
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamos');
    }
}
