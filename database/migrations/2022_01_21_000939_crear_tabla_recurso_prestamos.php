<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaRecursoPrestamos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurso_prestamos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('prestamo_id')->unsigned(); //clave foranea a la tabla prestamo
            $table->bigInteger('recurso_id')->unsigned(); //clave foranea a la tabla recurso
            $table->smallInteger('cant_prest')->default($value = '1')->nullable($value = true); 

            $table->foreign('prestamo_id')
                ->references('id')->on('prestamos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('recurso_id')
                ->references('id')->on('recursos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurso_prestamos');
    }
}
